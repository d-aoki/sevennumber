//
//  ViewController.m
//  calculatePazzle
//
//  Created by Daichi Aoki on 2014/02/17.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

//デザイン系
#import "DesignUtils.h"
#import "SoundsUtils.h"

//スコア画面遷移のためのインポート
#import "ScoreViewController.h"
#import "ProcessingViewUtils.h"

//広告用(バナー、スプラッシュ)
#import "CSSAdView.h"
#import "CSSSplashAdView.h"
#import "appCCloud.h"

#define OBJECT_SIZE 70
#define OBJECT_FIRST_POST 0

//乱数の表示桁数の範囲
#define RANDOM_NUMBER_MIN 1
#define RANDOM_NUMBER_MAX 5

//メインフレームに自動で追加するフレームの数
#define MAINFRAME_MARK_MAX 5

//制限時間
NSString *const limittime_Str = @"90";

//１ステージで上がる範囲
int const countUpNum = 2;

//フリーモードで強制ゲームオーバになるレベル
int const gameoverCnt = 7;

//メインフレームのブロック数
int const mainFrame_height_Blocks = 4;
int const mainFrame_width_Blocks = 4;

//メインブロックの数に対する座標範囲数
//4*4
int const mainFrame_min_X = 20;
int const mainFrame_max_X = 300;

int const mainFrame_min_Y = 120;
int const mainFrame_max_Y = 400;

//アニメーションの速度設定定数
//ブロック生成速度
float const createBlockTime = 0.6;
//マスクAlpha値
float const maskAlpha = 0.4;

//チャレンジモードのレベル設定(※設定値-1)
//(四則演算制御->7,マスク->14)
int const cngOperatorlimitLevel = 6;
int const cngMaskLevel = 13;
//桁上げレベル
int const carryNum = 3;


@interface ViewController ()
//広告系(バナー、スプラッシュ)
@property (strong,nonatomic) CSSAdView *bannerAdView;

//バウンドアニメージョンのプロパティ変数
@property (nonatomic) UIDynamicAnimator *animator;

@end

@implementation ViewController


{
    //モードきりかえフラグ
    int mode;
    
    //他クラスからの読み込み
    //BGM用
    AVAudioPlayer *Audio;
    //ブロックタップ音階と７ブロック(最大８個)
    AVAudioPlayer *tapAudioDo,*tapAudioRe,*tapAudioMi,*tapAudioFa,*tapAudioSo,*tapAudioRa,*tapAudioSi,*tapAudioDo2;
    AVAudioPlayer *tapAudio1,*tapAudio2,*tapAudio3,*tapAudio4,*tapAudio5,*tapAudio6,*tapAudio7,*tapAudio8;
    NSArray *_tapBlockArray,*_seventapBlockArray;
    int tapBlockCnt,seventapBlockCnt;
    //「７」ブロック生成
    AVAudioPlayer *sevenAudio;
    //戻るボタン用
    AVAudioPlayer *backAudio;
    //その他効果音
    AVAudioPlayer *effectAudio;
    
    AVAudioPlayer *undoSound;
    
    //UNDO用情報格納変数とボタン押下許可フラグ
    UIView *_undoSelectView,*_undoChangeView;
    //BOOL undoTapFlg;
    float originSelectX,originSelectY;
    float originChangeX,originChangeY;
    int undoChangeNum,undoSelectNum;
    float undoSelectAlpha,undoChangeAlpha;
    
    //各種設定の読み込み--------------
    //演算子の設定関連
    NSUserDefaults *_operatorUD;
    NSInteger operatorInt;
    int limitFlg;
    //制限時間
    NSUserDefaults *_timeLimiterUD;
    NSInteger timelimiterInt;
    //ライフポイント
    NSUserDefaults *_lifepointUD;
    NSInteger lifepointInt;
    //マスク
    NSUserDefaults *_maskBlocksUD;
    NSInteger maskblocksInt;
    BOOL maskNottouchFlg;
    
    //-----------------------------
    
    //メインフレーム・サブフレームのブロック位置把握変数
    NSMutableArray *_numberObjects,*_numberObjects1,*_numberObjects2,*_numberObjects3,*_numberObjects4;
    NSMutableArray *_numberSubObject;
    
    //ライフ格納変数
    NSArray *_lifePntArray;

    //選択したブロック,変更対象のブロックの代入変数
    UIView *_touchView,*_selectView,*_changeView;
    UIView *_nullBlockView;
    UILabel *changeLabel,*selectLabel;

    //サブブロックの配列数
    NSInteger numberSubArrayCnt;
    
    UIView *numberObj;
    
    
    //制限時間タイマー系変数
    UILabel *timeLimitLabel;
    NSTimer *countdownTimer;
    
    //タッチイベント変数
    CGPoint _mainbeginTouch,_mainendTouch,_mainmoveTouch;
    
    
    //メインフレーム・サブフラグに触れているかの真偽値
    BOOL mainFrameFlg,subFrameFlg;
    //計算結果が正常値かどうか
    BOOL resetCalNum;
    //パーフェクト判別
    BOOL perfectFlg;

    //ゲームオーバー時のタイマーストップフラグ
    BOOL timerState;
    
    //選択したブロックの最初の座標保管変数
    float originX,originY;
    
    //設定変更前のクリアラベルサイズ保持変数
    float originCrearWidth,originCrearHeight;
    
    //チャレンジモードの制限お知らせラベルなどの元位置
    float originOpImageX,originOpImageMsgX,originMaskImageX,originMaskImageMsgX;
    
    //対象のパネル位置把握変数
    int selectArrayRow,selectArrayCol,selectArrayFirstRow,selectArrayFirstCol;
    int calCnt;
    int post_X,post_Y;
    
    //レベルカウント変数,表示範囲(MAX)
    int levelCnt,levelCntMax;
    
    //結合した際の値格納変数
    int calBlock;
    
    //１ステージのトータルスコア格納変数
    int oneStageSumScore;
    
    //クリアできなかった場合のカウント変数
    int notClearCnt;
    
    //四則演算ボタン判別フラグ,現在の状態保存フラグ,1つ前の状態保存フラグ
    //0->未選択,1->minus,2->multiple,3->par
    int calculationFlg,resetcalFlg,undocalFlg;
    
    //チャレンジモード用マックス値格納配列
    int blockMaxInt;
    //一回しか表示させないようにするフラグ
    BOOL cngOperatorFlg,cngMaskFlg,blockmaxFlg;
    
    
    //メインフレームの選択タッチ回数の取得
    int touchNumber_Cnt;
    
    //起動時の四則演算代入変数(乱数)
    int randomCol;
    int randomMark;
    
    //touch系処理の制御真偽値
    BOOL touchControlFlg;
    
    //四則演算押下判別フラグ
    BOOL minusFlg,multipleFlg,parFlg;
    
    
    //四則演算状態の維持(Undo機能)
    int secondUndoInt,firstUndoInt;
}


//*******************
//主に設定系の読み取り
//*******************
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //設定の読みとり(制限時間)
    [self countdownTimerOption];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    //チャレンジモードのみ必要(お知らせ画像の初期位置)
    ///////////////////////////////////////////
    originOpImageX = _operatorImage.frame.origin.x;
    originOpImageMsgX = _operatorImage.frame.origin.x;
    originMaskImageX = _maskImage.frame.origin.x;
    originMaskImageMsgX = _maskImageMsg.frame.origin.x;
    
    NSArray *cngMsgArray = [[NSArray alloc]initWithObjects:_maskMsg,_operatorMsg, nil];
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadow:cngMsgArray];
    
    //play時はundoボタンは押せません
    [_undoBtn setEnabled:NO];
    _undoView.backgroundColor = [UIColor grayColor];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    
    //背景の設定
    [self designSettingAct];
    
    if(appDelegate.modeFlg == 1){
        //BGMの設定(フリーモードの場合のみ)
        [self FreeBGMSettingAct];
    }
   
    //メインフレーム真偽値設定
    mainFrameFlg = NO;
    subFrameFlg = NO;
    resetCalNum = NO;
    perfectFlg = YES;
    touchControlFlg = YES;
    timerState = YES;
    
    //チャレンジモード
    cngMaskFlg = YES;
    cngOperatorFlg = YES;
    blockmaxFlg = YES;
    
    //四則演算制限真偽値
    minusFlg = YES;
    multipleFlg = YES;
    parFlg = YES;
    
    //マスクオプションがONの場合
    if(maskblocksInt==1 && mode != 0){
        maskNottouchFlg = NO;
    }
    
    //計算ボタン初期値
    calculationFlg = 0;
    resetcalFlg = 0;
    undocalFlg = 0;
    calBlock = 0;
    
    secondUndoInt = 0;
    firstUndoInt = 0;
    
    //フレームの定義(オプションボタン)
    [_multipleView.layer setBorderColor:[UIColor greenColor].CGColor];
    [_multipleView.layer setBorderWidth:4.0];
    _multipleView.layer.cornerRadius = 20;
    
    [_minusView.layer setBorderColor:[UIColor blueColor].CGColor];
    [_minusView.layer setBorderWidth:4.0];
    _minusView.layer.cornerRadius = 20;
    
    [_parView.layer setBorderColor:[UIColor yellowColor].CGColor];
    [_parView.layer setBorderWidth:4.0];
    _parView.layer.cornerRadius = 20;
    
    [_undoView.layer setBorderColor:[UIColor orangeColor].CGColor];
    [_undoView.layer setBorderWidth:3.0];
    _undoView.layer.cornerRadius = 15;
    
    
    //とりあえずここでインスタンスか
    _nullBlockView = [[UIView alloc]init];

    //オブジェクト生成(配列の初期化)
    _numberObjects = [[NSMutableArray alloc]initWithCapacity:3];
    _numberObjects1 = [[NSMutableArray alloc]initWithCapacity:3];
    _numberObjects2 = [[NSMutableArray alloc]initWithCapacity:3];
    _numberObjects3 = [[NSMutableArray alloc]initWithCapacity:3];
    _numberObjects4 = [[NSMutableArray alloc]initWithCapacity:3];
    
    //ブロックタップ音階配列初期か
    _tapBlockArray = [[NSArray alloc]initWithObjects:@"pianoD",@"pianoR",@"pianoM",@"pianoF",@"pianoS",@"pianoRa",@"pianoSi",@"pianoD2",nil];
    
    //各オブジェクト変数の初期化
    post_X = 0;
    post_Y = 0;
    
    selectArrayCol = 0;
    selectArrayRow = 0;
    
    touchNumber_Cnt = 0;
    numberSubArrayCnt = 0;
    
    //タップ音変数初期か
    tapBlockCnt = 0;
    seventapBlockCnt = 0;
    
    //初期レベル１から
    levelCnt = 1;
    levelCntMax = 0;
    
    //クリアできない場合のカウント変数
    notClearCnt = 0;
    
    limitFlg = 0;
    oneStageSumScore = 0;
    mode = 0;
    calCnt = 0;
    
    blockMaxInt= RANDOM_NUMBER_MAX;
    
    //初期レベルは１
    appDelegate.levelDate = 1;

    //クリア文字の元々のサイズを保持
    originCrearWidth = _perfectLbl.frame.size.width;
    originCrearHeight = _perfectLbl.frame.size.height;
    
    //モード判別値格納変数(現在のモード)
    mode = [self modeDistinction];
    
    //設定情報の読み込み処理
    [self settingNSUserDefaults];
    
    //設定の読みとり(四則演算制御)
    [self settingOperator];
    
    
    //設定の読みとり(ライフポイント)
    [self settingLifePoint];
    
    
    //アプリ起動時に制限時間ラベルを作成しておきます。
    [self addTimelimitLabel];
    
    
    //新規ブロック生成
    [self createRandomBlock];
    //前面
    [_mainPanel bringSubviewToFront:_perfectLbl];
    
    ////////////////////////
    // 広告
    
#if AD_DISABLE
    DLog(@"広告非表示");
#else
    
    DLog(@"広告表示");
    //フラグが"NO"の場合はスプラッシュ広告を出さない
    //初期値設定(NO)
    //[splash]
    appDelegate.splashAdFlg = NO;
    //splash広告カウントの初期化
    appDelegate.splashCnt = 0;
    
    // Windowスクリーンのサイズを取得
    CGRect WindowSize = [[UIScreen mainScreen] bounds];

    if(WindowSize.size.height == 480){
        //3.5インチ
        //広告は出しません
    }else{
        //4インチ
        // [banner <appC cloud>]
        [self.view addSubview:appDelegate.simpleAdView];
    }
    
#endif

    
}

//*****************************************
//指定した範囲の座標にタッチした際に呼ばれるメソッド
//移動させたいナンバーブロックを選択
//*****************************************
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    _mainbeginTouch = [touch locationInView:self.view];
    
    //UIViewインスタンス初期か
    _touchView = [[UIView alloc]init];
    
    //マスク機能がONでマスク処理中の場合
    //タッチイベントを終了します
    if((mode == 1 && maskblocksInt==1) || (mode == 2 && levelCnt > cngMaskLevel)){
        if(maskNottouchFlg == NO){
            return;
        }
    }
    
    if(touchControlFlg == YES){
    
    
    //*******************************
    //メインフレームの範囲制限(大)
    //6×5->x=10~310,y=127~487
    //5×5->y=204~504
    //*******************************
    if((_mainbeginTouch.x > mainFrame_min_X && _mainbeginTouch.x <mainFrame_max_X)&&(_mainbeginTouch.y > mainFrame_min_Y && _mainbeginTouch.y <mainFrame_max_Y)){
        
        //各ブロックの領域を細かく分けます。
        //列単位(中)
        if(_mainbeginTouch.y <= mainFrame_min_Y+OBJECT_SIZE){
            selectArrayRow = 0;
            //行単位(小)
            if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE){
                selectArrayCol = 0;
                _touchView = _numberObjects[0];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*2){
                selectArrayCol = 1;
                _touchView = _numberObjects[1];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*3){
                selectArrayCol = 2;
                _touchView = _numberObjects[2];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*4){
                selectArrayCol = 3;
                _touchView = _numberObjects[3];
                
            }
        }else if(_mainbeginTouch.y <= mainFrame_min_Y+(OBJECT_SIZE*2)){
            selectArrayRow = 1;
            //行単位(小)
            if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE){
                selectArrayCol = 0;
                _touchView = _numberObjects1[0];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*2){
                selectArrayCol = 1;
                _touchView = _numberObjects1[1];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*3){
                selectArrayCol = 2;
                _touchView = _numberObjects1[2];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*4){
                selectArrayCol = 3;
                _touchView = _numberObjects1[3];
            }
        }else if(_mainbeginTouch.y <= mainFrame_min_Y+(OBJECT_SIZE*3)){
            selectArrayRow = 2;
            //行単位(小)
            if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE){
                selectArrayCol = 0;
                _touchView = _numberObjects2[0];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*2){
                selectArrayCol = 1;
                _touchView = _numberObjects2[1];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*3){
                selectArrayCol = 2;
                _touchView = _numberObjects2[2];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*4){
                selectArrayCol = 3;
                _touchView = _numberObjects2[3];

            }
        }else if(_mainbeginTouch.y <= mainFrame_min_Y+(OBJECT_SIZE*4)){
            selectArrayRow = 3;
            //行単位(小)
            if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE){
                selectArrayCol = 0;
                _touchView = _numberObjects3[0];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*2){
                selectArrayCol = 1;
                _touchView = _numberObjects3[1];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*3){
                selectArrayCol = 2;
                _touchView = _numberObjects3[2];
                
            }else if(_mainbeginTouch.x <= mainFrame_min_X+OBJECT_SIZE*4){
                selectArrayCol = 3;
                _touchView = _numberObjects3[3];
            }
        }
        
        //タッチしたBlockの値が0かどうか
        //0はブロックが存在しないということで処理をしない
        //選択したViewの数値取得(7の場合の処理のため)
        NSArray *numberObjArray = _touchView.subviews;
        selectLabel = numberObjArray[0];
        int numberObjNum = [selectLabel.text intValue];
        
        if(numberObjNum==1000000){
            //何もしません
            return;
            
        }else{
        
            //最初の対象ブロックの選択・保存
            if(touchNumber_Cnt == 0){
            
                //一回目に選択したブロックの内部位置を登録
                selectArrayFirstRow = selectArrayRow;
                selectArrayFirstCol = selectArrayCol;
            
                //選択した数字が[7]でないときは普通に処理
                if(numberObjNum!=7){
                    _selectView = _touchView;
                    
                    //1つめのブロック選択時undoボタンは押せません
                    [_undoBtn setEnabled:NO];
                    _undoView.backgroundColor = [UIColor grayColor];
                    
                    //undo用の記録Viewに保存
                    _undoSelectView = [[UIView alloc]init];
                    _undoSelectView = _touchView;
                    //DLog(@"%f",_undoSelectView.alpha);
                    undoSelectAlpha = _undoSelectView.alpha;
                    originSelectX = _selectView.frame.origin.x;
                    originSelectY = _selectView.frame.origin.y;
                    
                    [_selectView.layer setBorderColor:[UIColor redColor].CGColor];
                    _selectView.backgroundColor = [UIColor whiteColor];
                
                    
                    //ブロック選択音
                    //[self soundsPlayAct:@"pianoD"];
                    [self tapBlockSound:_selectView];
                    
                    //選択したフレームの座標を保存しておく
                    originX = _selectView.frame.origin.x;
                    originY = _selectView.frame.origin.y;
                
                    //２回目の対象ブロック選択フラグ値
                    touchNumber_Cnt=1;
                }else{
                    //7の場合はブロックをけして、得点加算！
                    //くるくる回転しながら消えていきます
                    touchNumber_Cnt=0;
                
                    //７ブロックがくるくる回りながら消えます+点数配分を行います。
                    [self tapSevenBlockAnimated:_touchView];
                    
                    //７ブロックタップサウンド
                    [self createSevenSound:_touchView];
                    //seventapBlockCnt++;
                
                    //消えたブロック判別処理
                    [self notBlockAction:selectArrayRow Action:selectArrayCol];
                    
                    //「７」ブロックをタップ時に白ブロックのカウントと自動計算でのリセット判別
                    [self moveWhiteBlockCheckAct];
                    
                }
            }else{
            
                //２番目に選択したViewを格納
                _changeView = _touchView;
                _changeView.backgroundColor = [UIColor whiteColor];
                
                //２番目に選択したブロックも記録する
                _undoChangeView = [[UIView alloc]init];//困ったときの初期化
                _undoChangeView = _touchView;
                originChangeX = _touchView.frame.origin.x;
                originChangeY = _touchView.frame.origin.y;
                undoChangeAlpha = _undoChangeView.alpha;
                //DLog(@"%f",_undoChangeView.alpha);
                
    
                //タッチしたブロックが一緒の場合
                if((selectArrayFirstCol==selectArrayCol) && (selectArrayFirstRow == selectArrayRow)){
                    
                    [_changeView.layer setBorderColor:[UIColor blackColor].CGColor];
                    touchNumber_Cnt=0;
                    
                    //undo記録変数も初期化
                    _changeView = nil;
                    
                    //タッチしたブロックが一緒の場合は音はそのままです
                    tapBlockCnt--;
                    
                }else{
                    //touchEndのブロック移動フレーム処理許可フラグ
                    mainFrameFlg = YES;

                    [_changeView.layer setBorderColor:[UIColor redColor].CGColor];
                    
                    //ブロック選択音
                    //[self soundsPlayAct:@"pianoD"];
                    [self tapBlockSound:_changeView];
                
                    touchNumber_Cnt=0;
                }
            }
        }
    
    }
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    _mainmoveTouch = [touch locationInView:self.view];
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    _mainendTouch = [touch locationInView:self.view];
    
    //マスク機能がONでマスク処理中の場合
    //タッチイベントを終了します
    if((mode == 1  && maskblocksInt==1 )|| (mode == 2 && levelCnt > cngMaskLevel)){
        if(maskNottouchFlg == NO){
            return;
        }
    }
    
    //*****************************************************
    //そもそもタッチ開始がメインフレームでない場合この処理は行えない
    //mainFrameFlg == YES;
    //*****************************************************
    if(touchControlFlg == YES){
    
    if(mainFrameFlg == YES){
        
        //２つ目のViewが選択されたらタッチイベントを制御します(押せないようにする)
        touchControlFlg = NO;
        
        //選択したViewの数値取得
        NSArray *selectLabelArray = _selectView.subviews;
        selectLabel = selectLabelArray[0];
        int selectNum = [selectLabel.text intValue];
        undoSelectNum = [selectLabel.text intValue];
        
        NSArray *changeLabelArray = _changeView.subviews;
        changeLabel = changeLabelArray[0];
        int changeNum = [changeLabel.text intValue];
        undoChangeNum = [changeLabel.text intValue];
        
        //演算子ボタンが仕様されている場合はそのボタンの計算を行う
        //選択した二つのブロック値を計算
        calBlock = 0;
        
        //割り算の場合、割り切れる式かどうか判別する
        BOOL parFlg = NO;
        
        
        if (calculationFlg==0) {
            //加算
            limitFlg = 0;
            calBlock = selectNum + changeNum;
            
        }else if(calculationFlg==1){
            //減算
            limitFlg = 1;
            calBlock = selectNum - changeNum;
            
        }else if(calculationFlg==2){
            //かけ算
            limitFlg = 2;
            calBlock = selectNum * changeNum;
        }else if(calculationFlg==3){
            //割り算
            limitFlg = 3;
            calBlock = selectNum / changeNum;
            //割り切れるかどうか判別します
            if((selectNum%changeNum)!=0){
                //割り切れない場合
                parFlg=YES;
            }
        }
    
        //四則演算の制限処理
        [self limitoperatorAct];
        
        //結合値が2桁以上か確認する
        NSNumber *checksumNum = [[NSNumber alloc]initWithInt:calBlock];
        //桁数を求める
        NSInteger digits = (int)log10([checksumNum doubleValue]) + 1;
        

        //答えが「マイナス」もしくは「計算式が割り切れない」、３桁以上の場合、不正操作とみなす
        if((calBlock < 0) || parFlg == YES || digits > 2 || changeNum == 7){
        
            //四則演算制限がかかっていた場合は制限解除(そのステージの)
            [self noCalwithResetOperatorAct];
            
            //ブロックくるくる
            [self notCalBlockTranceAct];
            
            //選択しているブロックの枠線を戻す(赤->白)
            [_selectView.layer setBorderColor:[UIColor blackColor].CGColor];
            
            if(changeNum == 7){
                
                //なんか「７」ブロックの色が戻ってしまうので...
                if(_changeView.tag == 1){
                    _changeView.backgroundColor = [UIColor redColor];
                }else if (_changeView.tag == 2){
                    _changeView.backgroundColor = [UIColor blueColor];
                }else if (_changeView.tag == 3){
                    _changeView.backgroundColor = [UIColor yellowColor];
                }else{
                    _changeView.backgroundColor = [UIColor greenColor];
                }
                
                [_changeView.layer setBorderColor:[UIColor whiteColor].CGColor];
            }else{
                [_changeView.layer setBorderColor:[UIColor blackColor].CGColor];
            }
            //不正操作お知らせ音
            [self effectSound:@"notcalplay2"];
            
        }else{
            
            //計算ペナルティの場合の前設定の保存(四則演算フラグが変わる前に)
            resetcalFlg = calculationFlg;
            
            //対象ブロックの移動
            [self moveBlockFrame];
            //最初に選択したブロック位置を初期か(正常に移動できた場合のみ)
            [self notBlockAction:selectArrayFirstRow Action:selectArrayFirstCol];
        }
        
        //ブロックの計算が終了した際に、演算ボタンの色を戻します)
        //チャレンジモードの場合は四則演算制限がない場合だけ通します
        if((mode == 1 && levelCnt <= 0) || (operatorInt == 0 && mode == 1) || (mode == 2 && levelCnt <= cngOperatorlimitLevel) || mode == 0){
            _parView.backgroundColor = [UIColor whiteColor];
            _minusView.backgroundColor = [UIColor whiteColor];
            _multipleView.backgroundColor = [UIColor whiteColor];
        }
        //ブロックの移動処理終了でフラグを戻す
        mainFrameFlg = NO;
        
        //ブロックが移動したらUndoFlgを許可する
        [_undoBtn setEnabled:YES];
        _undoView.backgroundColor = [UIColor whiteColor];

        }
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//******************
//乱数発生メソッド
//******************
-(int)getRandamInt:(int)min max:(int)max {
    static int initFlag;
    if (initFlag == 0) {
        srand((unsigned int)time(NULL));
        initFlag = 1;
    }
    
    int randomNum = 0;
    //乱数代入
    randomNum = min + (int)(rand()*(max-min+1.0)/(1.0+RAND_MAX));
    
    //乱数が「７」の場合、再度乱数発生処理
    while (randomNum == 7) {
        //乱数代入
        randomNum = min + (int)(rand()*(max-min+1.0)/(1.0+RAND_MAX));
    }
    
    return randomNum;
}

//**************************************************
//対象のblock移動
//演算子オプションを使用しない場合はプラスした数値にかわる処理
//**************************************************
-(void)moveBlockFrame
{
    
    //タイマー
    [NSTimer scheduledTimerWithTimeInterval:0.2//タイマーを発生させる間隔(0.3秒)
                                        target:self//メソッドがあるオブジェクト
                                    selector:@selector(numberBlockAnimated)//呼び出すメソッド
                                    userInfo:nil//メソッドに渡すパラメータ
                                    repeats:NO];//繰り返しなし*/
    
        
    //２番目にタッチされたViewはメインフレ-むの上を移動する
    [self.view bringSubviewToFront:_changeView];
        
    [UIView animateWithDuration:0.1
                        animations:^{
                            [_selectView setFrame:CGRectMake(_changeView.frame.origin.x,_changeView.frame.origin.y,OBJECT_SIZE,OBJECT_SIZE)];
                        }];
}

//******************************
//裏返しアニメーションメソッド
//計算結果ブロック生成
//******************************
-(void)numberBlockAnimated{

    //ブロックが重なったら片方のブロックを削除する
    [_selectView removeFromSuperview];

    
    //合計値を文字列にキャスト
    NSString *plusBlockStr = [NSString stringWithFormat:@"%d",calBlock];
    
    //合計値ブロック表示
    changeLabel.text = plusBlockStr;
    
    
    //もし結合値が「7」の場合、ブロックの色をカエル
    if([changeLabel.text isEqual:@"7"]){
        
        //四則演算子によるブロックの色分け
        [self sevenblockColor:calculationFlg view:_changeView];
        
        //ブロックタッチの効果音も初期化する
        tapBlockCnt = 0;
        
        [_changeView.layer setBorderColor:[UIColor whiteColor].CGColor];
        
        //結合ブロック生成音(7)
        //[self soundsPlayAct:@"numberSeven"];
        [self createSevenSound:_changeView];
        
    }else{
        _changeView.backgroundColor = [UIColor whiteColor];
        
        [_changeView.layer setBorderColor:[UIColor blackColor].CGColor];
        //結合ブロック生成音(7以外)
        [self soundsPlayAct:@"moveBlock"];
    }
    
    
    
    //マスク機能がついていた場合、マスクをきる
    _changeView.alpha = 0.8f;
    //選択したViewの数値取得
    NSArray *checkLabelArray = _changeView.subviews;
    UILabel *checkLabel = checkLabelArray[0];
    checkLabel.alpha = 1.0;

    
    //裏返すアニメーションの設定
    // アニメーション定義開始
    [ UIView beginAnimations: @"TransitionAnimation" context:nil ];
    
    // トランジションアニメーションを設定
    [ UIView setAnimationTransition: UIViewAnimationTransitionFlipFromRight
                            forView:_changeView
                                cache:YES];
    
    // アニメーションを開始
    [ UIView commitAnimations];
    
    
    //結合ブロックを内部的位置づけする
    if(selectArrayRow==0){
        _numberObjects[selectArrayCol]=_changeView;
    }else if(selectArrayRow==1){
        _numberObjects1[selectArrayCol]=_changeView;
    }else if(selectArrayRow==2){
        _numberObjects2[selectArrayCol]=_changeView;
    }else if(selectArrayRow==3){
        _numberObjects3[selectArrayCol]=_changeView;
    }
    
    //計算アニメーションが終了したタイミングでタッチ制御解放
    touchControlFlg = YES;
    //四則演算の設定を初期化する
    calculationFlg = 0;
    
    //ブロック移動の最後に白ブロックの数をカウント
    //2以下でかつ自動計算の結果、「７」ブロックが作られなければリセット...処理
    [self moveWhiteBlockCheckAct];
    
}



//*********************************
//ブロックがない場合の処理
//「1000000」はブロックなしと捉える
//*********************************
-(void)notBlockAction:(int)ArrayRow Action:(int)ArrayCol{
    
    //消した「７」ブロックの内部位置の変更
    UIView *_lisetView = [[UIView alloc]init];
    
    //ラベル生成(ラベル「1000000」はブロックなしとする)
    UILabel *number = [[UILabel alloc]init];
    number.frame = _lisetView.bounds;
    number.textAlignment = NSTextAlignmentCenter;
    number.text = @"1000000";
    number.font = [UIFont fontWithName:@"Verdana" size:20];
    [_lisetView addSubview:number];
    
    
    
    if(ArrayRow==0){
        _numberObjects[ArrayCol]=_lisetView;
    }else if(ArrayRow==1){
        _numberObjects1[ArrayCol]=_lisetView;
    }else if(ArrayRow==2){
        _numberObjects2[ArrayCol]=_lisetView;
    }else if(ArrayRow==3){
        _numberObjects3[ArrayCol]=_lisetView;
    }
}


//*****************************************
//計算結果がマイナスもしくは割り切れなかった場合
//残っているブロックが回る(7以外)
//*****************************************
-(void)notCalBlockTranceAct{
    
    for(int i=0;i<mainFrame_height_Blocks;i++){
        for(int j=0;j<mainFrame_width_Blocks;j++){
            
            //エラー動作(Viewの回転)を行う
            UIView *_resetMoveView = [[UIView alloc]init];
           
                if(i==0){
                    _resetMoveView = _numberObjects[j];
                }else if(i==1){
                    _resetMoveView = _numberObjects1[j];
                }else if(i==2){
                    _resetMoveView = _numberObjects2[j];
                }else if(i==3){
                    _resetMoveView = _numberObjects3[j];
                }
            
            //選択したViewの数値取得
            NSArray *LabelArray = _resetMoveView.subviews;
            UILabel *checkLabel = LabelArray[0];
            int checkNum = [checkLabel.text intValue];
            
            //回転アニメーション
            // y軸に対して回転．（z軸を指定するとUIViewのアニメーションのように回転）
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
            
            // アニメーションのオプションを設定
            animation.duration = 0.3; // アニメーション速度
            animation.repeatCount = 2; // 繰り返し回数
            
            // 回転角度を設定
            animation.fromValue = [NSNumber numberWithFloat:0.0]; // 開始時の角度
            animation.toValue = [NSNumber numberWithFloat:2 * M_PI]; // 終了時の角度
            
            //ブロック値が「７」以外のものをまわす(高速スピン！)
            if(checkNum != 7){
                // アニメーションを追加
                [_resetMoveView.layer addAnimation:animation forKey:@"rotate-layer"];
            }
            
            //くるくるが終了したタイミングでタッチ制御解放
            touchControlFlg = YES;
            //タッチしたブロックが一緒の場合は音はそのままです
            //tapBlockCnt--;
            //失敗したときは初期かでいいのかな
            calculationFlg=0;
        }
    }
}

//*********************************
//「７」ブロックを消す毎に点数を加算する
//*********************************
-(void)sumScoreAct:(int)calFlg{
    
    //現在のスコアをキャスト代入
    int nowScore = [_totalScore.text intValue];
    
    
    if(calFlg == 0){
        //加算ブロックは通常通り+7
        nowScore = nowScore + 7;
        //パーフェクトの場合の元合計格納
        oneStageSumScore += 7;
        
    }else if (calFlg == 2){
        //減算ブロックは通常+2倍！
        nowScore = nowScore + 14;
        //パーフェクトの場合の元合計格納
        oneStageSumScore += 14;
    }else if (calFlg == 3){
        //除算ブロックは通常×3倍！！
        nowScore = nowScore + 28;
        //パーフェクトの場合の元合計格納
        oneStageSumScore += 28;
    }else{
        //乗算は×1(まず入ることがない)
        nowScore = nowScore + 7;
        //パーフェクトの場合の元合計格納
        oneStageSumScore += 7;
    }
    
    
    _totalScore.text = [NSString stringWithFormat:@"%d",nowScore];
    
    //現在のスコアをappDelegateに代入する
    [self appdelegateScore:nowScore];
}

//**********************
//パーフェクトだった場合
//ポイント2倍表示処理
//**********************
-(void)perfectPointAct{
    
    //ラベルが大きくなりながらフェードあうと
    /* 拡大・縮小 */
    
    // 拡大縮小を設定
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    // アニメーションのオプションを設定
    animation.duration = 0.8; // アニメーション速度
    animation.repeatCount = 1; // 繰り返し回数
    animation.autoreverses = YES; // アニメーション終了時に逆アニメーション
    
    // 拡大・縮小倍率を設定
    animation.fromValue = [NSNumber numberWithFloat:1.0]; // 開始時の倍率
    animation.toValue = [NSNumber numberWithFloat:2.0]; // 終了時の倍率
    
    // アニメーションを追加
    [_twoPointLbl.layer addAnimation:animation forKey:@"scale-layer"];
    
    // 引数が1つのメソッドを遅延実行する
    [self performSelector:@selector(fadeout:)
               withObject:_twoPointLbl afterDelay:0.3];
    
    
    //現在の点数＋２倍
    //現在のスコアを代入
    
    int nowScore = [_totalScore.text intValue];
    
    //現在のスコア倍にする！
    nowScore = nowScore + (oneStageSumScore * 2);
    
    //終わったら初期か
    oneStageSumScore = 0;
    
    _totalScore.text = [NSString stringWithFormat:@"%d",nowScore];
    
    //現在のスコアをappDelegateに代入する(パーフェクトの場合)
    [self appdelegateScore:nowScore];
    
    
    [self limitPlusAct];
    
}


//*************************************
//ステージが変わる毎にレベルアップ値が変わる
//ステージレベルの切り替えとアニメーション
//*************************************
-(void)levelUpAct{
    
    levelCnt++;
    
    //チャレンジモードの場合入ります
    if(mode == 2){
        [self challengemodeAnimated];
    }
    //３ずつ数字が挙っていく
    //levelCntMax = levelCnt+countUpNum;
    //levelCntMax += countUpNum;
    //DLog(@"%d",levelCntMax);
    [self nomalSevenLebelAct];
   
    //appDelegate変数に現在のレベルを代入
    [self appdelegateScoreLevel:levelCnt];
    
    
    
    //viewを倍にする
    [self magnificationUpAct:_levelLbl];

}

//***************************************
//パーフェクトの場合、クリア文字とアニメーション
//フェードイン->拡大->フェードアウト
//***************************************
-(void)crearLblAct:(UILabel *)perLbl{
    
    [self fadeIn:perLbl];
    
    //viewを倍にする
    [self magnificationUpAct:perLbl];
    
    // 引数が1つのメソッドを遅延実行する
    [self performSelector:@selector(fadeout:)
               withObject:perLbl afterDelay:0.5];
    
}


#pragma mark - blockAction

//**********************************
//ブロック生成処理メソッド(現在はランダム)
//**********************************
-(void)createRandomBlock
{

    //メインパネルに初期ブロック生成
    for(int i=0;i<mainFrame_height_Blocks;i++){
        for(int j=0;j<mainFrame_width_Blocks;j++){
            
            //メインフレームのブロック
            numberObj = [[UIView alloc]initWithFrame:CGRectMake(0+post_X,0+post_Y,OBJECT_SIZE,OBJECT_SIZE)];
            
            
            //乱数発生メソッドの呼び出し
            NSString *blocknumStr = [self modeNumberSphere];
            
            //ブロックデザイン
            [numberObj.layer setBorderColor:[UIColor blackColor].CGColor];
            [numberObj.layer setBorderWidth:1.0];
            numberObj.layer.cornerRadius = 18;
            //numberObj.layer.shadowOpacity = 0.8;
            numberObj.layer.shadowOffset = CGSizeMake(2, 2);
            numberObj.backgroundColor = [[UIColor whiteColor]init];
            numberObj.alpha = 0.8f;
            
            //ラベル生成
            UILabel *number = [[UILabel alloc]init];
            number.frame = numberObj.bounds;
            number.textAlignment = NSTextAlignmentCenter;
            number.text = blocknumStr;
            number.font = [UIFont fontWithName:@"Chalkboard SE" size:25];
            [numberObj addSubview:number];
            
            //多次元配列に代入
            //内部的ブロック位置の登録
            switch (i) {
                case 0:
                    _numberObjects[j] = numberObj;
                    break;
                case 1:
                    _numberObjects1[j] = numberObj;
                    break;
                case 2:
                    _numberObjects2[j] = numberObj;
                    break;
                case 3:
                    _numberObjects3[j] = numberObj;
                    break;
                default:
                    break;
            }
            
            
            //一旦ブロックサイズを"０"(倍率)にする
            // アニメーションをする処理
            numberObj.transform = CGAffineTransformMakeScale(0,0);
            
            //メインパネルにブロックの張り付け
            [_mainPanel addSubview:numberObj];
            
            // アニメーションをする処理
            numberObj.transform = CGAffineTransformMakeScale(0,0);
            
            [UIView animateWithDuration:createBlockTime
                             animations:^{
                                 // アニメーションをする処理
                                 numberObj.transform = CGAffineTransformMakeScale(1.0,1.0);
                             } completion:nil];
            
            post_X+=OBJECT_SIZE;
        }
        post_X=0;
        post_Y+=OBJECT_SIZE;
    }
    
    post_X = 0;
    post_Y = 0;
    
    
    //ブロック作成音
    //[self soundsPlayAct:@"createBlock"];
    [self effectSound:@"createBlock"];
    
    //生成が終わったらタッチ制御解放と演算子の制限解除(設定ONの場合)
    touchControlFlg = YES;
    
    [self resetOperatorbuttonAct];
    
    //オプション機能(マスク)
    [self settingMaskBlocks];
    //ボタンが押せなくなったら、Viewも暗くします
    _undoView.backgroundColor = [UIColor grayColor];
    [_undoBtn setEnabled:NO];
}


//***************************************
//残っているブロック全削除(ブロックの再生成)
//***************************************
-(void)recreateBlockAct{
    
    //ブロックを再生成する間、タッチ制御
    touchControlFlg = NO;
    
    //画面上のブロックを消します
    //残ったブロック格納用
    UIView *_downMoveView = [[UIView alloc]init];
    
    for(int i=0;i<mainFrame_height_Blocks;i++){
        for(int j=0;j<mainFrame_width_Blocks;j++){
            if(i==0){
                _downMoveView = _numberObjects[j];
            }else if(i==1){
                _downMoveView = _numberObjects1[j];
            }else if(i==2){
                _downMoveView = _numberObjects2[j];
            }else if(i==3){
                _downMoveView = _numberObjects3[j];
            }
            
            
            
            //削除アニメーション
            [UIView animateWithDuration:0.5 // アニメーション速度2.5秒
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 // 画像消える
                                 _downMoveView.transform = CGAffineTransformMakeScale(0,0);
                             } completion:nil];}
    }
    
    
    //ブロックを全部使ったかどうかのチェック
    [self checkPerfectBlock];
    
    //配列の初期化
    _numberObjects = [[NSMutableArray alloc]initWithCapacity:3];
    _numberObjects1 = [[NSMutableArray alloc]initWithCapacity:3];
    _numberObjects2 = [[NSMutableArray alloc]initWithCapacity:3];
    _numberObjects3 = [[NSMutableArray alloc]initWithCapacity:3];
    _numberObjects4 = [[NSMutableArray alloc]initWithCapacity:3];
    
    //パーフェクト確認フラグの初期か(初期値->YES)
    perfectFlg = YES;
    //チャレンジモードの桁上げ
    blockmaxFlg = YES;
    
    //新規ブロック投下！
    // 引数なしのメソッドを遅延実行する
    [self performSelector:@selector(createRandomBlock)
               withObject:nil afterDelay:0.7];
    
    
    //ブロックが再生成されたタイミングでタイマーがリセットされます
    [self performSelector:@selector(resetTimer)
               withObject:nil afterDelay:1.7];
    
}


//*********************************************************
//白ブロックの残り数の確認処理メソッド
//白ブロックが２つ以下で、そのブロックの計算結果でブロックリセット判別
//このメソッドややこしいです。。
//*********************************************************
-(void)moveWhiteBlockCheckAct{
    
    //残っているブロックに「７」がないか確認します
    UIView *view = [[UIView alloc]init];
    
    //7ブロックのチェックカウント変数
    int sevenCheckCnt=0;
    //白ブロックの残り数の確認
    int whiteBlockCnt = 0;
    
    //残２つの数値を配列に代入
    int whiteblock1=999;
    int whiteblock2=999;
    
    //ブロックリセット判別フラグ
    BOOL blockResetFlg = NO;
    
    //縦4横4のブロック
    for(int i=0;i<mainFrame_height_Blocks;i++){
        for(int j=0;j<mainFrame_width_Blocks;j++){
            
            if(i==0){
                view = _numberObjects[j];
            }else if(i==1){
                view = _numberObjects1[j];
            }else if(i==2){
                view = _numberObjects2[j];
            }else if(i==3){
                view = _numberObjects3[j];
            }
            
            NSArray *LabelArray = view.subviews;
            UILabel *numLabel = LabelArray[0];
            int checkNum = [numLabel.text intValue];
            
            
            //7が存在するかチェックします
            if(checkNum==7){
                sevenCheckCnt++;
            }
            
            //白ブロックの残り数の確認
            if(checkNum!=7 && checkNum!=1000000){
                whiteBlockCnt++;
                
                if(whiteBlockCnt == 1){
                    whiteblock1 = checkNum;
                }else if (whiteBlockCnt == 2){
                    whiteblock2 = checkNum;
                }
            }
        }
    }
    
    //残ブロック２つ以下でのブロックリセット判別メソッド
    blockResetFlg = [self whiteblockCheckAct:whiteblock1 block:whiteblock2 blockCnt:whiteBlockCnt];
    
    //「７」が残っていない場合、残りのブロックを下に落とす
    if(sevenCheckCnt==0 && blockResetFlg == YES){
        
        //ブロックの再生成
        [self recreateBlockAct];
    }
    
}


//*******************************************
//残ブロックが２つの場合
//内部計算をして７にならない場合はリセットフラグを渡す
//reset判別を返す
//*******************************************
-(BOOL)whiteblockCheckAct:(int)whiteBlockA block:(int)whiteBlockB blockCnt:(int)whiteCnt{
    
    BOOL resetFlg = NO;
    
    //白ブロックが3つ以下で、計算結果(内部的)が７にならない場合
    if(whiteCnt < 3){
    
        //999が２つは白ブロックなし
        if(whiteBlockA == 999 && whiteBlockB == 999){
            resetFlg = YES;
        }
        //999が１つの場合は白ブロックが１つのみ
        else if (whiteBlockA != 999 && whiteBlockB == 999){
            if(whiteBlockA != 7){
                resetFlg = YES;
            }
            //白ブロックが２つ残っている場合
        }else{
            //加算から入ります
            if((whiteBlockA+whiteBlockB)==7){
                resetFlg = NO;
            }
            //次は減算に入ります
            else if ((whiteBlockA-whiteBlockB)==7){
                resetFlg = NO;
            }else if ((whiteBlockB-whiteBlockA)==7){
                resetFlg = NO;
            }
            //次は除算に入ります
            else if ((whiteBlockA/whiteBlockB)==7){
                resetFlg = NO;
            }else if ((whiteBlockB/whiteBlockA)==7){
                resetFlg = NO;
            }else{
                resetFlg = YES;
            }
        }
    }
    
    return resetFlg;
    
}


//************************************************
//「７」ブロックを消した時点で、ブロック内部値のチェック
//ブロックが全て消えている場合はプラス加算！
//************************************************
-(void)checkPerfectBlock{
    
    //ブロックタップ音の初期化
    tapBlockCnt = 0;
    
    //ブロックが消えて新しいブロックを生成する間
    //タッチ制御
    touchControlFlg = NO;
    
    //ブロック数分ループ
    for(int i=0;i<mainFrame_height_Blocks;i++){
        for(int j=0;j<mainFrame_width_Blocks;j++){
            
            //エラー動作(Viewの回転)を行う
            UIView *_checkView = [[UIView alloc]init];
            
            if(i==0){
                _checkView = _numberObjects[j];
            }else if(i==1){
                _checkView = _numberObjects1[j];
            }else if(i==2){
                _checkView = _numberObjects2[j];
            }else if(i==3){
                _checkView = _numberObjects3[j];
            }
            
            //選択したViewの数値取得
            NSArray *checkLabelArray = _checkView.subviews;
            UILabel *checkLabel = checkLabelArray[0];
            int checkNum = [checkLabel.text intValue];
            
            //パーフェクトかどうかの判別
            if(checkNum!=1000000){
                perfectFlg = NO;
            }
            
        }
    }
    
    
    //パーフェクトの場合、点数の加算と「パーフェクト」ラベル表示
    if(perfectFlg == YES){
        
        //「クリア」サウンド
        //[self soundsPlayAct:@"crear"];
        [self effectSound:@"crear"];
        
        [self crearLblAct:_perfectLbl];
        
        //得点２倍処理
        [self perfectPointAct];
        //レベルアップ
        [self levelUpAct];
        
        
    }else{
        
        //ライフオプションONの場合は処理を行う
        [self lifeNoClearAct];
    }
}

//*********************************
//７ブロックタップでアニメーションメソッド
//*********************************
-(void)tapSevenBlockAnimated:(UIView *)touchView{
    
    //点数の倍率を知らせるラベル生成
    //倍率ラベル生成
    UILabel *magLabel = [[UILabel alloc]init];
    magLabel.frame = touchView.bounds;
    //magLabel.frame = CGRectMake(touchView.frame.origin.x,touchView.frame.origin.y,10,10);
    magLabel.textAlignment = NSTextAlignmentCenter;
    magLabel.font = [UIFont fontWithName:@"Chalkboard SE" size:10];
    magLabel.textColor = [UIColor whiteColor];
    
    //ブロックからいい具合に少しずらす
    magLabel.center = CGPointMake(touchView.frame.origin.x+50,touchView.frame.origin.y+15);
    
    //点数配分を判別するフラグを生成します
    int calFlg = 0;
    
    //touchViewのtag判別
    if(touchView.tag ==1){
        calFlg = 0;
    }else if (touchView.tag == 2){
        magLabel.text = @"×2";
        calFlg = 2;
    }else if (touchView.tag == 3){
        magLabel.text = @"×3";
        calFlg = 3;
    }else{
        //一応かけ算も用意しておきます
         magLabel.text = @"×1";
        calFlg = 1;
    }
    
    
    //メインパネルにはりつけ
    [_mainPanel addSubview:magLabel];
    [_changeView bringSubviewToFront:magLabel];
    
    
    //拡大フェードアウトアニメーション
    [self magnificationUpAct:magLabel];
    
    [self performSelector:@selector(fadeout:)
               withObject:magLabel afterDelay:0.3];
    
    //最後は消滅
    magLabel = nil;
    
    /* アニメーション2（z軸を中心として回転） */
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    // 回転角度を設定
    animation.fromValue = [NSNumber numberWithFloat:0.0]; // 開始時の角度
    animation.toValue = [NSNumber numberWithFloat:4 * M_PI]; // 終了時の角度
    
    // アニメーションのオプションを設定
    animation.duration = 0.1; // アニメーション速度
    animation.repeatCount = 1; // 繰り返し回数
    
    
    // アニメーション
    [UIView animateWithDuration:0.15 // アニメーション速度0.3秒
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         // 画像消える
                         touchView.transform = CGAffineTransformMakeScale(0,0);
                         
                     } completion:nil];
    
    // アニメーションを追加
    [touchView.layer addAnimation:animation forKey:@"move-rotate-layer"];
    
    
    
    //得点の加算
    [self sumScoreAct:calFlg];
    
    
    //７を消した後もUNDOボタンは押せなくなります
    _undoView.backgroundColor = [UIColor grayColor];
    [_undoBtn setEnabled:NO];
}

//**********************************************
//四則演算子ボタンによる「７」ブロックの色分けと倍率加算
//**********************************************

-(void)sevenblockColor:(int)calFlg view:(UIView*)changeView{
    
    //点数配分とそれ関連のアクションは「Tag」で判別します
    if(calFlg==0){
        changeView.backgroundColor = [UIColor redColor];
        changeView.tag = 1;
    }else if (calFlg==1){
        changeView.backgroundColor = [UIColor blueColor];
        changeView.tag = 2;
    }else if (calFlg==2){
        //基本的にはここに入ることはありません
        changeView.backgroundColor = [UIColor greenColor];
    }else if (calFlg==3){
        changeView.backgroundColor = [UIColor yellowColor];
        changeView.tag = 3;
    }
    
}



#pragma mark - LimmitCountdownTimerSetting

//***********************
//制限時間ラベルを作成します
//***********************
-(void)addTimelimitLabel{
    
    //制限時間ラベルの生成
    //ラベル初期化、レイアウト設定
    timeLimitLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,-30,260,290)];
    timeLimitLabel.text = limittime_Str;
    timeLimitLabel.font = [UIFont fontWithName:@"Chalkboard SE" size:220];
    timeLimitLabel.textColor = [UIColor whiteColor];
    timeLimitLabel.textAlignment = NSTextAlignmentCenter;
    timeLimitLabel.alpha = 0.4f;
    
    //とりあえずここでは非表示
    //timeLimitLabel.hidden = YES;
    
    //self.viewに張付け
    [_mainPanel addSubview:timeLimitLabel];
    //メインパネルの一番背面に設定
    [_mainPanel sendSubviewToBack:timeLimitLabel];
    
}

-(void)countdownTimerOption{
    
    
    //制限時間ONの場合
    if(timelimiterInt == 1 || mode == 0 || mode == 2){
        
        //ラベル表示
        timeLimitLabel.hidden = NO;
    
        //タイマーストップ
        [countdownTimer invalidate];
        
        //カウントダウンタイマー再開
        [self editcountdownTimer];
        
    }else{
        
        //ラベル非表示
        timeLimitLabel.hidden = YES;
        //一応タイマー停止
        [countdownTimer invalidate];
        
        //timerを規定値に戻します
        timeLimitLabel.text = limittime_Str;
        
    }
}

//****************************************
//ステージクリアした場合はタイマーがストップ
//新規でタイマー起動
//****************************************
-(void)resetTimer{
    
    //制限時間ONの場合
    if(timelimiterInt == 1 || mode == 1 || mode == 2){
        //タイマーストップ
        [countdownTimer invalidate];
        countdownTimer = nil;
        
    
        //timerを規定値に戻します
        //カウントダウン10で赤文字なので、黒に戻す
        timeLimitLabel.textColor = [UIColor whiteColor];
        
        //タイマーリセット処理
        if(mode == 1 || mode == 2){
            timeLimitLabel.text = limittime_Str;
        }
        //タイマー実行
        [self editcountdownTimer];
    }
}


//*************************************
//タイムアタックの場合、ステージクリアで+10Sec
//タイムアタックモードのみ
//*************************************
-(void)limitPlusAct{
    
    //タイムアタックの場合
    if(mode == 0){
        //タイマーが+10秒
        //int型に変換
        int countNum = [timeLimitLabel.text intValue];
        //1秒ずつ減らしていく
        countNum+=10;
        //＋十秒ラベル
        [self plusfadeIn:_twoPointLbl];
        [self magnificationUpAct:_twoPointLbl];
        
        //ラベルにはりつけ
        timeLimitLabel.text = [NSString stringWithFormat:@"%d",countNum];
    }

}
//******************
//フェードイン(ラベル)
//******************
-(void)plusfadeIn:(UILabel *)fadeInLabel{
        
    //フェードイン
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.5];
    //目標のアルファ値を指定
    fadeInLabel.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];


}


// MARK: timeLimitTimer

//********************************
//制限時間のカウントダウン処理(タイマー)
//********************************

-(void)editcountdownTimer{
    //カウントダウンタイマー再起動
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0//タイマーを発生させる間隔(1.0秒)
                                                      target:self//メソッドがあるオブジェクト
                                                    selector:@selector(countdownTimerAct:)//呼び出すメソッド
                                                    userInfo:nil//メソッドに渡すパラメータ
                                                     repeats:YES];//繰り返し*/
}

-(void)countdownTimerAct:(NSTimer *)timer
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //ゲームオーバーにならない限りタイマーは止まらない
    if(timerState == YES){
        //int型に変換
        int countNum = [timeLimitLabel.text intValue];
        //1秒ずつ減らしていく
        countNum-=1;
        
        //ラベルにはりつけ
        timeLimitLabel.text = [NSString stringWithFormat:@"%d",countNum];
        
        if(mode == 0 || mode == 2){
            //ランキングモードの場合は
            if([timeLimitLabel.text isEqual:@"-1"]){
                //強制ゲームオーバーです
                [self viewdownGameOver];
                
            }else if(countNum <= 10){
                
                //カウントダウン残り10秒から
                //サイズ倍
                [self magnificationUpAct:timeLimitLabel];
                
                //ランキングモードの場合のみ
                if(appDelegate.modeFlg == 0 || appDelegate.modeFlg == 2){
                    [self RankingBGMSettingAct:@"lasttimeSecSound"];
                }
            }else{
                //ランキングモードの場合のみ
                if(appDelegate.modeFlg == 0 || appDelegate.modeFlg == 2){
                    [self RankingBGMSettingAct:@"timeSecSound"];
                }
            }

        }else{
        
            //タイマーが"0"になった場合(timerの停止)
            if([timeLimitLabel.text isEqual:@"0"]){
                
                [self freeTimeUp];
            }
        }
    }


}

#pragma mark - LimmitCalculationSetting

//*********************
//四則演算の制限機能
//*********************
-(void)settingOperator{
    
    //設定確認
    if(operatorInt == 1 || (mode == 2 && levelCnt > cngOperatorlimitLevel)){
        
    }else{
        ///四則演算の制限解除処理
        //ボタンに制限がかかっていた場合、解放
        minusFlg = YES;
        multipleFlg = YES;
        parFlg = YES;
    }
    
}


-(void)backcolorControl{
    
    //四則演算スイッチONの場合
    if(operatorInt == 1 || (mode == 2 && levelCnt > cngOperatorlimitLevel)){
        
        if(limitFlg == 1){
            //対象Viewの塗り潰しと戻し
            _minusView.backgroundColor = [UIColor grayColor];
        }else if(limitFlg == 2){
            _multipleView.backgroundColor = [UIColor grayColor];
        }else if(limitFlg == 3){
            _parView.backgroundColor = [UIColor grayColor];
        }
        
    }
}


//****************************
//四則演算子ボタンの制限と解除
//****************************
-(void)limitoperatorAct{
    
    if(calCnt == 0){
        firstUndoInt = calculationFlg;
        calCnt++;
    }else if(calCnt == 1){
        secondUndoInt = firstUndoInt;
        firstUndoInt = calculationFlg;
        calCnt++;
    }else{
        secondUndoInt = firstUndoInt;
        firstUndoInt = calculationFlg;
        calCnt = 1;
    }
    
    //ここは１つ前の制限フラグをたてるだけ！これで完成なのに。
    //１つ前の状態で制限がかかっていたものは戻します
    if(secondUndoInt == 0){
        DLog(@"１つ前の状態では何も制限はありませんでした");
    }else if(secondUndoInt == 1){
        DLog(@"１つ前の状態では引き算が制限でした");
    }else if(secondUndoInt == 2){
        DLog(@"１つ前の状態ではかけ算が制限でした");
    }else if (secondUndoInt == 3){
        DLog(@"１つ前の状態では割り算が制限でした");
    }
    
    
    
    //四則演算スイッチONの場合
    if(operatorInt == 1 || (mode == 2 && levelCnt > cngOperatorlimitLevel)){
        
        if(calculationFlg==0){
            //加算
            //ボタンに制限がかかっていた場合、解除
            minusFlg = YES;
            multipleFlg = YES;
            parFlg = YES;
            
            //対象Viewの戻し
            _minusView.backgroundColor = [UIColor whiteColor];
            _multipleView.backgroundColor = [UIColor whiteColor];
            _parView.backgroundColor = [UIColor whiteColor];
            
            //limitFlg = 0;
            
        }else if(calculationFlg == 1){
            //減算
            minusFlg = NO;
            multipleFlg = YES;
            parFlg = YES;
            
            //対象Viewの塗り潰しと戻し
            _minusView.backgroundColor = [UIColor grayColor];
            _multipleView.backgroundColor = [UIColor whiteColor];
            _parView.backgroundColor = [UIColor whiteColor];
            
            //limitFlg = 1;
            
        }else if(calculationFlg == 2){
            //かけ算
            minusFlg = YES;
            multipleFlg = NO;
            parFlg = YES;
            
            //対象Viewの塗り潰しと戻し
            _minusView.backgroundColor = [UIColor whiteColor];
            _multipleView.backgroundColor = [UIColor grayColor];
            _parView.backgroundColor = [UIColor whiteColor];
            
            //limitFlg = 2;
            
        }else if(calculationFlg == 3){
            //割り算
            minusFlg = YES;
            multipleFlg = YES;
            parFlg = NO;
            
            //対象Viewの塗り潰しと戻し
            _minusView.backgroundColor = [UIColor whiteColor];
            _multipleView.backgroundColor = [UIColor whiteColor];
            _parView.backgroundColor = [UIColor grayColor];
            
            //limitFlg = 3;
        }
        
    }


}

//******************************
//次のステージで四則演算制限のリセット
//******************************
-(void)resetOperatorbuttonAct{
    
    //四則演算スイッチONの場合
    if(operatorInt == 1 || (mode == 2 && levelCnt > cngOperatorlimitLevel)){
        
        calculationFlg=0;
        
        minusFlg = YES;
        multipleFlg = YES;
        parFlg = YES;
    
        //対象Viewの戻し
        _minusView.backgroundColor = [UIColor whiteColor];
        _multipleView.backgroundColor = [UIColor whiteColor];
        _parView.backgroundColor = [UIColor whiteColor];
    
        limitFlg = 0;
    }
    
}

//***********************************************
//計算できない場合、四則演算ボタンの一個前の制限状態に戻す
//***********************************************

-(void)noCalwithResetOperatorAct{
    
    //Undoのフラグも進めず維持
    
    firstUndoInt = secondUndoInt;
    
    //四則演算の制限状態は現状維持
    //四則演算子制限がONの場合
    if(operatorInt == 1 || (mode == 2 && levelCnt > cngOperatorlimitLevel)){
        
        //resetcalFlg => 直前の状態保持変数
        if(resetcalFlg==0){
            //加算
            //ボタンに制限がかかっていた場合、解除
            minusFlg = YES;
            multipleFlg = YES;
            parFlg = YES;
            
            //対象Viewの戻し
            _minusView.backgroundColor = [UIColor whiteColor];
            _multipleView.backgroundColor = [UIColor whiteColor];
            _parView.backgroundColor = [UIColor whiteColor];
            
            //前の状態のフラグ立て(色かえフラグ(使用不可の))
            limitFlg = 0;
            
            
        }else if(resetcalFlg == 1){
            //減算
            minusFlg = NO;
            multipleFlg = YES;
            parFlg = YES;
            
            //対象Viewの塗り潰しと戻し
            _minusView.backgroundColor = [UIColor grayColor];
            _multipleView.backgroundColor = [UIColor whiteColor];
            _parView.backgroundColor = [UIColor whiteColor];
            
            //前の状態のフラグ立て(色かえフラグ(使用不可の))
            limitFlg = 1;
            
        }else if(resetcalFlg == 2){
            //かけ算
            minusFlg = YES;
            multipleFlg = NO;
            parFlg = YES;
            
            //対象Viewの塗り潰しと戻し
            _minusView.backgroundColor = [UIColor whiteColor];
            _multipleView.backgroundColor = [UIColor grayColor];
            _parView.backgroundColor = [UIColor whiteColor];
            
            //前の状態のフラグ立て(色かえフラグ(使用不可の))
            limitFlg = 2;
            
        }else if(resetcalFlg == 3){
            //割り算
            minusFlg = YES;
            multipleFlg = YES;
            parFlg = NO;
            
            //対象Viewの塗り潰しと戻し
            _minusView.backgroundColor = [UIColor whiteColor];
            _multipleView.backgroundColor = [UIColor whiteColor];
            _parView.backgroundColor = [UIColor grayColor];
            
            //前の状態のフラグ立て(色かえフラグ(使用不可の))
            limitFlg = 3;
        }
        
    }

}


#pragma mark - limitLifePointSetting

-(void)settingLifePoint{
    
    if(lifepointInt == 1 || mode == 2){
        
        //タイムアタックモードの場合はライフ機能は使えません
        if(mode == 1 || mode == 2){
            //ライフ表示
            _life1.hidden = NO;
            _life2.hidden = NO;
            _life3.hidden = NO;
            
            //とりあえず今はライフを戻す(よろしくないやり方)
            [self fadeInImage:_life1];
            [self fadeInImage:_life2];
            [self fadeInImage:_life3];
            
            _life1.alpha = 0.8;
            _life2.alpha = 0.8;
            _life3.alpha = 0.8;
        
            //ライフポイントラベルの格納(初期化)
            _lifePntArray = [[NSArray alloc]initWithObjects:_life1,_life2,_life3,nil];
            
        }
        
    }else{
        
        //ライフ表示
        _life1.hidden = YES;
        _life2.hidden = YES;
        _life3.hidden = YES;
        
        //ライフカウントも戻します
        notClearCnt = 0;
        
    }
    
}


-(void)lifeNoClearAct{
    
    if(lifepointInt == 1 || mode == 2){
    
        //ライフが一つずつ削られます
        //カウント３つで「げーむおーばー」
        if(notClearCnt !=3){
        
            UILabel *clearLbl = _lifePntArray[notClearCnt];
        
            //viewを倍にしながら...
            [self magnificationUpAct:clearLbl];
            //そのままフェードあうと！
            // 引数が1つのメソッドを遅延実行する
            [self performSelector:@selector(fadeout:)
                       withObject:clearLbl afterDelay:0.3];
            
            //クリアできない場合、カウントされていきます。
            notClearCnt++;
            
        }else{
            
            //ゲームオーバーの場合
            [self viewdownGameOver];
        
            //とりあえず今はライフを戻す(よろしくないやり方)
            [self fadeInImage:_life1];
            [self fadeInImage:_life2];
            [self fadeInImage:_life3];
        
            //ライフカウントも戻します
            notClearCnt = 0;
        
            //得点も0に戻します
            _totalScore.text = @"0";
        }
        
    }

}


#pragma mark - limitMaskBlocksSetting

-(void)settingMaskBlocks{
    
    if((maskblocksInt==1 && mode == 1) || (mode == 2 && levelCnt > cngMaskLevel)){
    
        //タイムアタックモードの場合は使えません
        //フリーモードのみ
        if(mode == 1 || mode == 2){
            int maskblocknum = 0;
            
            //マスク処理中はタッチイベント不可フラグ
            maskNottouchFlg = NO;
            
            //メモリー中メッセージ表示
            if(maskblocksInt ==1  && mode == 1){
                [self fadeIn:_maskMemory];
            }else if((mode == 2 && levelCnt > cngMaskLevel)){
                [self fadeIn:_maskMemory];
            }
    
            //3秒後１列１ブロックマスクをかける
            while (maskblocknum < mainFrame_height_Blocks) {
        
                //マスクをかける配列番号取得(ランダム)
                int blockNum = [self getRandamInt:RANDOM_NUMBER_MIN-1 max:RANDOM_NUMBER_MAX-2];
                //マスク対象View代入用
                UIView *maskView = [[UIView alloc]init];
        
                if(maskblocknum==0){
                    maskView = _numberObjects[blockNum];
                }else if (maskblocknum==1){
                    maskView = _numberObjects1[blockNum];
                }else if (maskblocknum==2){
                    maskView = _numberObjects2[blockNum];
                }else if (maskblocknum==3){
                    maskView = _numberObjects3[blockNum];
                }
        
                //選択したViewの数値取得
                NSArray *checkLabelArray = maskView.subviews;
                UILabel *checkLabel = checkLabelArray[0];
            
                [self performSelector:@selector(maskFadeout:)
                           withObject:maskView afterDelay:3.0];
            
                [self performSelector:@selector(fadeout:)
                           withObject:checkLabel afterDelay:3.0];
        
                maskblocknum++;
        
            }
        }
    }

}


//*******************
//マスク用フェードアウト
//*******************
-(void)maskFadeout:(UIView *)fadeoutView{

    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.5];
    //目標のアルファ値を指定
    fadeoutView.alpha = maskAlpha;
    //アニメーション実行
    [UIView commitAnimations];
    
    //マスク処理中はタッチイベント不可フラグ
    maskNottouchFlg = YES;
    
    //マスク処理が終わったらメモリー中メッセージも消します
    //メモリー中メッセージ表示
    if(maskblocksInt ==1  && mode == 1){
        [self fadeout:_maskMemory];
    }else if((mode == 2 && levelCnt > cngMaskLevel)){
        [self fadeout:_maskMemory];
    }

    
}

#pragma mark - Animated

//******************************************
//viewのサイズを倍にするアニメーション処理メソッド
//******************************************
-(void)magnificationUpAct:(UILabel *)magUpLbl{
    // 拡大縮小を設定
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    // アニメーションのオプションを設定
    animation.duration = 0.8; // アニメーション速度
    animation.repeatCount = 1; // 繰り返し回数
    animation.autoreverses = YES; // アニメーション終了時に逆アニメーション
    
    // 拡大・縮小倍率を設定
    animation.fromValue = [NSNumber numberWithFloat:1.0]; // 開始時の倍率
    animation.toValue = [NSNumber numberWithFloat:2.0]; // 終了時の倍率
    
    // アニメーションを追加
    [magUpLbl.layer addAnimation:animation forKey:@"scale-layer"];
}


//******************
//フェードイン(ラベル)
//******************
-(void)fadeIn:(UILabel *)fadeInLabel{
    
    //フェードイン
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.3];
    //目標のアルファ値を指定
    fadeInLabel.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
}

//*******************
//フェードアウト(ラベル)
//*******************
-(void)fadeout:(UILabel *)fadeoutLbl{
    
    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.2];
    //目標のアルファ値を指定
    fadeoutLbl.alpha = 0;
    //アニメーション実行
    [UIView commitAnimations];
}


//******************
//フェードイン(画像)
//******************
-(void)fadeInImage:(UIImageView *)fadeInImage{
    
    //フェードイン
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.3];
    //目標のアルファ値を指定
    fadeInImage.alpha = 1;

    //アニメーション実行
    [UIView commitAnimations];
}

//*******************
//フェードアウト(画像)
//*******************
-(void)fadeoutImage:(UIImageView *)fadeoutImage{
    
    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.2];
    //目標のアルファ値を指定
    fadeoutImage.alpha = 0;
    //アニメーション実行
    [UIView commitAnimations];
}

#pragma mark - ActionButton

- (IBAction)backMenuBtnAct:(id)sender {
    
    //alpha設定
    _operatorImage.alpha = 0;
    _operatorImageMsg.alpha = 0;
    _maskImage.alpha = 0;
    _maskImageMsg.alpha = 0;
    
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    //戻る音
    backAudio = [soundsUtils backMenuSounds];
    [backAudio play];
    //BGMもフェードアウトしながら消える
    [soundsUtils doVolumeFade:Audio];
    
    
    //この画面からメニュー画面に戻る際に広告フラグをたてる
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    appDelegate.splashAdFlg = YES;
    
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.8;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    //transition.subtype = kCATransitionFromBottom;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    //タイマー停止
    [countdownTimer invalidate];
    countdownTimer = nil;
    
    //一つ前の画面に戻る
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// MARK: calculationAction

//*******************
//マイナスボタン
//ActionMethod
//*******************

- (IBAction)minusAct:(id)sender {
    
    if(minusFlg == YES){
        //四則演算クリック音
        [self effectSound:@"calculationSound"];
    
        _minusView.backgroundColor = [UIColor blueColor];
        _multipleView.backgroundColor = [UIColor whiteColor];
        _parView.backgroundColor = [UIColor whiteColor];
    
        //四則演算設定がONの場合のBackColor制御
        [self backcolorControl];
    
        if(calculationFlg == 0 ||calculationFlg == 2 || calculationFlg == 3){
            //フラグ立て
            calculationFlg=1;
        }else{
            calculationFlg=0;
            _minusView.backgroundColor = [UIColor whiteColor];
        }
    }else{
        //押せませんの音
        [self effectSound:@"notcalplay"];
    }
    
}

//*******************
//かけるボタン
//ActionMethod
//*******************

- (IBAction)multipleAct:(id)sender {
    
    if(multipleFlg == YES){
        //四則演算クリック音
        [self effectSound:@"calculationSound"];
    
        //選択色付け
        _multipleView.backgroundColor = [UIColor greenColor];
        _minusView.backgroundColor = [UIColor whiteColor];
        _parView.backgroundColor = [UIColor whiteColor];
    
        //四則演算設定がONの場合のBackColor制御
        [self backcolorControl];
    
        if(calculationFlg == 0 || calculationFlg == 1 || calculationFlg == 3){
            //フラグ立て
            calculationFlg=2;
        }else{
            calculationFlg=0;
            _multipleView.backgroundColor = [UIColor whiteColor];
        }
    }else{
        //押せませんの音
        [self effectSound:@"notcalplay"];
    }
}
//*******************
//わるボタン
//ActionMethod
//*******************

- (IBAction)parAct:(id)sender {
    
    
    if(parFlg == YES){
    
        //四則演算クリック音
        [self effectSound:@"calculationSound"];
    
        //選択色付け
        _parView.backgroundColor = [UIColor yellowColor];
        _minusView.backgroundColor = [UIColor whiteColor];
        _multipleView.backgroundColor = [UIColor whiteColor];
    
        //四則演算設定がONの場合のBackColor制御
        [self backcolorControl];
    
        if(calculationFlg == 0 || calculationFlg == 2 || calculationFlg == 1){
            //フラグ立て
            calculationFlg=3;
        }else{
            calculationFlg=0;
            _parView.backgroundColor = [UIColor whiteColor];
        }
    }else{
        //押せませんの音
        [self effectSound:@"notcalplay"];
    }
}


#pragma mark - NSUserDefaults

-(void)settingNSUserDefaults{
    
    
    //設定変更毎に設定の読み込み
    _timeLimiterUD = [NSUserDefaults standardUserDefaults];// 取得
    timelimiterInt = [_timeLimiterUD integerForKey:@"KEY_Timelimiter"];  // KEY_Iの内容をint型として取得
    
    
    if(mode == 1){
        //設定の読み込み
        _operatorUD = [NSUserDefaults standardUserDefaults];// 取得
        operatorInt = [_operatorUD integerForKey:@"KEY_Operator"];  // KEY_Iの内容をint型として取得
    }else{
        //通常モードは四則演算制限はありません
        operatorInt = 0;
    }
    
    
    _lifepointUD = [NSUserDefaults standardUserDefaults];// 取得
    lifepointInt = [_lifepointUD integerForKey:@"KEY_Lifepoint"];
    
    _maskBlocksUD = [NSUserDefaults standardUserDefaults];// 取得
    maskblocksInt = [_maskBlocksUD integerForKey:@"KEY_Maskblocks"];
    
}

#pragma mark - PushSegueAnimated

//**********************************************
//ゲームオーバになった際に上から落ちてくるView(結果表示)
//**********************************************
-(void)viewdownGameOver{
    
    
    ScoreViewController *scoreView = [self.storyboard instantiateViewControllerWithIdentifier:@"ScoreViewController"];
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.8;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    //transition.subtype = kCATransitionFromBottom;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:scoreView animated:NO];
    
    //タイマーよとまれ！！
    timerState = NO;
    
}

#pragma mark - AppDelegate

//*******************************
//スコアレベルをAppDelegateに代入する
//*******************************

-(void)appdelegateScoreLevel:(int)levelNum{
    
    //appDelegate変数に代入しておく(レベルスコア)
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //現在のレベル-1を代入する
    if(levelCnt==0){
        appDelegate.levelDate = 0;
    }else{
        appDelegate.levelDate = levelCnt;
    }
}

//*******************************
//スコアレベルをAppDelegateに代入する
//*******************************

-(void)appdelegateScore:(int)scoreNum{
    
    //appDelegate変数に代入しておく(レベルスコア)
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    appDelegate.scoreDate = scoreNum;
    
}

#pragma mark - Design and Sound

//****************************
//効果音を出す
//****************************
-(void)soundsPlayAct:(NSString *)sounds{
    // サウンドの準備
    NSString *path = [[NSBundle mainBundle] pathForResource:sounds ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain(url), &sound);
    
    // サウンドの再生
    AudioServicesPlaySystemSound(sound);
}

//**********************************
//その他効果音
//**********************************
-(void)effectSound:(NSString *)soundNameStr{
    
    //いずれは色毎に段階をつけていく
    SoundsUtils *soundUtils = [[SoundsUtils alloc]init];
    effectAudio = [soundUtils effectSounds:soundNameStr];
    [effectAudio play];
}


//******************************
//ランキングモードBGMの設定
//******************************
-(void)RankingBGMSettingAct:(NSString *)soundName{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //BGMの設定
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    
    Audio =[soundsUtils RankingbgmSoundsSetting:soundName];
    [Audio play];
    //DLog(@"カチッ");
    
    //BGMが格納されている変数をAppdelegateに
    appDelegate.AppBGMSound = Audio;
    
}

//******************************
//フリーモードBGMの設定
//******************************
-(void)FreeBGMSettingAct{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //BGMの設定
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    
    Audio = [soundsUtils bgmSoundsSetting];
    [Audio play];
    
    //BGMが格納されている変数をAppdelegateに
    appDelegate.AppBGMSound = Audio;
    
}


//******************************
//背景の設定
//******************************
-(void)designSettingAct{
    
    //背景画像の設定(カメラロール)
    NSUserDefaults *_backImageUD = [NSUserDefaults standardUserDefaults];
    NSData *data = [_backImageUD objectForKey:@"KEY_BackImage"];
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    
    //背景
    NSUserDefaults *_segmentUD = [NSUserDefaults standardUserDefaults];
    int segmentInt = [_segmentUD integerForKey:@"KEY_Segment"];
    
    if(segmentInt == 0){
        [designUtils backImageSettingAct:self.view];
    }else{
        [designUtils backImageCameraSettingAct:self.view image:data];
    }
}

//**********************************
//７を作ったときの効果音(wavファイル)
//引数「createBlockView」->色判別に使う
//**********************************
-(void)createSevenSound:(UIView *)createBlockView{
    
   //いずれは色毎に段階をつけていく
    SoundsUtils *soundUtils = [[SoundsUtils alloc]init];
    
    switch (seventapBlockCnt) {
        case 0:
            tapAudio1 = [soundUtils createSevenBlocksSound:createBlockView];
            [tapAudio1 play];
            break;
        case 1:
            tapAudio2 = [soundUtils createSevenBlocksSound:createBlockView];
            [tapAudio2 play];
            break;
        case 2:
            tapAudio3 = [soundUtils createSevenBlocksSound:createBlockView];
            [tapAudio3 play];
            break;
        case 3:
            tapAudio4 = [soundUtils createSevenBlocksSound:createBlockView];
            [tapAudio4 play];
            break;
        case 4:
            tapAudio5 = [soundUtils createSevenBlocksSound:createBlockView];
            [tapAudio5 play];
            break;
        case 5:
            tapAudio6 = [soundUtils createSevenBlocksSound:createBlockView];
            [tapAudio6 play];
            break;
        case 6:
            tapAudio7 = [soundUtils createSevenBlocksSound:createBlockView];
            [tapAudio7 play];
            break;
        default:
            tapAudio8 = [soundUtils createSevenBlocksSound:createBlockView];
            [tapAudio8 play];
            break;
    }
    
    if(seventapBlockCnt != 8){
        seventapBlockCnt++;
    }else{
        //最後まで行ったら初期かします。
        seventapBlockCnt=0;
    }
}

//****************************
//ブロックタッチの音階処理
//****************************
-(void)tapBlockSound:(UIView *)soundView{
    
    SoundsUtils *soundUtils = [[SoundsUtils alloc]init];
 
    switch (tapBlockCnt) {
        case 0:
            tapAudioDo = [soundUtils tapBlocksSound:_tapBlockArray[tapBlockCnt]];
            [tapAudioDo play];
            break;
        case 1:
            tapAudioRe = [soundUtils tapBlocksSound:_tapBlockArray[tapBlockCnt]];
            [tapAudioRe play];
            break;
        case 2:
            tapAudioMi = [soundUtils tapBlocksSound:_tapBlockArray[tapBlockCnt]];
            [tapAudioMi play];
            break;
        case 3:
            tapAudioFa = [soundUtils tapBlocksSound:_tapBlockArray[tapBlockCnt]];
            [tapAudioFa play];
            break;
        case 4:
            tapAudioSo = [soundUtils tapBlocksSound:_tapBlockArray[tapBlockCnt]];
            [tapAudioSo play];
            break;
        case 5:
            tapAudioRa = [soundUtils tapBlocksSound:_tapBlockArray[tapBlockCnt]];
            [tapAudioRa play];
            break;
        case 6:
            tapAudioSi = [soundUtils tapBlocksSound:_tapBlockArray[tapBlockCnt]];
            [tapAudioSi play];
            break;
        default:
            tapAudioDo2 = [soundUtils tapBlocksSound:_tapBlockArray[tapBlockCnt]];
            [tapAudioDo2 play];
            break;
    }
    
    if(tapBlockCnt != 7){
        tapBlockCnt++;
    }else{
        //最後まで行ったら初期かします。
        tapBlockCnt=0;
    }
    
}


#pragma mark - modeFlg


//現在のモードを確認する
-(int)modeDistinction{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    int modeInt = appDelegate.modeFlg;
    
    return modeInt;
    
}

//表示する数字の範囲（モード別）
-(NSString *)modeNumberSphere{
    
    //乱数格納変数
    int blockNum;
    NSString *randomStr;
    
    //乱数発生メソッドの呼び出し
   if(mode == 1 || mode == 2){
        
        //3の倍数ずつ桁上げ(blockmaxFlg->この処理が何回も通るので一回だけ(頭ぐだぐだの状態で作っちゃったやつ))
        if(levelCnt%carryNum == 0 && blockmaxFlg == YES){
            blockMaxInt += countUpNum ;
            DLog(@"%d",blockMaxInt);
            blockmaxFlg = NO;
        }
        
        blockNum = [self getRandamInt:RANDOM_NUMBER_MIN max:blockMaxInt];
        randomStr = [NSString stringWithFormat:@"%d",blockNum];
        
    }else{
        blockNum = [self getRandamInt:RANDOM_NUMBER_MIN max:RANDOM_NUMBER_MAX];
        randomStr = [NSString stringWithFormat:@"%d",blockNum];
    }
    
    return randomStr;
    
}

//通常モードの場合レベル７でゲームオーバーです
//カウント0でゲームオーバーです
-(void)nomalSevenLebelAct{
    
    
    //レベル値キャスト,代入
    _levelLbl.text = [NSString stringWithFormat:@"%d",levelCnt];
    
    
    //レベル7クリアで強制ゲームオーバー
    /*if(mode == 1){
        if(levelCnt == gameoverCnt){
            [self viewdownGameOver];
        }
    }*/
}

//************************************
//通常モードは制限時間を越えると即終了です
//************************************
-(void)freeTimeUp{
    
        //赤文字です
        //timeLimitLabel.textColor = [UIColor redColor];
        
        //タイマー停止
        [countdownTimer invalidate];
        
        //ブロックの再生成
        [self recreateBlockAct];
}

#pragma mark - Undo


//*****************************
//undoボタンで１つ前の状態に戻る
//*****************************
- (IBAction)undoBtnAct:(id)sender {
    
    
    if([_undoBtn isEnabled]){
    
        //四則演算に制限がかかっている場合は
        //演算制御も前の状態に戻します。
        [self undolimitOperator];
        
        //音
        SoundsUtils *soundUtils = [[SoundsUtils alloc]init];
        undoSound = [soundUtils effectSounds:@"selectBlock"];
        [undoSound play];
        
        //最初にタッチしたブロックを復元します
        ////////////////////////////////
        _selectView.frame = CGRectMake(originSelectX,originSelectY,OBJECT_SIZE,OBJECT_SIZE);
        [_selectView.layer setBorderColor:[UIColor blackColor].CGColor];
        [_selectView.layer setBorderWidth:1.0];
        _selectView.layer.cornerRadius = 18;
        _selectView.layer.shadowOffset = CGSizeMake(2, 2);
        _selectView.backgroundColor = [[UIColor whiteColor]init];
        //１回目に選択したブロックのAlpha値を入れます(マスク対策)
        _selectView.alpha = undoSelectAlpha;
        
        //修正(マスク処理対策)
        //ブロックの数字ラベルを取り出し、削除
        NSArray *selectLabelArray = _selectView.subviews;
        selectLabel = selectLabelArray[0];
        [selectLabel removeFromSuperview];
        //削除後前の数字ラベルを貼付けます
        UILabel *selectNumber = [[UILabel alloc]init];
        selectNumber.frame = numberObj.bounds;
        selectNumber.textAlignment = NSTextAlignmentCenter;
        selectNumber.text = [NSString stringWithFormat:@"%d",undoSelectNum];
        selectNumber.font = [UIFont fontWithName:@"Chalkboard SE" size:25];
        [_selectView addSubview:selectNumber];
        
        //もしもundoの対象がMaskがかかっているものなら、ラベルは消す！
        if(undoSelectAlpha==maskAlpha){
            [self fadeout:selectNumber];
        }
        
        
        [_mainPanel addSubview:_selectView];
        //２番目にタッチされたViewはメインフレ-むの上を移動する
        [self.view bringSubviewToFront:_changeView];
    
        //内部的に登録
        if(selectArrayFirstRow==0){
            _numberObjects[selectArrayFirstCol]=_selectView;
        }else if(selectArrayFirstRow==1){
            _numberObjects1[selectArrayFirstCol]=_selectView;
        }else if(selectArrayFirstRow==2){
            _numberObjects2[selectArrayFirstCol]=_selectView;
        }else if(selectArrayFirstRow==3){
            _numberObjects3[selectArrayFirstCol]=_selectView;
        }
    
        //加算を行ったブロックの修正
        //ブロックの数字ラベルを取り出し、削除
        NSArray *changeLabelArray = _changeView.subviews;
        changeLabel = changeLabelArray[0];
        [changeLabel removeFromSuperview];
        //削除後前の数字ラベルを貼付けます
        UILabel *changeNumber = [[UILabel alloc]init];
        changeNumber.frame = numberObj.bounds;
        changeNumber.textAlignment = NSTextAlignmentCenter;
        changeNumber.text = [NSString stringWithFormat:@"%d",undoChangeNum];
        changeNumber.font = [UIFont fontWithName:@"Chalkboard SE" size:25];
        [_changeView addSubview:changeNumber];
        //ブロックデザインもちょっと修正
        _changeView.backgroundColor = [UIColor whiteColor];
        [_changeView.layer setBorderColor:[UIColor blackColor].CGColor];
        [_changeView.layer setBorderWidth:1.0];
        //２回目に選択したブロックのAlpha値を入れます(マスク対策)
        _changeView.alpha = undoChangeAlpha;
        
        //もしもundoの対象がMaskがかかっているものなら、ラベルは消す！
        if(undoChangeAlpha==maskAlpha){
            [self fadeout:changeNumber];
        }
    
        //selectViewが元の場所に戻ります
        _selectView.frame = CGRectMake(_changeView.frame.origin.x,_changeView.frame.origin.y,OBJECT_SIZE,OBJECT_SIZE);
    
        [UIView animateWithDuration:0.3
                         animations:^{
                             [_selectView setFrame:CGRectMake(originSelectX,originSelectY,OBJECT_SIZE,OBJECT_SIZE)];
                         }];

        //changeViewはくるっと回転
        //裏返すアニメーションの設定
        [ UIView beginAnimations: @"TransitionAnimation" context:nil ];
        [ UIView setAnimationTransition: UIViewAnimationTransitionFlipFromRight
                                forView:_changeView
                                  cache:YES];
        // アニメーションを開始
        [ UIView commitAnimations];
        
        //ボタンが押せなくなったら、Viewも暗くします
        _undoView.backgroundColor = [UIColor grayColor];
        [_undoBtn setEnabled:NO];
        
    }

}

//*******************************************
//四則演算の制限がかかっている場合、前の状態に戻します
//*******************************************
-(void)undolimitOperator{
    
    //0->加算,1->減算,2->乗算,3->除算
    //////////////////////////////
    
    DLog(@"現在の四則演算の状態->%d",resetcalFlg);
    
    //四則演算の制限がかかっている場合、制限も前の状態に戻します
    if(operatorInt == 1 || (mode == 2 && levelCnt > cngOperatorlimitLevel)){
        //四則演算に制限がかかっている場合は元に戻します
        if(resetcalFlg == 1){
            minusFlg = YES;
            _minusView.backgroundColor = [UIColor whiteColor];
        }else if (resetcalFlg == 2){
            multipleFlg = YES;
            _multipleView.backgroundColor = [UIColor whiteColor];
        }else if(resetcalFlg == 3){
            parFlg = YES;
            _parView.backgroundColor = [UIColor whiteColor];
        }
        
        //ここは１つ前の制限フラグをたてるだけ！これで完成なのに。
        //１つ前の状態で制限がかかっていたものは戻します
        if(secondUndoInt == 0){
            //DLog(@"１つ前の状態では何も制限はありませんでした");
            limitFlg = 0;
        }else if(secondUndoInt == 1){
            //DLog(@"１つ前の状態では引き算が制限でした");
            minusFlg = NO;
            _minusView.backgroundColor = [UIColor grayColor];
            limitFlg = 1;
        }else if(secondUndoInt == 2){
            //DLog(@"１つ前の状態ではかけ算が制限でした");
            multipleFlg = NO;
            _multipleView.backgroundColor = [UIColor grayColor];
            limitFlg = 2;
        }else if (secondUndoInt == 3){
            //DLog(@"１つ前の状態では割り算が制限でした");
            parFlg = NO;
            _parView.backgroundColor = [UIColor grayColor];
            limitFlg = 3;
        }
        
        firstUndoInt = secondUndoInt;
        
    }

    
}


# pragma mark -Challenge Mode

-(void)challengemodeAnimated{
    
    //チャレンジモードでレベルが上がった場合はラベルでお知らせ
    //四則演算制限レベル到達
    if(mode == 2 && levelCnt > cngOperatorlimitLevel && cngOperatorFlg == YES){
        [self fadeIn:_operatorMsg];
        
        //一回この処理を通ったら、もう通りません
        cngOperatorFlg = NO;
        
        //alpha設定
        _operatorImage.alpha = 0.85f;
        _operatorImageMsg.alpha = 1.0f;
        
        //にょきっと表示
        [UIView animateWithDuration:1.0
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             _operatorImage.frame = CGRectMake(0,_operatorImage.frame.origin.y,_operatorImage.frame.size.width,_operatorImage.frame.size.height);
                         } completion:nil];
    
        [UIView animateWithDuration:1.0
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             _operatorImageMsg.frame = CGRectMake(21,_operatorImageMsg.frame.origin.y,_operatorImageMsg.frame.size.width,_operatorImageMsg.frame.size.height);
                         } completion:nil];
        
        [self performSelector:@selector(OperatorbackAnimated)
                   withObject:nil afterDelay:3.0];
        
            }

    //マスク処理レベル到達
    if(mode == 2 && levelCnt > cngMaskLevel && cngMaskFlg == YES){
        [self fadeIn:_maskMsg];
        
        //一回この処理を通ったら、もう通りません
        cngMaskFlg = NO;
        
        //alpha設定
        _maskImage.alpha = 0.85f;
        _maskImageMsg.alpha = 1.0f;
        
        
        [UIView animateWithDuration:1.0
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             _maskImage.frame = CGRectMake(0,_maskImage.frame.origin.y,_maskImage.frame.size.width,_maskImage.frame.size.height);
                         } completion:nil];
        
        [UIView animateWithDuration:1.0
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             _maskImageMsg.frame = CGRectMake(18,_maskImageMsg.frame.origin.y,_maskImageMsg.frame.size.width,_maskImageMsg.frame.size.height);
                         } completion:nil];
        
        
        [self performSelector:@selector(maskbackAnimated)
                   withObject:nil afterDelay:3.0];
        
    }
    
}

-(void)OperatorbackAnimated{
    
    
    //シュシュっと戻す
    [UIView animateWithDuration:1.0
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _operatorImage.frame = CGRectMake(originOpImageX,_operatorImage.frame.origin.y,_operatorImage.frame.size.width,_operatorImage.frame.size.height);
                     } completion:nil];
    
    [UIView animateWithDuration:1.0
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _operatorImageMsg.frame = CGRectMake(originOpImageMsgX,_operatorImageMsg.frame.origin.y,_operatorImageMsg.frame.size.width,_operatorImageMsg.frame.size.height);
                     } completion:nil];
    

}

-(void)maskbackAnimated{
    
    //シュシュっと戻す
    [UIView animateWithDuration:1.0
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _maskImage.frame = CGRectMake(originMaskImageX,_maskImage.frame.origin.y,_maskImage.frame.size.width,_maskImage.frame.size.height);
                     } completion:nil];
    
    [UIView animateWithDuration:1.0
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _maskImageMsg.frame = CGRectMake(originMaskImageMsgX,_maskImageMsg.frame.origin.y,_maskImageMsg.frame.size.width,_maskImageMsg.frame.size.height);
                     } completion:nil];

}


@end
