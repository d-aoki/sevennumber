//
//  TutorialMainViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/24.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "TutorialMainViewController.h"
#import "DesignUtils.h"
#import "SoundsUtils.h"
#import "ProcessingViewUtils.h"

@interface TutorialMainViewController ()

@end

@implementation TutorialMainViewController

{
    AVAudioPlayer *backAudio,*touchSound;
    
    //画面遷移させるためのタッチ変数
    CGPoint _movePageTouch;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //メッセージラベルのデザイン
    NSArray *array = [[NSArray alloc]initWithObjects:_message1,_message2,_message3, nil];
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadow:array];
    
    
    //デフォルト背景
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    //[designUtils gradation:self.view];
    [designUtils backImageSettingAct:self.view];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Touch Event

//********************************************
//画面がタッチされたら、次のページに遷移する用にします
//********************************************
-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    _movePageTouch = [touch locationInView:self.view];
    
    DLog(@"ページがタッチされました。ページ遷移します");
    
    //インスタンス
    UIViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialSecondViewController"];
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:view animated:NO];
    
}

//*************************
//トップに戻ります
//*************************
- (IBAction)backBtnAct:(id)sender {
    
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    //戻る音
    backAudio = [soundsUtils backMenuSounds];
    [backAudio play];
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.8;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    //transition.subtype = kCATransitionFromBottom;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    //一つ前の画面に戻る
    [self.navigationController popToRootViewControllerAnimated:YES];


}
@end
