//
//  PlayerNameEditViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/18.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "PlayerNameEditViewController.h"
#import "AppDelegate.h"
#import "RankingUtils.h"
#import "ScoreViewController.h"
#import "ProcessingViewUtils.h"

#import "SoundsUtils.h"


//テキストフィールドの入力最大文字数
#define MAX_LENGTH 8

//プレイヤー名のデフォルト
NSString* const defaultPlayerName = @"Player1";
//キーボードの高さ(216から微調整)
float const keyBoardHeight = 156;

@interface PlayerNameEditViewController ()

@end

@implementation PlayerNameEditViewController
{
    NSString *playerName;
    
    NSUserDefaults *playerNameUD;
    
    AVAudioPlayer *selectSound;
    
    //キーボードの高さ取得
    float keyboardH;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //AppDelegate設定
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    //モードの判別
    //適したタイトルを入れます
    if(appDelegate.modeFlg == 0){
        _modeTitleLabel.text = @"TimeAttack Mode";
    }else{
        _modeTitleLabel.text = @"Challenge Mode";
    }
    
    
    //フォントデザインの切り替え
    NSArray *array = [[NSArray alloc]initWithObjects:_prizeMsg,_congMsg,_modeTitleLabel, nil];
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadow:array];
    
    
    //delegate設定
    _PlayerNTextBox.delegate = self;
    
    //入賞メッセージ
    _prizeMsg.text = appDelegate.prizeMsg;

    //前にプレイヤー名を入力している場合はその名前をテキストのデフォ設定にしておく
    playerNameUD = [NSUserDefaults standardUserDefaults];
    NSString *player = [playerNameUD valueForKey:@"KEY_Player"];
    DLog(@"%@",player);
    
    if(![player isEqual:@""]){
        _PlayerNTextBox.text = player;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//**********************************************
//ランキングに登録するプレイヤー名が設定のままで良い場合
//この画面を閉じます
//**********************************************
- (IBAction)DoneBtn:(id)sender {
    
    //音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSound = [soundsUtils effectSounds:@"click"];
    [selectSound play];
    
    if(![_PlayerNTextBox.text isEqual:@""]){
        playerName = _PlayerNTextBox.text;
        DLog(@"%@が入力されました",playerName);
    }
    
    ScoreViewController *scoreView = [[ScoreViewController alloc]init];
    [scoreView closePickerView:self];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //textFieldに入力されていない場合は「player1」
    //入力した場合はその名前でランキングに登録されます
    if([_PlayerNTextBox.text isEqual:@""]){
        DLog(@"名前が入力されていないため、「player1」で登録します");
        appDelegate.playerNameStr = defaultPlayerName;
        DLog(@"%@ = %@",appDelegate.playerNameStr,defaultPlayerName);
    }else{
        appDelegate.playerNameStr = playerName;
        
        //次からは現在入力されたテキストが入ります(NSUserDefault)
        playerNameUD = [NSUserDefaults standardUserDefaults];
        [playerNameUD setValue:playerName forKey:@"KEY_Player"];
        [playerNameUD synchronize];
    }
    
    //点数でランキングの順位付けアルゴリズム
    RankingUtils *rankUtils = [[RankingUtils alloc]init];
    
    //適当なラベルに
    UILabel *label = [[UILabel alloc]init];
    label.text = [NSString stringWithFormat:@"%d",appDelegate.scoreDate];
    [rankUtils rankingRegist:label modeFlg:appDelegate.modeFlg];
    
}

#pragma mark - textField Delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    // すでに入力されているテキストを取得
    NSMutableString *text = [textField.text mutableCopy];
    
    // すでに入力されているテキストに今回編集されたテキストをマージ
    [text replaceCharactersInRange:range withString:string];
    
    // 結果が文字数をオーバーしていないならYES，オーバーしている場合はNO
    return ([text length] <= MAX_LENGTH);
    
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    DLog(@"入力します");
    DLog(@"%f",keyboardH);
    //入力欄が隠れないように入力Viewもずらします
    [UIView animateWithDuration:0.3 // アニメーション速度0.3秒
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         //キーボードの高さ分上げる
                         _congView.frame = CGRectMake(_congView.frame.origin.x,_congView.frame.origin.y-keyBoardHeight,_congView.frame.size.width,_congView.frame.size.height);
                     } completion:nil];
    

    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if(![_PlayerNTextBox.text isEqual:@""]){
        playerName = _PlayerNTextBox.text;
        DLog(@"%@が入力されました",playerName);
    }
    
    //入力欄が隠れないように入力Viewもずらします
    [UIView animateWithDuration:0.15 // アニメーション速度0.3秒
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         //キーボードの高さ分上げる
                         _congView.frame = CGRectMake(_congView.frame.origin.x,_congView.frame.origin.y+keyBoardHeight,_congView.frame.size.width,_congView.frame.size.height);
                     } completion:nil];

    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - KeyBoard

-(void)keyboardWillShow:(NSNotification*)note
{
    // キーボードの表示完了時の場所と大きさを取得します。
    CGRect keyboardFrameEnd = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardH = keyboardFrameEnd.size.height;
    
    DLog(@"%f",keyboardH);
    
}

@end
