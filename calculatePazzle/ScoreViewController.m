//
//  ScoreViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/03.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "ScoreViewController.h"
#import "MainViewController.h"
#import "ViewController.h"
#import "DCSocial.h"
#import "AppDelegate.h"
#import "SoundsUtils.h"
#import "ProcessingViewUtils.h"
#import "DesignUtils.h"
#import "RankingUtils.h"

//広告系
#import "CSSAdView.h"
#import "appCCloud.h"

@interface ScoreViewController ()

@property (nonatomic) UIDynamicAnimator *animator;
//広告系(AdMoBバナー)
@property (strong,nonatomic) CSSAdView *bannerAdView;

@end

@implementation ScoreViewController
{
    //プレイ画面で流れているBGMを格納する変数
    AVAudioPlayer *playBGMSound;
    
    AVAudioPlayer *selectSounds;
    
    //表示位置の選択
    appCSimpleView *simpleView;
    
    //メニューに戻る、リトライボタンの連続押し防止
    BOOL touchMenuBack,touchReTry;
    
    //ランキング閲覧画面の表示非表示判断
    BOOL rankShowFlg;
    
    //ランキングの内容を表示するView
    NSArray *rankingArray;
    
    //アプリ内の最高得点格納変数
    NSUserDefaults *tHighScore,*cHighScore;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //ランキングボタンは最初非表示なので
    rankShowFlg = NO;
    
    //スコア系ラベルの加工
    NSArray *labelArray = [[NSArray alloc]initWithObjects:_gameoverLbl,_levelLabel,_levelNum,_scoreLabel,_scoreNum, nil];
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadow:labelArray];
    
    //フレームの定義(RankingShowView)
    [_rankShowView.layer setBorderColor:[UIColor blackColor].CGColor];
    [_rankShowView.layer setBorderWidth:2.0];
    _rankShowView.layer.cornerRadius = 20;
    
    
    //背景の設定など
    [self designSettingAct];
    
    //各スコアに代入
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    DLog(@"%d",appDelegate.levelDate);
    
    _levelNum.text = [NSString stringWithFormat:@"%d",appDelegate.levelDate];
    
    _scoreNum.text = [NSString stringWithFormat:@"%d",appDelegate.scoreDate];
    
    
    //タイムアタックモードの場合、ランキング処理を行います
    /////////////////////////////////////////////
    if(appDelegate.modeFlg == 0){
    
        //ゲーム結果をゲームセンターに転送します
        [self sendHighScoreGameCenter:_scoreNum];
        
        //ランキングに入らなかった場合、ランキングを更新します
        //ランキング入賞確認のみ処理
        RankingUtils *rankUtils = [[RankingUtils alloc]init];
        
        [rankUtils checkFlgRanking:_scoreNum mode:appDelegate.modeFlg];
    
        //ランキングに入った場合
        //順位によって表示するメッセージを変更します
        if(appDelegate.prizeNumber != 0){
            switch (appDelegate.prizeNumber) {
                case 1:
                    _prizeMsgLabel.text = @"WIN 1st prize!!";
                    
                    //1位の場合、スコアをUserDefaultに代入します
                    tHighScore = [NSUserDefaults standardUserDefaults];
                    [tHighScore setInteger:appDelegate.scoreDate forKey:@"KEY_tAppScore"];
                    [tHighScore synchronize];
                    
                    break;
                case 2:
                    _prizeMsgLabel.text = @"WIN 2nd prize!!";
                    break;
                case 3:
                    _prizeMsgLabel.text = @"WIN 3rd prize!!";
                    break;
                case 4:
                    _prizeMsgLabel.text = @"WIN 4th prize!!";
                    break;
                case 5:
                    _prizeMsgLabel.text = @"WIN 5th prize!!";
                    break;
                case 6:
                    _prizeMsgLabel.text = @"WIN 6th prize!!";
                    break;
                case 7:
                    _prizeMsgLabel.text = @"WIN 7th prize!!";
                    break;
                case 8:
                    _prizeMsgLabel.text = @"WIN 8th prize!!";
                    break;
                case 9:
                    _prizeMsgLabel.text = @"WIN 9th prize!!";
                    break;
                case 10:
                    _prizeMsgLabel.text = @"WIN 10th prize!!";
                    break;
                default:
                    break;
            }
        
            //入賞した場合にappDelegate変数にメッセージ代入
            appDelegate.prizeMsg = _prizeMsgLabel.text;
        
        }
        
    }else if(appDelegate.modeFlg == 2){
        
        //ゲーム結果をゲームセンターに転送します
        //[self sendHighScoreGameCenter:_scoreNum];
        
        //ランキングに入らなかった場合、ランキングを更新します
        //ランキング入賞確認のみ処理
        RankingUtils *rankUtils = [[RankingUtils alloc]init];
        [rankUtils checkFlgRanking:_scoreNum mode:appDelegate.modeFlg];
        
        //ランキングに入った場合
        //順位によって表示するメッセージを変更します
        if(appDelegate.prizeNumber != 0){
            switch (appDelegate.prizeNumber) {
                case 1:
                    _prizeMsgLabel.text = @"WIN 1st prize!!";
                    
                    //スコアが１位の場合、NSUserDefaultに登録
                    cHighScore = [NSUserDefaults standardUserDefaults];
                    [cHighScore setInteger:appDelegate.scoreDate forKey:@"KEY_cAppScore"];
                    [cHighScore synchronize];
                    
                    break;
                case 2:
                    _prizeMsgLabel.text = @"WIN 2nd prize!!";
                    break;
                case 3:
                    _prizeMsgLabel.text = @"WIN 3rd prize!!";
                    break;
                case 4:
                    _prizeMsgLabel.text = @"WIN 4th prize!!";
                    break;
                case 5:
                    _prizeMsgLabel.text = @"WIN 5th prize!!";
                    break;
                case 6:
                    _prizeMsgLabel.text = @"WIN 6th prize!!";
                    break;
                case 7:
                    _prizeMsgLabel.text = @"WIN 7th prize!!";
                    break;
                case 8:
                    _prizeMsgLabel.text = @"WIN 8th prize!!";
                    break;
                case 9:
                    _prizeMsgLabel.text = @"WIN 9th prize!!";
                    break;
                case 10:
                    _prizeMsgLabel.text = @"WIN 10th prize!!";
                    break;
                default:
                    break;
            }
            
            //入賞した場合にappDelegate変数にメッセージ代入
            appDelegate.prizeMsg = _prizeMsgLabel.text;
            
        }
    }
    
    //BOOL変数初期化
    touchMenuBack = YES;
    touchReTry = YES;
    
    ////////////////////////
    // 広告
    // [bannaer<AdMob>]
#if AD_DISABLE
    DLog(@"広告非表示");
#else
    CGSize adSize = GAD_SIZE_320x50;
    
    CSSAdView *adView = [CSSAdView adViewForAdmobMediationWithAdSize:adSize rootViewController:self mediationID:ADMOB_MEDIATION_ID delegate:nil];
    [self.view addSubview:adView];
    self.bannerAdView = adView;
#endif
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // バナー広告(AdMoB)
    // 広告
    //self.bannerAdView.frame = CGRectMake(0, self.view.frame.size.height - self.bannerAdView.bounds.size.height, self.bannerAdView.bounds.size.width, self.bannerAdView.bounds.size.height);
    
    self.bannerAdView.frame = CGRectMake(0,STATUSBAR_HEIGHT, self.bannerAdView.bounds.size.width, self.bannerAdView.bounds.size.height);
    
    DLog(@"広告表示開始");
    [self.bannerAdView admobMediationRequestStart];
    
    DLog(@"bannerAdView %@", self.bannerAdView);

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    // [banner <appC cloud>]
    //表示位置の選択
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    //広告Viewは最後に追加する
    [self.view addSubview:appDelegate.simpleAdView];
    
    //落下し、バウンドするアニメーションメソッドの実行(遅延実行)
    //[self boundviewAnimated:_adview];
    [self performSelector:@selector(boundAnimated:)
               withObject:_gameoverLbl afterDelay:1.0];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    DLog(@"広告表示停止");
    //メニュー画面が閉じたら、広告ストップ
    [self.bannerAdView admobMediationRequestStop];
    DLog(@"bannerAdView %@", self.bannerAdView);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - animated

//************************************
//画面下に落下し、バウンドするアニメーション
//************************************
-(void)boundviewAnimated:(UIView *)boundView{
    
    
    // UIDynamicAnimator！今回の主役
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    // UIDynamicAnimatorの仲間たち１。重力を司る者
    UIGravityBehavior *gravityBeahvior = [[UIGravityBehavior alloc] initWithItems:@[boundView]];
    
    // UIDynamicAnimatorの仲間たち２。衝突を司る者
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[boundView]];
    collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
    
    // アニメーターにビヘイビアを登録
    [self.animator addBehavior:gravityBeahvior];
    [self.animator addBehavior:collisionBehavior];
    
}

-(void)boundAnimated:(UILabel *)boundLabel{
    
    // UIDynamicAnimator！今回の主役
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    // UIDynamicAnimatorの仲間たち１。重力を司る者
    UIGravityBehavior *gravityBeahvior = [[UIGravityBehavior alloc] initWithItems:@[boundLabel]];
    
    // UIDynamicAnimatorの仲間たち２。衝突を司る者
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[boundLabel]];
    collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
    
    // アニメーターにビヘイビアを登録
    [self.animator addBehavior:gravityBeahvior];
    [self.animator addBehavior:collisionBehavior];
    
    
    
    //いいタイミングで各Viewがフェードインしていく
    //フェードイン
    [self performSelector:@selector(fadeInLabel:)
               withObject:_levelLabel afterDelay:1.0];
    [self performSelector:@selector(fadeInLabel:)
               withObject:_levelNum afterDelay:1.0];
    
    [self performSelector:@selector(fadeInLabel:)
               withObject:_scoreLabel afterDelay:2.0];
    [self performSelector:@selector(fadeInLabel:)
               withObject:_scoreNum afterDelay:2.0];
        
    AppDelegate * appDelegate = [[UIApplication sharedApplication]delegate];
    //ランキングに入ったときだけ表示させる
    //"0"はランキング外です
    if(appDelegate.prizeNumber != 0){
        
        [self performSelector:@selector(fadeInLabel:)
                   withObject:_prizeImage afterDelay:2.5];
        [self performSelector:@selector(rankfadeInLabel:)
                   withObject:_prizeMsgLabel afterDelay:2.5];
        
        [self performSelector:@selector(playerNameEditAct) withObject:nil afterDelay:2.5];
    }
    
    
    [self performSelector:@selector(fadeInBtn:)
               withObject:_retry afterDelay:3.0];
    [self performSelector:@selector(fadeInBtn:)
               withObject:_backMenu afterDelay:3.0];
    
}


//************
//フェードイン
//************
-(void)fadeInLabel:(UILabel *)fadeInLabel{
    
    //フェードイン
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.6];
    //目標のアルファ値を指定
    fadeInLabel.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
}

//********************
//フェードイン(ランクイン)
//********************
-(void)rankfadeInLabel:(UILabel *)fadeInLabel{
    
    //フェードイン
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.6];
    //目標のアルファ値を指定
    fadeInLabel.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
}

-(void)fadeInBtn:(UIButton *)fadeInButton{
    
    //フェードイン
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.6];
    //目標のアルファ値を指定
    fadeInButton.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
    
    //ボタンを押せるようにする
    [fadeInButton setEnabled:YES];
}

#pragma mark - ButtonAction

//***********************
//リトライボタン処理メソッド
//***********************

- (IBAction)retryBtnAct:(id)sender {
    
    
    //連続押し防止(リトライボタン)
    if(touchReTry == YES){
        
        //とりあえず押せないようにします
        touchReTry = NO;
        
        //play中のBGMを止めます
        [self stopBGMSound];
    
        //選択音
        SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
        selectSounds = [soundsUtils selectMenuSounds];
        [selectSounds play];
    
        ViewController *playView = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayViewController"];
        //フェードアウトしながら画面遷移
        [self pushViewControllerAnimted:playView];
        
        //画面遷移の際にスコア初期か
        AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
        appDelegate.levelDate = 0;
        appDelegate.scoreDate = 0;
    }
    
}

//**********************
//メニューに戻る処理メソッド
//**********************
- (IBAction)backMenuBtnAct:(id)sender {
    
    //連続押し防止(メニューボタン)
    if(touchMenuBack == YES){
        
        //とりあえず押せないようにします
        touchMenuBack = NO;
        
        //play中のBGMを止めます
        [self stopBGMSound];
    
        //選択音
        SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
        selectSounds = [soundsUtils selectMenuSounds];
        [selectSounds play];
    
        [self performSelector:@selector(popMenuViewControllerAnimted)
                   withObject:nil afterDelay:0.2];
    
        //この画面からメニュー画面に戻る際に広告フラグをたてる
        AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
        appDelegate.splashAdFlg = YES;
        
        //画面遷移の際にスコア初期か
        appDelegate.levelDate = 0;
        appDelegate.scoreDate = 0;
    }
    
    
}


#pragma mark - PushAnimated

//***************************************
//フェードアウトしながら画面遷移する(プレイ画面)
//***************************************
-(void)pushViewControllerAnimted:(UIViewController *)pushView{
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 1.0;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    [self.navigationController pushViewController:pushView animated:NO];
    
    //遷移処理が終わったら、連続押し防止フラグを初期化します
    touchReTry = YES;
    
}

//************************************************
//フェードアウトしながら画面遷移する(メニュー画面<Root>)
//************************************************
-(void)popMenuViewControllerAnimted{
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    //戻る感じに動作させます
    transition.subtype = kCATransitionFromRight;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    
    //[self.navigationController pushViewController:pushMenuView animated:YES];
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    //画面遷移が終了したら、連続押し防止フラグを初期化します
    touchMenuBack = YES;
    
    
}

#pragma mark - social

- (IBAction)twitterBtnAct:(id)sender {
    
    NSString *msg = [self scoreMsg];
    
    [DCSocial postToTwitter:self text:msg imageName:nil url:nil];
}

- (IBAction)facebookBtnAct:(id)sender {
    
     NSString *msg = [self scoreMsg];
    
    [DCSocial postToFacebook:self text:msg imageName:nil url:nil];
}

//*****************************
//socialでのメッセージ選択メソッド
//*****************************
-(NSString *)scoreMsg{
    
    AppDelegate *appDelegate  = [[UIApplication sharedApplication]delegate];
    NSString *msg;
    
    if(appDelegate.modeFlg == 0){
        msg = [NSString stringWithFormat:@"脳トレカジュアルゲーム『DoReMi Seven』のTimeAttackモードで%d点とりました！",appDelegate.scoreDate];
    }else if(appDelegate.modeFlg == 2){
        msg = [NSString stringWithFormat:@"脳トレカジュアルゲーム『DoReMi Seven』のChallengeモードで%d点とりました！",appDelegate.scoreDate];
    }

    return msg;
}

#pragma mark - sound

//*****************************
//play画面で流れているBGMを止めます
//*****************************
-(void)stopBGMSound{
    
    //SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    
    AppDelegate * appDelegate = [[UIApplication sharedApplication]delegate];
    playBGMSound = appDelegate.AppBGMSound;
    
    [playBGMSound stop];
    //フェードアウト
    //[soundsUtils doVolumeFade:playBGMSound];
  
}

#pragma mark - backImage

-(void)designSettingAct{
    
    //背景画像の設定(カメラロール)
    NSUserDefaults *_backImageUD = [NSUserDefaults standardUserDefaults];
    NSData *data = [_backImageUD objectForKey:@"KEY_BackImage"];
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    
    //背景
    NSUserDefaults *_segmentUD = [NSUserDefaults standardUserDefaults];
    int segmentInt = [_segmentUD integerForKey:@"KEY_Segment"];
    
    if(segmentInt == 0){
        [designUtils backImageSettingAct:self.view];
    }else{
        [designUtils backImageCameraSettingAct:self.view image:data];
    }

}

#pragma mark - rankingShow

//***************************
//ちょこっとランキング閲覧ボタン
//***************************
- (IBAction)rankShowAct:(id)sender {
    
    DLog(@"ボタンが押されました");
    //[_rankshowTableView bringSubviewToFront:];
    //[_prizeImage sendSubviewToBack:_rankShowView];
    
    if(rankShowFlg == NO){
        
        _prizeImage.hidden = YES;
        _prizeMsgLabel.hidden = YES;
        
        //次押したらViewを閉じます
        rankShowFlg = YES;
        //_rankShowView.alpha = 1.0;
        
        //ランキング画面が出ている間はソーシャルボタンを押せなくします
        [_twitterBtn setEnabled:NO];
        [_faceBookBtn setEnabled:NO];
        
        //にょきっと出すので、アニメーションの前に表示させます
        _rankShowView.hidden = NO;

        //フェードイン
        //アニメーションのタイプを指定
        [UIView beginAnimations:@"fadeIn" context:nil];
        //イージング指定
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        //アニメーション秒数を指定
        [UIView setAnimationDuration:0.2];
        //目標のアルファ値を指定
        _rankShowView.alpha = 0.9;
        //アニメーション実行
        [UIView commitAnimations];
        
    }else{
        
        //次押したらViewを出します
        rankShowFlg = NO;
        
        _prizeImage.hidden = NO;
        _prizeMsgLabel.hidden = NO;
        
        //ソーシャルボタンを押せるようにします
        [_twitterBtn setEnabled:YES];
        [_faceBookBtn setEnabled:YES];
        
        //フェードアウト
        [UIView beginAnimations:@"fadeOut" context:nil];
        //イージング指定
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        //アニメーション秒数を指定
        [UIView setAnimationDuration:0.2];
        //目標のアルファ値を指定
        _rankShowView.alpha = 0;
        //アニメーション実行
        [UIView commitAnimations];
    
        //画面から消えたら、内部的にも消します
        _rankShowView.hidden = NO;
    }
   
}

#pragma mark - ranking

//***********************
//ランキングの内容を返します
//***********************
-(void)ranking{
    
    //ランキングの内容を配列に格納します
    NSUserDefaults *_rankArrayUD = [NSUserDefaults standardUserDefaults];
    rankingArray = [_rankArrayUD objectForKey:SCORE_KEY];
}

#pragma mark - TableViewDelegate

//*******************
//Section数
//*******************
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

//********************
//Row（行数）
//********************
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [rankingArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    //配列の内容を表示します
    NSString *rank = [rankingArray objectAtIndex:indexPath.row];
    
    //rank.font = [UIFont fontWithName:@"Chalkboard SE" size:25];
    cell.textLabel.text = rank;
    cell.textLabel.font = [UIFont fontWithName:@"Chalkboard SE" size:18];
    
    return cell;
}


#pragma mark - GameCenter Delegate

//***********************
//スコアの送信
//***********************
-(void)sendHighScoreGameCenter:(UILabel *)gameScore{
    
    //GameCenterにログインしている場合
    if([GKLocalPlayer localPlayer].isAuthenticated){
        
        GKScore* score = [[GKScore alloc]initWithLeaderboardIdentifier:@"jp.co.carolsendai.SevensNumber"];
        
        //点数を整数にキャストして、ゲームセンタのスコアに入れます
        NSInteger scoreInt = [gameScore.text intValue];
        score.value = scoreInt;
        
        [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
            if (error) {
                // エラーの場合
                UIAlertView *alert =
                [[UIAlertView alloc] initWithTitle:@"お知らせ" message:@"GameCenterにスコアを転送できませんでした"
                                          delegate:self cancelButtonTitle:@"確認" otherButtonTitles:nil];
                [alert show];
            }
        }];
        
    }
}

//**************************
//LearderBoardの表示(モーダル)
//**************************
- (IBAction)gameCImageAct:(id)sender {
    
    GKGameCenterViewController *gcView = [GKGameCenterViewController new];
    
    if (gcView != nil)
    {
        gcView.gameCenterDelegate = self;
        gcView.viewState = GKGameCenterViewControllerStateLeaderboards;
        //gcView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:gcView animated:YES completion:nil];
    }
}

//*******************************
//リーダーボードで完了タップ時の処理
//前の画面に戻る
//*******************************
- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


//******************************************
//ランキングに入賞した場合、ランキングのプレイヤー名
//入力画面に遷移します
//******************************************
-(void)playerNameEditAct{
    
    // PickerViewControllerのインスタンスをStoryboardから取得し
    self.playerEditView = [[self storyboard] instantiateViewControllerWithIdentifier:@"PlayerNameEditViewController"];
    self.playerEditView.delegate = self;
    
    // PickerViewをサブビューとして表示する
    // 表示するときはアニメーションをつけて下から上にゆっくり表示させる
    
    // アニメーション完了時のPickerViewの位置を計算
    UIView *pickerView = self.playerEditView.view;
    CGPoint middleCenter = pickerView.center;
    
    // アニメーション開始時のPickerViewの位置を計算
    UIWindow* mainWindow = (((AppDelegate*) [UIApplication sharedApplication].delegate).window);
    CGSize offSize = [UIScreen mainScreen].bounds.size;
    CGPoint offScreenCenter = CGPointMake(offSize.width / 2.0, offSize.height * 1.5);
    pickerView.center = offScreenCenter;
    
    [mainWindow addSubview:pickerView];
    
    // アニメーションを使ってPickerViewをアニメーション完了時の位置に表示されるようにする
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.3];
    pickerView.center = middleCenter;
    [UIView commitAnimations];
    
    //入賞音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSounds = [soundsUtils effectSounds:@"prizeRank"];
    [selectSounds play];
        
}

// PickerViewController上にある透明ボタンがタップされたときに呼び出されるPickerViewControllerDelegateプロトコルのデリゲートメソッド
- (void)closePickerView:(PlayerNameEditViewController *)controller
{
    // PickerViewをアニメーションを使ってゆっくり非表示にする
    //UIView *playerNameView = self.view;
    UIView *playerNameView = controller.view;
    
    // アニメーション完了時のPickerViewの位置を計算
    CGSize offSize = [UIScreen mainScreen].bounds.size;
    CGPoint offScreenCenter = CGPointMake(offSize.width / 2.0, offSize.height * 1.5);
    
    [UIView beginAnimations:nil context:(void *)playerNameView];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    
    // アニメーション終了時に呼び出す処理を設定
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    playerNameView.center = offScreenCenter;
    [UIView commitAnimations];
}

//*****************************************************************
// 単位のPickerViewを閉じるアニメーションが終了したときに呼び出されるメソッド
//*****************************************************************
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    // PickerViewをサブビューから削除
    UIView *playerNameView = (__bridge UIView *)context;
    [playerNameView removeFromSuperview];
}



@end
