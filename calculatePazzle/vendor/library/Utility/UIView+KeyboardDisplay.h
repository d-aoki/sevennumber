//
//  UIView+KeyboardDisplay.h
//  ScheduleConcierge
//
//  UIViewクラスを継承する全てのViewクラスについて、キーボード表示時のView位置調整機能を付加します。
//  キーボードが表示された際に、キーボード分表示をずらしたいViewに enableKeyboardDisplay を呼び出します。
//  Notificationを正しく削除するために、呼び出しもとのdealloc処理で、必ず disableKeyboardDisplay を呼び出す必要があります。
//
//  Created by Natsuki Funaki on 2013/07/03.
//  Copyright (c) 2013年 Takashi Yasukawa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (KeyboardDisplay)

@property (assign, nonatomic) BOOL keyboardWasShown;
@property (assign, nonatomic) NSMutableArray* firstResponderViews;

- (void)enableKeyboardDisplay;
- (void)enableKeyboardDisplayAtFirstResponderViews:(NSArray*)atViews;
- (void)keyboardDisplayAddFirstResponderView:(id)firstResponder;


- (void)disableKeyboardDisplay;


@end
