//
//  CSSInformationViewController.h
//
//  キャロルシステム仙台のインフォメーション画面を表示するコントローラです。
//  表示に、呼び出した環境に合わせるようにしていますが、大体の調整です。
//
//  利用したいところでshowWithRootViewControllerメソッドを呼び出してください。
//
//  何か個別にカスタマイズしたい場合は、適に自由に変更してください。
//
//  Version 1.0
//
//  Created by  on 12/05/30.
//  Copyright (c) 2013年

#import <UIKit/UIKit.h>

typedef void (^CSSInformationDismissCompletion)(void);

@interface CSSInformationViewController : UIViewController

@property (nonatomic, strong) UIImageView *logoView;

@property (nonatomic, strong) UILabel *catchCopyLabel;
@property (nonatomic, strong) UILabel *companyNameLabel;
@property (nonatomic, strong) UILabel *companyWebSiteURLLabel;

@property (nonatomic, strong) UITextView *licenseTextView;

@property (nonatomic, strong) UILabel *copyrightLabel;

@property (nonatomic, strong) UIButton *btnLink;

@property (readwrite, copy) CSSInformationDismissCompletion dismissCompletion;

+ (void)showWithRootViewController:(UIViewController*)rootViewController showCompletion:(void (^)(void))completion dismissCompletion:(CSSInformationDismissCompletion)dismissCompletion;
@end
