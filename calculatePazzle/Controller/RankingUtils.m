//
//  RankingUtils.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/14.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "RankingUtils.h"
#import "AppDelegate.h"

@implementation RankingUtils
{
    //点数分のUUIDを確保します
    //タイムアタック
    NSUserDefaults *_rankingScoreUD1,*_rankingScoreUD2,*_rankingScoreUD3,*_rankingScoreUD4,*_rankingScoreUD5,*_rankingScoreUD6,*_rankingScoreUD7,*_rankingScoreUD8,*_rankingScoreUD9,*_rankingScoreUD10;
    
    //チャレンジ
    NSUserDefaults *_cngrankScoreUD1,*_cngrankScoreUD2,*_cngrankScoreUD3,*_cngrankScoreUD4,*_cngrankScoreUD5,*_cngrankScoreUD6,*_cngrankScoreUD7,*_cngrankScoreUD8,*_cngrankScoreUD9,*_cngrankScoreUD10;
    
    
    
    //タイムアタックモード,チャレンジモード
    NSUserDefaults *_rankArrayUD,*_cngrankArrayUD;
    
    //順位格納変数(10位まで)
    NSString *rank1,*rank2,*rank3,*rank4,*rank5,*rank6,*rank7,*rank8,*rank9,*rank10;
    
    //点数格納配列
    NSString *_rank1Str,*_rank2Str,*_rank3Str,*_rank4Str,*_rank5Str,*_rank6Str,*_rank7Str,*_rank8Str,*_rank9Str,*_rank10Str;
    
    NSString *formatScore1,*formatScore2,*formatScore3,*formatScore4,*formatScore5,*formatScore6,*formatScore7,*formatScore8,*formatScore9,*formatScore10;
    
    NSCalendar *calendar;
    NSUInteger flags;
    NSDateComponents *comps;

}

//*******************************
//ランキングの登録を行います(引数あり)
//*******************************
-(void)rankingRegist:(UILabel *)score modeFlg:(int)modeFlg{
    
    
    //点数が出たら、ランキングの入れ替えを行います
    if(score){
        [self checkRanking:score mode:modeFlg];
    }
    
    //順位はこの先変更ないと思うので今はこれで...。
    rank1=@"1";rank2=@"2";
    rank3=@"3";rank4=@"4";
    rank5=@"5";rank6=@"6";
    rank7=@"7";rank8=@"8";
    rank9=@"9";rank10=@"10";
    
    //順位を配列に入れます
    NSArray *rankIntArray = [[NSArray alloc]initWithObjects:rank1,rank2,rank3,rank4,rank5,rank6,rank7,rank8,rank9,rank10,nil];
    
    //１０位までの結果を代入(ないデータはNULLになっているはず)
    
    //モードによって代入するNSUserDefault変数が異なります
    //(modeFlg=0)->タイムアタック,(modeFlg=1)->チャレンジ
    if(modeFlg == 0){
        _rankingScoreUD1 = [NSUserDefaults standardUserDefaults];
        _rank1Str = [_rankingScoreUD1 stringForKey:@"KEY_Rank1"];
        _rankingScoreUD2 = [NSUserDefaults standardUserDefaults];
        _rank2Str = [_rankingScoreUD2 stringForKey:@"KEY_Rank2"];
        _rankingScoreUD3 = [NSUserDefaults standardUserDefaults];
        _rank3Str = [_rankingScoreUD3 stringForKey:@"KEY_Rank3"];
        _rankingScoreUD4 = [NSUserDefaults standardUserDefaults];
        _rank4Str = [_rankingScoreUD4 stringForKey:@"KEY_Rank4"];
        _rankingScoreUD5 = [NSUserDefaults standardUserDefaults];
        _rank5Str = [_rankingScoreUD5 stringForKey:@"KEY_Rank5"];
        _rankingScoreUD6 = [NSUserDefaults standardUserDefaults];
        _rank6Str = [_rankingScoreUD6 stringForKey:@"KEY_Rank6"];
        _rankingScoreUD7 = [NSUserDefaults standardUserDefaults];
        _rank7Str = [_rankingScoreUD7 stringForKey:@"KEY_Rank7"];
        _rankingScoreUD8 = [NSUserDefaults standardUserDefaults];
        _rank8Str = [_rankingScoreUD8 stringForKey:@"KEY_Rank8"];
        _rankingScoreUD9 = [NSUserDefaults standardUserDefaults];
        _rank9Str = [_rankingScoreUD9 stringForKey:@"KEY_Rank9"];
        _rankingScoreUD10 = [NSUserDefaults standardUserDefaults];
        _rank10Str = [_rankingScoreUD10 stringForKey:@"KEY_Rank10"];
    }else if(modeFlg == 2){
        //チャレンジモード
        _cngrankScoreUD1 = [NSUserDefaults standardUserDefaults];
        _rank1Str = [_cngrankScoreUD1 stringForKey:@"KEY_CRank1"];
        _cngrankScoreUD2 = [NSUserDefaults standardUserDefaults];
        _rank2Str = [_cngrankScoreUD2 stringForKey:@"KEY_CRank2"];
        _cngrankScoreUD3 = [NSUserDefaults standardUserDefaults];
        _rank3Str = [_cngrankScoreUD3 stringForKey:@"KEY_CRank3"];
        _cngrankScoreUD4 = [NSUserDefaults standardUserDefaults];
        _rank4Str = [_cngrankScoreUD4 stringForKey:@"KEY_CRank4"];
        _cngrankScoreUD5 = [NSUserDefaults standardUserDefaults];
        _rank5Str = [_cngrankScoreUD5 stringForKey:@"KEY_CRank5"];
        _cngrankScoreUD6 = [NSUserDefaults standardUserDefaults];
        _rank6Str = [_cngrankScoreUD6 stringForKey:@"KEY_CRank6"];
        _cngrankScoreUD7 = [NSUserDefaults standardUserDefaults];
        _rank7Str = [_cngrankScoreUD7 stringForKey:@"KEY_CRank7"];
        _cngrankScoreUD8 = [NSUserDefaults standardUserDefaults];
        _rank8Str = [_cngrankScoreUD8 stringForKey:@"KEY_CRank8"];
        _cngrankScoreUD9 = [NSUserDefaults standardUserDefaults];
        _rank9Str = [_cngrankScoreUD9 stringForKey:@"KEY_CRank9"];
        _cngrankScoreUD10 = [NSUserDefaults standardUserDefaults];
        _rank10Str = [_cngrankScoreUD10 stringForKey:@"KEY_CRank10"];
    }
    else if (modeFlg == 3){
        
        for(int i=0;i<2;i++){
            
            if(i==0){
                _rankingScoreUD1 = [NSUserDefaults standardUserDefaults];
                _rank1Str = [_rankingScoreUD1 stringForKey:@"KEY_Rank1"];
                _rankingScoreUD2 = [NSUserDefaults standardUserDefaults];
                _rank2Str = [_rankingScoreUD2 stringForKey:@"KEY_Rank2"];
                _rankingScoreUD3 = [NSUserDefaults standardUserDefaults];
                _rank3Str = [_rankingScoreUD3 stringForKey:@"KEY_Rank3"];
                _rankingScoreUD4 = [NSUserDefaults standardUserDefaults];
                _rank4Str = [_rankingScoreUD4 stringForKey:@"KEY_Rank4"];
                _rankingScoreUD5 = [NSUserDefaults standardUserDefaults];
                _rank5Str = [_rankingScoreUD5 stringForKey:@"KEY_Rank5"];
                _rankingScoreUD6 = [NSUserDefaults standardUserDefaults];
                _rank6Str = [_rankingScoreUD6 stringForKey:@"KEY_Rank6"];
                _rankingScoreUD7 = [NSUserDefaults standardUserDefaults];
                _rank7Str = [_rankingScoreUD7 stringForKey:@"KEY_Rank7"];
                _rankingScoreUD8 = [NSUserDefaults standardUserDefaults];
                _rank8Str = [_rankingScoreUD8 stringForKey:@"KEY_Rank8"];
                _rankingScoreUD9 = [NSUserDefaults standardUserDefaults];
                _rank9Str = [_rankingScoreUD9 stringForKey:@"KEY_Rank9"];
                _rankingScoreUD10 = [NSUserDefaults standardUserDefaults];
                _rank10Str = [_rankingScoreUD10 stringForKey:@"KEY_Rank10"];
            }else{
                //チャレンジモード
                _cngrankScoreUD1 = [NSUserDefaults standardUserDefaults];
                _rank1Str = [_cngrankScoreUD1 stringForKey:@"KEY_CRank1"];
                _cngrankScoreUD2 = [NSUserDefaults standardUserDefaults];
                _rank2Str = [_cngrankScoreUD2 stringForKey:@"KEY_CRank2"];
                _cngrankScoreUD3 = [NSUserDefaults standardUserDefaults];
                _rank3Str = [_cngrankScoreUD3 stringForKey:@"KEY_CRank3"];
                _cngrankScoreUD4 = [NSUserDefaults standardUserDefaults];
                _rank4Str = [_cngrankScoreUD4 stringForKey:@"KEY_CRank4"];
                _cngrankScoreUD5 = [NSUserDefaults standardUserDefaults];
                _rank5Str = [_cngrankScoreUD5 stringForKey:@"KEY_CRank5"];
                _cngrankScoreUD6 = [NSUserDefaults standardUserDefaults];
                _rank6Str = [_cngrankScoreUD6 stringForKey:@"KEY_CRank6"];
                _cngrankScoreUD7 = [NSUserDefaults standardUserDefaults];
                _rank7Str = [_cngrankScoreUD7 stringForKey:@"KEY_CRank7"];
                _cngrankScoreUD8 = [NSUserDefaults standardUserDefaults];
                _rank8Str = [_cngrankScoreUD8 stringForKey:@"KEY_CRank8"];
                _cngrankScoreUD9 = [NSUserDefaults standardUserDefaults];
                _rank9Str = [_cngrankScoreUD9 stringForKey:@"KEY_CRank9"];
                _cngrankScoreUD10 = [NSUserDefaults standardUserDefaults];
                _rank10Str = [_cngrankScoreUD10 stringForKey:@"KEY_CRank10"];
            }
            
            //配列に要素を追加します
            if(_rank1Str == nil){
                _rank1Str = @"No Data";
            }
            if(_rank2Str == nil){
                _rank2Str = @"No Data";
            }
            if(_rank3Str == nil){
                _rank3Str = @"No Data";
            }if(_rank4Str == nil){
                _rank4Str = @"No Data";
            }
            if(_rank5Str == nil){
                _rank5Str = @"No Data";
            }
            if(_rank6Str == nil){
                _rank6Str = @"No Data";
            }
            if(_rank7Str == nil){
                _rank7Str = @"No Data";
            }
            if(_rank8Str == nil){
                _rank8Str = @"No Data";
            }
            if(_rank9Str == nil){
                _rank9Str = @"No Data";
            }
            if(_rank10Str == nil){
                _rank10Str = @"No Data";
            }
            
            NSArray *rankStrArray = [[NSArray alloc]initWithObjects:_rank1Str,_rank2Str,_rank3Str,_rank4Str,_rank5Str,_rank6Str,_rank7Str,_rank8Str,_rank9Str,_rank10Str, nil];
            
            
            //ランキングに表示されるフォーマットをここで設定しておく(順位：点数 [日付])
            for(int i=0;i<[rankIntArray count];i++){
                
                
                switch (i) {
                    case 0:
                        formatScore1 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 1:
                        formatScore2 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 2:
                        formatScore3 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 3:
                        formatScore4 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 4:
                        formatScore5 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 5:
                        formatScore6 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 6:
                        formatScore7 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 7:
                        formatScore8 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 8:
                        formatScore9 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                    case 9:
                        formatScore10 = [NSString stringWithFormat:@"%@: %@",rankIntArray[i],rankStrArray[i]];
                        break;
                        
                    default:
                        break;
                }
            }
            
            NSArray *scoreFormatArray = [[NSArray alloc]initWithObjects:formatScore1,formatScore2,formatScore3,formatScore4,formatScore5,formatScore6,formatScore7,formatScore8,formatScore9,formatScore10, nil];
            
            
            //ランキングフォーマットをNSUserDefaultに保存します
            if(i==0){
                _rankArrayUD = [NSUserDefaults standardUserDefaults];
                [_rankArrayUD setObject:scoreFormatArray forKey:SCORE_KEY];
                [_rankArrayUD synchronize];//セーブ
                
            }else{
                _cngrankArrayUD = [NSUserDefaults standardUserDefaults];
                [_cngrankArrayUD setObject:scoreFormatArray forKey:CNG_SCORE_KEY];
                [_cngrankArrayUD synchronize];
            }
        }
        
        
        
        return;
    }
    
    //配列に要素を追加します
    if(_rank1Str == nil){
        _rank1Str = @"No Data";
    }
    if(_rank2Str == nil){
        _rank2Str = @"No Data";
    }
    if(_rank3Str == nil){
        _rank3Str = @"No Data";
    }if(_rank4Str == nil){
        _rank4Str = @"No Data";
    }
    if(_rank5Str == nil){
        _rank5Str = @"No Data";
    }
    if(_rank6Str == nil){
        _rank6Str = @"No Data";
    }
    if(_rank7Str == nil){
        _rank7Str = @"No Data";
    }
    if(_rank8Str == nil){
        _rank8Str = @"No Data";
    }
    if(_rank9Str == nil){
        _rank9Str = @"No Data";
    }
    if(_rank10Str == nil){
        _rank10Str = @"No Data";
    }
    
    NSArray *rankStrArray = [[NSArray alloc]initWithObjects:_rank1Str,_rank2Str,_rank3Str,_rank4Str,_rank5Str,_rank6Str,_rank7Str,_rank8Str,_rank9Str,_rank10Str, nil];
    
    
    //ランキングに表示されるフォーマットをここで設定しておく(順位：点数 [日付])
    for(int i=0;i<[rankIntArray count];i++){
        
        //日付の表示判別
        /*NSString *distinctionStr = rankStrArray[i];
        [self scoredayDistinction:distinctionStr];*/
        
        switch (i) {
            case 0:
                formatScore1 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 1:
                formatScore2 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 2:
                formatScore3 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 3:
                formatScore4 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 4:
                formatScore5 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 5:
                formatScore6 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 6:
                formatScore7 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 7:
                formatScore8 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 8:
                formatScore9 = [NSString stringWithFormat:@" %@: %@",rankIntArray[i],rankStrArray[i]];
                break;
            case 9:
                formatScore10 = [NSString stringWithFormat:@"%@: %@",rankIntArray[i],rankStrArray[i]];
                break;
                
            default:
                break;
        }
    }
    
    NSArray *scoreFormatArray = [[NSArray alloc]initWithObjects:formatScore1,formatScore2,formatScore3,formatScore4,formatScore5,formatScore6,formatScore7,formatScore8,formatScore9,formatScore10, nil];

    
    //modeFlg->3はメインViewから
    if (modeFlg == 0){

        //ランキングフォーマットをNSUserDefaultに保存します
        _rankArrayUD = [NSUserDefaults standardUserDefaults];
        [_rankArrayUD setObject:scoreFormatArray forKey:SCORE_KEY];
        [_rankArrayUD synchronize];//セーブ
    }else if(modeFlg == 2){

        _cngrankArrayUD = [NSUserDefaults standardUserDefaults];
        [_cngrankArrayUD setObject:scoreFormatArray forKey:CNG_SCORE_KEY];
        [_cngrankArrayUD synchronize];
    }
    
}

//*********************************
//ランキングの登録を行います(表示は別)
//*********************************
-(void)checkRanking:(UILabel *)ScoreLabel mode:(int)modeFlg{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //点数の比較を行うためInt型変数を用意します
    NSString *castStr = [NSString stringWithFormat:@"%@",ScoreLabel.text];
    int castInt = [castStr intValue];
    
    int scoreNum;
    
    
    //ランキング入賞時の日時の代入
    //NSString *rankDay = [self getTime];
    
    //ランキングにプレイヤー名を代入します
    NSString *playerName = appDelegate.playerNameStr;
    
    //スコアとランキング日を文字列結合します
    UILabel *ScoreStr = [[UILabel alloc]init];
    ScoreStr.text= [NSString stringWithFormat:@"%@  %@",ScoreLabel.text,playerName];
    //ScoreStr.font = [UIFont fontWithName:@"Chalkboard SE" size:10];
    //DLog(@"ランク日：%@",ScoreStr.text);
    
    //１０位までの結果を代入(ないデータはNULLになっているはず)
    if(modeFlg == 0){
        _rankingScoreUD1 = [NSUserDefaults standardUserDefaults];
        _rank1Str = [_rankingScoreUD1 stringForKey:@"KEY_Rank1"];
        _rankingScoreUD2 = [NSUserDefaults standardUserDefaults];
        _rank2Str = [_rankingScoreUD2 stringForKey:@"KEY_Rank2"];
        _rankingScoreUD3 = [NSUserDefaults standardUserDefaults];
        _rank3Str = [_rankingScoreUD3 stringForKey:@"KEY_Rank3"];
        _rankingScoreUD4 = [NSUserDefaults standardUserDefaults];
        _rank4Str = [_rankingScoreUD4 stringForKey:@"KEY_Rank4"];
        _rankingScoreUD5 = [NSUserDefaults standardUserDefaults];
        _rank5Str = [_rankingScoreUD5 stringForKey:@"KEY_Rank5"];
        _rankingScoreUD6 = [NSUserDefaults standardUserDefaults];
        _rank6Str = [_rankingScoreUD6 stringForKey:@"KEY_Rank6"];
        _rankingScoreUD7 = [NSUserDefaults standardUserDefaults];
        _rank7Str = [_rankingScoreUD7 stringForKey:@"KEY_Rank7"];
        _rankingScoreUD8 = [NSUserDefaults standardUserDefaults];
        _rank8Str = [_rankingScoreUD8 stringForKey:@"KEY_Rank8"];
        _rankingScoreUD9 = [NSUserDefaults standardUserDefaults];
        _rank9Str = [_rankingScoreUD9 stringForKey:@"KEY_Rank9"];
        _rankingScoreUD10 = [NSUserDefaults standardUserDefaults];
        _rank10Str = [_rankingScoreUD10 stringForKey:@"KEY_Rank10"];
    }else if(modeFlg == 2){
        //チャレンジモード
        _cngrankScoreUD1 = [NSUserDefaults standardUserDefaults];
        _rank1Str = [_cngrankScoreUD1 stringForKey:@"KEY_CRank1"];
        _cngrankScoreUD2 = [NSUserDefaults standardUserDefaults];
        _rank2Str = [_cngrankScoreUD2 stringForKey:@"KEY_CRank2"];
        _cngrankScoreUD3 = [NSUserDefaults standardUserDefaults];
        _rank3Str = [_cngrankScoreUD3 stringForKey:@"KEY_CRank3"];
        _cngrankScoreUD4 = [NSUserDefaults standardUserDefaults];
        _rank4Str = [_cngrankScoreUD4 stringForKey:@"KEY_CRank4"];
        _cngrankScoreUD5 = [NSUserDefaults standardUserDefaults];
        _rank5Str = [_cngrankScoreUD5 stringForKey:@"KEY_CRank5"];
        _cngrankScoreUD6 = [NSUserDefaults standardUserDefaults];
        _rank6Str = [_cngrankScoreUD6 stringForKey:@"KEY_CRank6"];
        _cngrankScoreUD7 = [NSUserDefaults standardUserDefaults];
        _rank7Str = [_cngrankScoreUD7 stringForKey:@"KEY_CRank7"];
        _cngrankScoreUD8 = [NSUserDefaults standardUserDefaults];
        _rank8Str = [_cngrankScoreUD8 stringForKey:@"KEY_CRank8"];
        _cngrankScoreUD9 = [NSUserDefaults standardUserDefaults];
        _rank9Str = [_cngrankScoreUD9 stringForKey:@"KEY_CRank9"];
        _cngrankScoreUD10 = [NSUserDefaults standardUserDefaults];
        _rank10Str = [_cngrankScoreUD10 stringForKey:@"KEY_CRank10"];
    }

    
    //DLog(@"%@",_rank1Str);
    
    if(_rank1Str == nil){
        //1位にデータがない場合
        _rank1Str = ScoreStr.text;
        appDelegate.prizeNumber =1;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank1Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            _rank10Str = _rank9Str;
            _rank9Str = _rank8Str;
            _rank8Str = _rank7Str;
            _rank6Str = _rank5Str;
            _rank5Str = _rank4Str;
            _rank4Str = _rank3Str;
            _rank3Str = _rank2Str;
            _rank2Str = _rank1Str;
            _rank1Str = ScoreStr.text;
            appDelegate.prizeNumber =1;
            [self NSUserDefRegistScore:modeFlg];
            return;
            
        }
    }

    if(_rank2Str == nil){
        //2位にデータがない場合
        _rank2Str = ScoreStr.text;
        appDelegate.prizeNumber =2;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank2Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            _rank10Str = _rank9Str;
            _rank9Str = _rank8Str;
            _rank8Str = _rank7Str;
            _rank6Str = _rank5Str;
            _rank5Str = _rank4Str;
            _rank4Str = _rank3Str;
            _rank3Str = _rank2Str;
            _rank2Str = ScoreStr.text;
            appDelegate.prizeNumber =2;
            [self NSUserDefRegistScore:modeFlg];
            return;
            
        }
    }


    if(_rank3Str == nil){
        //３位にデータがない場合
        _rank3Str = ScoreStr.text;
        appDelegate.prizeNumber =3;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank3Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            _rank10Str = _rank9Str;
            _rank9Str = _rank8Str;
            _rank8Str = _rank7Str;
            _rank6Str = _rank5Str;
            _rank5Str = _rank4Str;
            _rank4Str = _rank3Str;
            _rank3Str = ScoreStr.text;
            appDelegate.prizeNumber =3;
            [self NSUserDefRegistScore:modeFlg];
            return;
            
        }
    }
    
    if(_rank4Str == nil){
        //4位にデータがない場合
        _rank4Str = ScoreStr.text;
        appDelegate.prizeNumber =4;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank4Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            _rank10Str = _rank9Str;
            _rank9Str = _rank8Str;
            _rank8Str = _rank7Str;
            _rank6Str = _rank5Str;
            _rank5Str = _rank4Str;
            _rank4Str = ScoreStr.text;
            appDelegate.prizeNumber =4;
            [self NSUserDefRegistScore:modeFlg];
            return;
            
        }
    }
    
    if(_rank5Str == nil){
        //5位にデータがない場合
        _rank5Str = ScoreStr.text;
        appDelegate.prizeNumber =5;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank5Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =5;
            _rank10Str = _rank9Str;
            _rank9Str = _rank8Str;
            _rank8Str = _rank7Str;
            _rank7Str = _rank6Str;
            _rank6Str = _rank5Str;
            _rank5Str = ScoreStr.text;
            [self NSUserDefRegistScore:modeFlg];
            return;
            
        }
    }
    
    if(_rank6Str == nil){
        //6位にデータがない場合
        _rank6Str = ScoreStr.text;
        appDelegate.prizeNumber =6;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank6Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            _rank10Str = _rank9Str;
            _rank9Str = _rank8Str;
            _rank8Str = _rank7Str;
            _rank7Str = _rank6Str;
            _rank6Str = ScoreStr.text;
            appDelegate.prizeNumber =6;
            [self NSUserDefRegistScore:modeFlg];
            return;
            
        }
    }
    
    if(_rank7Str == nil){
        //7位にデータがない場合
        _rank7Str = ScoreStr.text;
        appDelegate.prizeNumber =7;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank7Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            _rank10Str = _rank9Str;
            _rank9Str = _rank8Str;
            _rank8Str = _rank7Str;
            _rank7Str = ScoreStr.text;
            appDelegate.prizeNumber =7;
            [self NSUserDefRegistScore:modeFlg];
            return;
        }
    }
    
    if(_rank8Str == nil){
        //8位にデータがない場合
        _rank8Str = ScoreStr.text;
        appDelegate.prizeNumber =8;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank8Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            _rank10Str = _rank9Str;
            _rank9Str = _rank8Str;
            _rank8Str = ScoreStr.text;
            appDelegate.prizeNumber =8;
            [self NSUserDefRegistScore:modeFlg];
            return;
        }
    }
    
    if(_rank9Str == nil){
        //9位にデータがない場合
        _rank9Str = ScoreStr.text;
        appDelegate.prizeNumber =9;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank9Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =9;
            _rank10Str = _rank9Str;
            _rank9Str = ScoreStr.text;
            [self NSUserDefRegistScore:modeFlg];
            return;
        }
    }
    
    if(_rank10Str == nil){
        //10位にデータがない場合
        _rank10Str = ScoreStr.text;
        appDelegate.prizeNumber =10;
        [self NSUserDefRegistScore:modeFlg];
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank10Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            _rank10Str = ScoreStr.text;
            appDelegate.prizeNumber =10;
            [self NSUserDefRegistScore:modeFlg];
            return;
        }
    }
    
    //ここまで到達するとランキング外です
    //ランキング外値は"0"です。
    appDelegate.prizeNumber =0;
    
}


//***************************************
//スコアが難易に入ったのかだけのチェックメソッド
//***************************************
-(void)checkFlgRanking:(UILabel *)ScoreLabel mode:(int)modeFlg{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //点数の比較を行うためInt型変数を用意します
    NSString *castStr = [NSString stringWithFormat:@"%@",ScoreLabel.text];
    int castInt = [castStr intValue];
    
    int scoreNum;
    
    //１０位までの結果を代入(ないデータはNULLになっているはず)
    if(modeFlg == 0){
        _rankingScoreUD1 = [NSUserDefaults standardUserDefaults];
        _rank1Str = [_rankingScoreUD1 stringForKey:@"KEY_Rank1"];
        _rankingScoreUD2 = [NSUserDefaults standardUserDefaults];
        _rank2Str = [_rankingScoreUD2 stringForKey:@"KEY_Rank2"];
        _rankingScoreUD3 = [NSUserDefaults standardUserDefaults];
        _rank3Str = [_rankingScoreUD3 stringForKey:@"KEY_Rank3"];
        _rankingScoreUD4 = [NSUserDefaults standardUserDefaults];
        _rank4Str = [_rankingScoreUD4 stringForKey:@"KEY_Rank4"];
        _rankingScoreUD5 = [NSUserDefaults standardUserDefaults];
        _rank5Str = [_rankingScoreUD5 stringForKey:@"KEY_Rank5"];
        _rankingScoreUD6 = [NSUserDefaults standardUserDefaults];
        _rank6Str = [_rankingScoreUD6 stringForKey:@"KEY_Rank6"];
        _rankingScoreUD7 = [NSUserDefaults standardUserDefaults];
        _rank7Str = [_rankingScoreUD7 stringForKey:@"KEY_Rank7"];
        _rankingScoreUD8 = [NSUserDefaults standardUserDefaults];
        _rank8Str = [_rankingScoreUD8 stringForKey:@"KEY_Rank8"];
        _rankingScoreUD9 = [NSUserDefaults standardUserDefaults];
        _rank9Str = [_rankingScoreUD9 stringForKey:@"KEY_Rank9"];
        _rankingScoreUD10 = [NSUserDefaults standardUserDefaults];
        _rank10Str = [_rankingScoreUD10 stringForKey:@"KEY_Rank10"];
    }else if(modeFlg == 2){
        _cngrankScoreUD1 = [NSUserDefaults standardUserDefaults];
        _rank1Str = [_cngrankScoreUD1 stringForKey:@"KEY_CRank1"];
        _cngrankScoreUD2 = [NSUserDefaults standardUserDefaults];
        _rank2Str = [_cngrankScoreUD2 stringForKey:@"KEY_CRank2"];
        _cngrankScoreUD3 = [NSUserDefaults standardUserDefaults];
        _rank3Str = [_cngrankScoreUD3 stringForKey:@"KEY_CRank3"];
        _cngrankScoreUD4 = [NSUserDefaults standardUserDefaults];
        _rank4Str = [_cngrankScoreUD4 stringForKey:@"KEY_CRank4"];
        _cngrankScoreUD5 = [NSUserDefaults standardUserDefaults];
        _rank5Str = [_cngrankScoreUD5 stringForKey:@"KEY_CRank5"];
        _cngrankScoreUD6 = [NSUserDefaults standardUserDefaults];
        _rank6Str = [_cngrankScoreUD6 stringForKey:@"KEY_CRank6"];
        _cngrankScoreUD7 = [NSUserDefaults standardUserDefaults];
        _rank7Str = [_cngrankScoreUD7 stringForKey:@"KEY_CRank7"];
        _cngrankScoreUD8 = [NSUserDefaults standardUserDefaults];
        _rank8Str = [_cngrankScoreUD8 stringForKey:@"KEY_CRank8"];
        _cngrankScoreUD9 = [NSUserDefaults standardUserDefaults];
        _rank9Str = [_cngrankScoreUD9 stringForKey:@"KEY_CRank9"];
        _cngrankScoreUD10 = [NSUserDefaults standardUserDefaults];
        _rank10Str = [_cngrankScoreUD10 stringForKey:@"KEY_CRank10"];

    }
    
    
    //DLog(@"%@",_rank1Str);
    
    if(_rank1Str == nil){
        //1位にデータがない場合
        appDelegate.prizeNumber =1;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank1Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =1;
            return;
        }
    }
    
    if(_rank2Str == nil){
        //2位にデータがない場合
        appDelegate.prizeNumber =2;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank2Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =2;
            return;
            
        }
    }
    
    
    if(_rank3Str == nil){
        //３位にデータがない場合
        appDelegate.prizeNumber =3;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank3Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =3;
            return;
            
        }
    }
    
    if(_rank4Str == nil){
        //4位にデータがない場合
        appDelegate.prizeNumber =4;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank4Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =4;
            return;
        }
    }
    
    if(_rank5Str == nil){
        //5位にデータがない場合
        appDelegate.prizeNumber =5;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank5Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =5;
            return;
        }
    }
    
    if(_rank6Str == nil){
        //6位にデータがない場合
        appDelegate.prizeNumber =6;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank6Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =6;
            return;
        }
    }
    
    if(_rank7Str == nil){
        //7位にデータがない場合
        appDelegate.prizeNumber =7;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank7Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =7;
            return;
        }
    }
    
    if(_rank8Str == nil){
        //8位にデータがない場合
        appDelegate.prizeNumber =8;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank8Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =8;
            return;
        }
    }
    
    if(_rank9Str == nil){
        //9位にデータがない場合
        appDelegate.prizeNumber =9;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank9Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =9;
            return;
        }
    }
    
    if(_rank10Str == nil){
        //10位にデータがない場合
        appDelegate.prizeNumber =10;
        return;
    }else{
        //元データと比較します
        scoreNum = [_rank10Str intValue];
        
        //ランクを１つずつずらします
        if(scoreNum == castInt || scoreNum < castInt){
            appDelegate.prizeNumber =10;
            return;
        }
    }
    
    //ここまで到達するとランキング外です
    //ランキング外値は"0"です。
    appDelegate.prizeNumber =0;
    
}


//********************************
//最終的な各モード別ランキングの登録
//********************************
-(void)NSUserDefRegistScore:(int)modeFlg{
    
    DLog(@"%@",_rank1Str);
    
    if(modeFlg==0){
        //最後にUserDefaultに保存します
        _rankingScoreUD1 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD1 setObject:_rank1Str forKey:@"KEY_Rank1"];
        [_rankingScoreUD1 synchronize];
    
        //NSString *score = [_rankingScoreUD1 stringForKey:@"KEY_Rank1"];
        //DLog(@"%@",score);
    
        _rankingScoreUD2 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD2 setObject:_rank2Str forKey:@"KEY_Rank2"];
        [_rankingScoreUD2 synchronize];
    
        _rankingScoreUD3 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD3 setObject:_rank3Str forKey:@"KEY_Rank3"];
        [_rankingScoreUD3 synchronize];
    
        _rankingScoreUD4 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD4 setObject:_rank4Str forKey:@"KEY_Rank4"];
        [_rankingScoreUD4 synchronize];
    
        _rankingScoreUD5 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD5 setObject:_rank5Str forKey:@"KEY_Rank5"];
        [_rankingScoreUD5 synchronize];
    
        _rankingScoreUD6 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD6 setObject:_rank6Str forKey:@"KEY_Rank6"];
        [_rankingScoreUD6 synchronize];
    
        _rankingScoreUD7 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD7 setObject:_rank7Str forKey:@"KEY_Rank7"];
        [_rankingScoreUD7 synchronize];
    
        _rankingScoreUD8 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD8 setObject:_rank8Str forKey:@"KEY_Rank8"];
        [_rankingScoreUD8 synchronize];
    
        _rankingScoreUD9 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD9 setObject:_rank9Str forKey:@"KEY_Rank9"];
        [_rankingScoreUD9 synchronize];
    
        _rankingScoreUD10 = [NSUserDefaults standardUserDefaults];
        [_rankingScoreUD10 setObject:_rank10Str forKey:@"KEY_Rank10"];
        [_rankingScoreUD10 synchronize];
    }else if (modeFlg == 2){
        //最後にUserDefaultに保存します
        _cngrankScoreUD1 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD1 setObject:_rank1Str forKey:@"KEY_CRank1"];
        [_cngrankScoreUD1 synchronize];
        
        _cngrankScoreUD2 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD2 setObject:_rank2Str forKey:@"KEY_CRank2"];
        [_cngrankScoreUD2 synchronize];
        
        _cngrankScoreUD3 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD3 setObject:_rank3Str forKey:@"KEY_CRank3"];
        [_cngrankScoreUD3 synchronize];
        
        _cngrankScoreUD4 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD4 setObject:_rank4Str forKey:@"KEY_CRank4"];
        [_cngrankScoreUD4 synchronize];
        
        _cngrankScoreUD5 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD5 setObject:_rank5Str forKey:@"KEY_CRank5"];
        [_cngrankScoreUD5 synchronize];
        
        _cngrankScoreUD6 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD6 setObject:_rank6Str forKey:@"KEY_CRank6"];
        [_cngrankScoreUD6 synchronize];
        
        _cngrankScoreUD7 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD7 setObject:_rank7Str forKey:@"KEY_CRank7"];
        [_cngrankScoreUD7 synchronize];
        
        _cngrankScoreUD8 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD8 setObject:_rank8Str forKey:@"KEY_CRank8"];
        [_cngrankScoreUD8 synchronize];
        
        _cngrankScoreUD9 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD9 setObject:_rank9Str forKey:@"KEY_CRank9"];
        [_cngrankScoreUD9 synchronize];
        
        _cngrankScoreUD10 = [NSUserDefaults standardUserDefaults];
        [_cngrankScoreUD10 setObject:_rank10Str forKey:@"KEY_CRank10"];
        [_cngrankScoreUD10 synchronize];
        
    }
    
}



@end
