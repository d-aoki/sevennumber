//
//  -メモ-
//  主にゲームのBGMや効果音を設定するクラスです
//
//  SoundsUtils.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/06.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "SoundsUtils.h"
#import "AudioPlayer.h"

#import <AVFoundation/AVFoundation.h>


//7を作る効果音３つ
NSString * const redSeven = @"redSeven";
NSString * const blueSeven = @"blueSeven";
NSString * const yellowSeven = @"yellowSeven";

@implementation SoundsUtils
{
    //7作成音格納変数
    NSString *SevenSoundStr;
    
    //日付データ取得変数
    NSCalendar *calendar;
    NSUInteger flags;
    NSDateComponents *comps;
}


#pragma mark - BGM

//********************************************************
//プレイ中のBGMの設定を行うメソッド
//ランキングモード用
//********************************************************
-(AVAudioPlayer *)RankingbgmSoundsSetting:(NSString *)soundName{
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"];
   
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    
    //無限ループ
    //audio.numberOfLoops = -1;
    audio.volume = 0.2;
    
    return audio;
}


//********************************************************
//プレイ中のBGMの設定を行うメソッド
//フリーモード用(時間によってBGMを「朝」「夜」バージョンに分けます)
//********************************************************
-(AVAudioPlayer *)bgmSoundsSetting{
    
    //現在の時間取得
    int bgmFlg = [self getTime];
    NSString *path;
    
    //夜のイメージ
    if(bgmFlg == 2){
        path = [[NSBundle mainBundle] pathForResource:@"night" ofType:@"mp3"];
    }else if(bgmFlg == 1){
    //朝のイメージ
        path = [[NSBundle mainBundle] pathForResource:@"neture" ofType:@"mp3"];
    }else{
    //昼のイメージ
        path = [[NSBundle mainBundle] pathForResource:@"morning" ofType:@"mp3"];
    }
    
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    
    //無限ループ
    audio.numberOfLoops = -1;
    audio.volume = 0.2;
    
    return audio;
}

//************************************
//BGMのフェードアウト
//************************************
-(void)doVolumeFade:(AVAudioPlayer *)audio
{
    
    if (audio.volume > 0.1) {
        audio.volume = audio.volume - 0.1;
        [self performSelector:@selector(doVolumeFade:) withObject:nil afterDelay:0.5];
	} else {
        // Stop and get the sound ready for playing again
        [audio stop];
        audio.currentTime = 0;
        [audio prepareToPlay];
        audio.volume = 0.2;
    }
    
}

//**********************************
//現在の時間を取得します
//時間によってフリーモードのBGMをかえます
//**********************************
-(int)getTime{
    
    int bgmFlg;
    
    //現在日付を取得
    NSDate *nowData = [NSDate date];
    
    calendar = [NSCalendar currentCalendar];
    // 時・分・秒を取得
    flags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    comps = [calendar components:flags fromDate:nowData];
    
    NSInteger hour = comps.hour;
  
    //0~5時または17~23時は「夜」フラグ,11~16時までは日中フラグ,それ以外は朝フラグ
    if((hour >= 0 && hour <= 5) || (hour >= 17 && hour <= 23)){
        DLog(@"夜です");
        bgmFlg = 2;
    }else if(hour >= 11 && hour <=16){
        DLog(@"日中です");
        bgmFlg = 1;
    }else{
        DLog(@"朝です");
        bgmFlg = 0;
    }
    
    return bgmFlg;
}


#pragma mark - Effect

//*************************************
//ブロックをタッチした時の音階判別
//効果音をならすメソッド
//*************************************
-(AVAudioPlayer *)tapBlocksSound:(NSString *)soundName{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    //音量
    audio.volume = 0.2;
    
    return audio;
}


//*************************************
//7ブロックができた場合の判別処理(色分け)
//効果音をならすメソッド
//*************************************
-(AVAudioPlayer *)createSevenBlocksSound:(UIView *)sevenBlock{
    
    if(sevenBlock.tag == 1){
        SevenSoundStr = redSeven;
    }else if (sevenBlock.tag == 2){
        SevenSoundStr = blueSeven;
    }else if (sevenBlock.tag == 3){
        SevenSoundStr = yellowSeven;
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:SevenSoundStr ofType:@"wav"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    //音量
    audio.volume = 0.2;
    
    return audio;
}


//**********************
//メニュー選択音
//**********************
-(AVAudioPlayer *)selectMenuSounds{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"selectMenu" ofType:@"wav"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    //音量
    audio.volume = 0.2;

    return audio;
}

//********************
//戻るボタン選択音
//********************
-(AVAudioPlayer *)backMenuSounds{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"backsounds" ofType:@"wav"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    //音量
    audio.volume = 0.2;
    
    return audio;
}


//********************
//その他効果音
//********************
-(AVAudioPlayer *)effectSounds:(NSString *)sound{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:sound ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    //音量
    audio.volume = 0.2;
    
    return audio;
}

@end
