//
//  CSSAdView.h
//  ScheduleConcierge
//
//  AdMobメディエーションを利用してバナー広告を表示するための便利クラスです。
//
//  Created by Natsuki Funaki on 2013/07/02.
//  Copyright (c) 2013年 Takashi Yasukawa. All rights reserved.
//
//  
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import <QuartzCore/QuartzCore.h>

typedef enum {
    CSSStaticBannerNone = 0,
    CSSStaticBanner30plusminus
} CSSStaticBannerType;

@interface CSSAdView : UIView <GADBannerViewDelegate, UIWebViewDelegate>

@property (weak, nonatomic) UIViewController *rootViewController;
@property (strong, nonatomic) GADBannerView *gadBannerView;
@property (strong, nonatomic) GADRequest *gadRequest;
@property (assign, nonatomic) CSSStaticBannerType staticBannerType;


/**
 * admobのメディエーションの仕組みを利用して広告を表示します。
 * delegateは必須ではなく、nilでも構いません。（デフォルトの動作をCSSAdView内で行う）
 */
+ (id)adViewForAdmobMediationWithAdSize:(CGSize)size
                     rootViewController:(id)viewController
                           mediationID:(NSString*)mediationID
                               delegate:(id<GADBannerViewDelegate>)delegate;

// admob
- (void)setAdmobMediationID:(NSString*)mediationID;
- (void)setAdmobBannerDelegate:(id<GADBannerViewDelegate>)delegate;
- (void)admobEnableTestModeWithTestDevices:(NSArray*)testDevices;

/**
 * admob広告の表示を開始します。
 */
- (void)admobMediationRequestStart;

/**
 * admob広告の表示を停止します。
 * 広告を表示している画面が非表示の場合など、停止した方が望ましいです。
 */
- (void)admobMediationRequestStop;


////////////////////////////////////////////////////////////////////
// static
/**
 * 会社の固定的なバナーを表示します。（30+-）
 */
+ (id)adViewForCSSStaticBannerWithType:(CSSStaticBannerType)type rootViewController:(UIViewController*)viewController;

- (NSString*)serviceURLWithCSSStaticBannerType:(CSSStaticBannerType)type;
- (void)staticBannerTapped:(UITapGestureRecognizer*)tapGestureRecoginzer;

////////////////////////////////////////////////////////////////////
- (void)moveAdViewTheEntireWithView:(GADBannerView*)view;

@end
