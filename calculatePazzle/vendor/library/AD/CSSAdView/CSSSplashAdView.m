//
//  CSSSplashAdView.m
//  CSJukeBox
//
//  Created by Natsuki Funaki on 2014/03/05.
//  Copyright (c) 2014年 CarolSystemSendai Co.,Ltd. All rights reserved.
//

#import "CSSSplashAdView.h"
#import <QuartzCore/QuartzCore.h>

#import "appCCloud.h"

@interface CSSSplashAdView ()
@property (strong, nonatomic) UIButton *amoAdTriggerButton;

@end

@implementation CSSSplashAdView

+ (CSSSplashAdView*)splashAdViewWithSources:(CSSSplashAdSource)adSources viewController:(UIViewController*)viewController dismissCompletion:(void (^)(void))completion {
    CGFloat offsetY = 0;
    if (!viewController.navigationController.navigationBarHidden) {
        offsetY = viewController.navigationController.navigationBar.bounds.size.height + 20;
    }
    
    CGRect fullRect = [CSSSplashAdView currentScreenBoundsDependOnOrientation];
    
    CSSSplashAdView *splashAdView = [[CSSSplashAdView alloc] initWithFrame:CGRectMake(0, offsetY, fullRect.size.width, fullRect.size.height - offsetY)];
    splashAdView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    splashAdView.adSources = adSources;
    splashAdView.viewController = viewController;
    splashAdView.delegate = splashAdView;
    splashAdView.adDismissCompletion = completion;
    
    // Ad設定
    //////////////////////////////////////////////////
    // appC
    if (adSources & CSSSplashAdSourceAppC) {
        DLog(@"appC広告を使う");
        [appCCloud setupAppCWithMediaKey:APPC_CLOUD_MEDIA_KEY option:APPC_CLOUD_AD];
    }
    
    //////////////////////////////////////////////////
    // MMedia の広告
    if (adSources & CSSSplashAdSourceMMAd) {
        DLog(@"MMedia広告を使う");
        [MMSDK initialize]; //Initialize a Millennial Media session
        
        //Create a location manager for passing location data for conversion tracking and ad requests
        splashAdView.locationManager = [[CLLocationManager alloc] init];
        [splashAdView.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        [splashAdView.locationManager startUpdatingLocation];
        
        
        MMRequest *request = [MMRequest requestWithLocation:splashAdView.locationManager.location];
        
        // Replace Your_APID with the APID provided to you by Millennial Media
        MMAdView *rectangle = [[MMAdView alloc] initWithFrame:splashAdView.bounds apid:MMEDIA_MAIN_RECTANGLE_APID rootViewController:viewController];
        
        splashAdView.mmRequest = request;
        splashAdView.mmAdView = rectangle;
    }
    
    //////////////////////////////////////////////////
    // AppliPromotion の広告
    // 設定は全てAppliPromotionの用意する設定ファイルで。
    if (adSources & CSSSplashAdSourceAMoAd) {
        DLog(@"AMoAd広告を使う");
        [AMoAdSDK sendUUID];
        // ボタン作成
     	[AMoAdSDK sendTriggerID:viewController
                        trigger:APPLI_PROMOTION_WALL_TRIGGER_ID
                       callback:@selector(amoadWallCallbackMethod:URL:width:height:)];
   
    }
    //////////////////////////////////////////////////
    // 消すボタン (右上隅)
    UIButton *adDismissButton = [[UIButton alloc] initWithFrame:CGRectMake(splashAdView.frame.size.width - 40, 20, 40, 40)];
    adDismissButton.backgroundColor = [UIColor redColor];
    adDismissButton.layer.cornerRadius = 20.0f;
    adDismissButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:26];
    [adDismissButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [adDismissButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [adDismissButton setTitle:@"×" forState:UIControlStateNormal];
    [adDismissButton addTarget:splashAdView action:@selector(adDismissSelected) forControlEvents:UIControlEventTouchUpInside];
    adDismissButton.hidden = YES;
    splashAdView.dismissButton = adDismissButton;
    
    return splashAdView;
}

- (void)showSplashAdWithAdSource:(CSSSplashAdSource)adSource {

    if (adSource & CSSSplashAdSourceAppC) {
        [self showAppC];
        
    } else if (adSource & CSSSplashAdSourceAMoAd) {
        [self showAMoAd];
        
    } else if (adSource & CSSSplashAdSourceMMAd) {
        [self showMMAd];

    } else {
        ALog(@"定義されていないソースです adSource:%d", adSource);
    }
}

- (void)hideSplahAd {
    self.mmAdView.hidden =
    self.appCCutinView.hidden =
    self.amoAdTriggerButton.hidden =
    self.dismissButton.hidden = YES;
    
    [self.dismissButton removeFromSuperview];
    
    if (self.appCCutinView) {
        [self.appCCutinView removeFromSuperview];
        self.appCCutinView = nil;
    }
    
    if (self.amoAdView) {
        [self.amoAdTriggerButton removeFromSuperview];
    }
    
    if (self.mmAdView) {
    }
    
    [self removeFromSuperview];
    
    if (self.adDismissCompletion != Nil) {
        self.adDismissCompletion();
    }
}

- (void)showAppC {
    if (self.appCCutinView) {
        [self.appCCutinView removeFromSuperview];
        self.appCCutinView = nil;
    }
    
    appCCutinView *cutinView = [[appCCutinView alloc] initWithViewController:self.viewController closeTarget:self closeAction:@selector(hideSplahAd)];
    self.appCCutinView = cutinView;
    self.dismissButton.hidden = NO;
    
    [self addSubview:cutinView];
    [self addSubview:self.dismissButton];

    [self.viewController.view addSubview:self];
}

- (void)showAMoAd {
    if (self.amoAdTriggerButton) {
        [self.amoAdTriggerButton removeFromSuperview];
    }
    
    if (self.amoAdTriggerButton) {
        self.dismissButton.hidden = NO;

        [self addSubview:self.amoAdTriggerButton];
        [self addSubview:self.dismissButton];

        [self.viewController.view addSubview:self];
    }
}

- (void)showMMAd {
    __weak __block CSSSplashAdView *blockSelf = self;
    [self addSubview:self.dismissButton];
    [self.mmAdView getAdWithRequest:self.mmRequest onCompletion:^(BOOL success, NSError *error) {
        if (success) {
            DLog(@"RECTANGLE AD REQUEST SUCCEEDED");
            dispatch_async(dispatch_get_main_queue(), ^{
                blockSelf.mmAdView.center = blockSelf.center;
                
                blockSelf.mmAdView.hidden =
                blockSelf.dismissButton.hidden = NO;
            });
            
        } else {
            DLog(@"RECTANGLE AD REQUEST FAILED WITH ERROR: %@", error);
            
        }
    }];

}

- (void)adDismissSelected {
    [self hideSplahAd];
}

- (void)amoadWallCallbackMethod:(NSInteger)sts
						 URL:(NSString *)url
					   width:(NSInteger)width
					  height:(NSInteger)height
{
    DLog(@"");
	/* Wall誘導枠ボタンの作成 */
    UIButton *buttonTrigger = [UIButton buttonWithType:UIButtonTypeCustom];
	[buttonTrigger setTitle:@"" forState:UIControlStateNormal];
	[buttonTrigger addTarget:self
					  action:@selector(amoAdWallButtonPushTrigger)
			forControlEvents:UIControlEventTouchUpInside];
	if (sts == 0) {
		// 正常終了
		buttonTrigger.frame = CGRectMake( 0, 0, (CGFloat)width, (CGFloat)height);
		
		// 取得した画像をボタンに貼り付ける
		NSURL *myURL = [NSURL URLWithString:url];
		NSData *myData = [NSData dataWithContentsOfURL:myURL];
		[buttonTrigger setBackgroundImage:[UIImage imageWithData:myData] forState:UIControlStateNormal];
        
	} else {
		// エラーが発生した時、Wall導入枠ボタンに代替画像を貼り付ける
		buttonTrigger.frame = CGRectMake(0, 100, 320, 91);
		UIImage *LocalImg = [UIImage imageNamed:@"amoadsample.png"];
		[buttonTrigger setBackgroundImage:LocalImg forState:UIControlStateNormal];
	}
	[buttonTrigger sizeToFit];
    
    self.amoAdTriggerButton = buttonTrigger;
}

- (void)amoAdWallButtonPushTrigger {
	[AMoAdSDK pushTrigger:self.viewController TriggerID:APPLI_PROMOTION_WALL_TRIGGER_ID];
	DLog(@"buttonPushTriggerClick");
}



+ (CGRect)currentScreenBoundsDependOnOrientation {
    
    CGRect screenBounds = [UIScreen mainScreen].bounds ;
    CGFloat width = CGRectGetWidth(screenBounds)  ;
    CGFloat height = CGRectGetHeight(screenBounds) ;
    UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    
    if(UIInterfaceOrientationIsPortrait(interfaceOrientation)){
        screenBounds.size = CGSizeMake(width, height);
    }else if(UIInterfaceOrientationIsLandscape(interfaceOrientation)){
        screenBounds.size = CGSizeMake(height, width);
    }
    return screenBounds ;
}

- (void)dealloc {
    DLog(@"dealloc!");
    self.delegate = nil;
    self.locationManager = nil;
}

@end
