//
//  PlayerNameEditViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/18.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//
#import <UIKit/UIKit.h>


@protocol PlayerNameEditViewControllerDelegate;

@interface PlayerNameEditViewController : UIViewController<UITextFieldDelegate>

//ランキングメッセージ
@property (weak, nonatomic) IBOutlet UILabel *prizeMsg;
@property (weak, nonatomic) IBOutlet UILabel *congMsg;

//プレイヤー名登録View
@property (weak, nonatomic) IBOutlet UIView *congView;

//PlaerName入力textField
@property (weak, nonatomic) IBOutlet UITextField *PlayerNTextBox;

//modeTitle
@property (weak, nonatomic) IBOutlet UILabel *modeTitleLabel;


//決定ボタン
- (IBAction)DoneBtn:(id)sender;


@property (weak,nonatomic) id<PlayerNameEditViewControllerDelegate>delegate;

@end

@protocol PlayerNameEditViewControllerDelegate <NSObject>

- (void)closePickerView:(PlayerNameEditViewController *)controller;

@end