//
//  AppDelegate.h
//  calculatePazzle
//
//  Created by Daichi Aoki on 2014/02/17.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "appCCloud.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (nonatomic) NSString  *sbName;

//スコアの受け渡し変数
@property (nonatomic,assign) int levelDate;
@property (nonatomic,assign) int scoreDate;

//ランキングモードとフリーモードの判別
//0 -> タイムアタック,1 -> フリー ,2 -> チャレンジ
@property (nonatomic,assign) int modeFlg;

//スプラッシュ広告の表示フラグ
@property (nonatomic,assign) BOOL splashAdFlg;

//appC cloud情報の格納変数
@property (nonatomic,assign) appCSimpleView *simpleAdView;

//BGMの受け渡し変数
@property (nonatomic,assign) AVAudioPlayer *AppBGMSound;

//スプラッシュ広告のカウント変数(5回に１回)
@property (nonatomic,assign) int splashCnt;


//ランキング系

//ランキング代入配列
@property (nonatomic,assign) NSArray *rankingArray;
//ランキング順位判別フラグ
@property (nonatomic,assign) int prizeNumber;

//入賞メッセージ格納変数
@property (nonatomic,assign) NSString *prizeMsg;
//入賞した際のプレイヤー名を入力します
@property (nonatomic,assign)NSString *playerNameStr;

@end
