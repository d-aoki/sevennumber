//
//  MainViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/02/28.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "MainViewController.h"
#import "ViewController.h"
#import "DCSocial.h"
#import "SoundsUtils.h"
#import "DesignUtils.h"
#import "AppDelegate.h"

#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>

//広告用(バナー、スプラッシュ)
#import "CSSAdView.h"
#import "CSSSplashAdView.h"
#import "appCCloud.h"

#import "RankingViewController.h"
#import "ProcessingViewUtils.h"
#import "RankingUtils.h"

//
#import <GameKit/GameKit.h>

//splash広告の表示頻度
int const splash_num_cnt = 3;

//freeplayボタンのRGB値
float const playLabelR = 1.0;
float const playLabelG = 0.753;
float const playLabelB = 0.796;
float const playLabelAlpha = 1.0;

//optionボタンのRGB値
float const optionLabelR = 1.0;
float const optionLabelG = 0.98;
float const optionLabelB = 0.803;
float const optionLabelAlpha = 1.0;

//timeAttackplayボタンのRGB値
float const rankplayLabelR = 0.59;
float const rankplayLabelG = 0.90;
float const rankplayLabelB = 0.48;
float const rankplayLabelAlpha = 1.0;

//ChallengeLabelのRGB値
float const challplayLabelR = 1.0;
float const challrankplayLabelG = 0.388;
float const challrankplayLabelB = 0.278;
float const challrankplayLabelAlpha = 1.0;

//RankingのRGB値
float const rankingLabelR = 0.82;
float const rankingLabelG = 0.93;
float const rankingLabelB = 0.99;
float const rankingLabelAlpha = 1.0;

@interface MainViewController ()

//広告系(バナー、スプラッシュ)
@property (strong,nonatomic) CSSAdView *bannerAdView;
@property (strong,nonatomic) CSSSplashAdView *splashAdView;

@end

@implementation MainViewController
{
    //メニュー選択音
    AVAudioPlayer *selectSounds;
    
    //メニューボタンの連続押し防止
    BOOL touchPlayFlg,touchFreePlayFlg,touchChallengePlayFlg;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //アプリダウンロード後、最初の一回だけチュートリアルを流します。
    
    NSUserDefaults *appTutorialUD = [NSUserDefaults standardUserDefaults];
    int appCnt = [appTutorialUD integerForKey:@"KEY_AppCnt"];

    
    if(appCnt == 0){
        //playViewのインスタンス
        ViewController *playViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialMainViewController"];
        
        //フェードアウトしながらアニメーション遷移
        [self pushViewControllerAnimted:playViewController];
        
        //カウントをインクリメントします
        appCnt++;
        [appTutorialUD setInteger:appCnt forKey:@"KEY_AppCnt"];
        [appTutorialUD synchronize];
    }
    
    
    //起動時にもランキングを読み込みます
    RankingUtils *rankUtils = [[RankingUtils alloc]init];
    [rankUtils rankingRegist:nil modeFlg:3];
    
    
    //playモードの説明ラベル
    /////////////////////
    // フリーラベルを45度回転
    CGFloat angle = -45.0 * M_PI / 180.0;
    _timeAtackLabel.transform = CGAffineTransformMakeRotation(angle);
    _freePlayLabel.transform = CGAffineTransformMakeRotation(angle);
    _challengeLabel.transform = CGAffineTransformMakeRotation(angle);
        
    
    //butoonLabel
    NSArray *labelArray = [[NSArray alloc]initWithObjects:_rankPlayLabel,_playLabel,_optionLabel,_rankingLabel,_challengePlayLabel, nil];
    //title
    NSArray *titleArray = [[NSArray alloc]initWithObjects:_titleSeven,_titleNumber, nil];
    
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils labelshadow:labelArray];
    [proUtils titleLabelshadow:titleArray];
    
    //バージョンラベルは個別で加工します
    _versionLabel.shadowColor = [UIColor lightGrayColor];
    _versionLabel.shadowOffset = CGSizeMake(1,2);
    _appVersionLabel.shadowColor = [UIColor lightGrayColor];
    _appVersionLabel.shadowOffset = CGSizeMake(1,2);
    
    
    
    //このクラスでは起動時の最初の一回ののみ呼ばれます
    //デフォルト背景
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    //[designUtils gradation:self.view];
    [designUtils backImageSettingAct:self.view];
    
    //BOOL初期化
    //メニューボタン押しを許可します
    touchPlayFlg =YES;
    touchFreePlayFlg = YES;
    touchChallengePlayFlg = YES;
    
    
    //ランキングモードとフリーモードの判別
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    appDelegate.modeFlg = 0;
    
    //appVersion
    [self appVersion];
    
    //メニューラベルのRGB値設定
    _playLabel.textColor = [UIColor colorWithRed:playLabelR green:playLabelG blue:playLabelB alpha:playLabelAlpha];
    _optionLabel.textColor = [UIColor colorWithRed:optionLabelR green:optionLabelG blue:optionLabelB alpha:optionLabelAlpha];
    _rankPlayLabel.textColor = [UIColor colorWithRed:rankplayLabelR green:rankplayLabelG blue:rankplayLabelB alpha:rankplayLabelAlpha];
    _challengePlayLabel.textColor = [UIColor colorWithRed:challplayLabelR green:challrankplayLabelG blue:challrankplayLabelB alpha:challrankplayLabelAlpha];
    
    _rankingLabel.textColor = [UIColor colorWithRed:rankingLabelR green:rankingLabelG blue:rankingLabelB alpha:rankingLabelAlpha];
    
    ////////////////////////
    // 広告
    
#if AD_DISABLE
    DLog(@"広告非表示");
#else
    
    DLog(@"広告表示");
    //フラグが"NO"の場合はスプラッシュ広告を出さない
    //初期値設定(NO)
    //[splash]
    appDelegate.splashAdFlg = NO;
    //splash広告カウントの初期化
    appDelegate.splashCnt = 0;
    
    
    // [banner <appC cloud>]
    //メディアキーを指定して、広告情報取得
    [appCCloud setupAppCWithMediaKey:APPC_CLOUD_MEDIA_KEY option:APPC_CLOUD_AD];
    
    // [bannaer<AdMob>]
    CGSize adSize = GAD_SIZE_320x50;
    
    CSSAdView *adView = [CSSAdView adViewForAdmobMediationWithAdSize:adSize rootViewController:self mediationID:ADMOB_MEDIATION_ID delegate:nil];
    [self.view addSubview:adView];
    self.bannerAdView = adView;
    
#endif
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    //gameCenterのログイン情報の読み込み
    [self authenticateLocalPlayer];
    
    // バナー広告
    self.bannerAdView.frame = CGRectMake(0,STATUSBAR_HEIGHT, self.bannerAdView.bounds.size.width, self.bannerAdView.bounds.size.height);
    
    //DLog(@"広告表示開始");
    [self.bannerAdView admobMediationRequestStart];

    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    //表示位置の選択
    appCSimpleView *simpleView = [[appCSimpleView alloc]initWithViewController:self vertical:appCVerticalBottom];
    
    //表示位置の選択も一度のみ（この情報はappDelegateに保存しておく）
    appDelegate.simpleAdView = simpleView;
    //広告Viewは最後に追加する
    [self.view addSubview:appDelegate.simpleAdView];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    //広告関係(Splash広告)
    ////////////////////
    
#if AD_DISABLE
    DLog(@"広告非表示");
#else
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    /*
    //表示位置の選択
    appCSimpleView *simpleView = [[appCSimpleView alloc]initWithViewController:self vertical:appCVerticalBottom];
    
    //表示位置の選択も一度のみ（この情報はappDelegateに保存しておく）
    appDelegate.simpleAdView = simpleView;
    //広告Viewは最後に追加する
    [self.view addSubview:appDelegate.simpleAdView];
    */
    
    //「プレイ中に戻るボタン」もしくはゲームオーバ画面で「メニューボタン」を
    //押した際に入ります（５回に１回）
        if(appDelegate.splashAdFlg == YES){
            
            //splashカウントのインクリメント
            appDelegate.splashCnt++;
            DLog(@"スプラッシュカウント -> %d",appDelegate.splashCnt);
            DLog(@"splashCnt[%d] > splash_num_cnt[%d]",appDelegate.splashCnt,splash_num_cnt);
            
            if(appDelegate.splashCnt > splash_num_cnt){
        
                __weak __block MainViewController *blockSelf = self;
                // splash
                self.splashAdView = [CSSSplashAdView splashAdViewWithSources:(CSSSplashAdSourceAppC) viewController:self dismissCompletion:^{
                }];
                //広告表示(スプラッシュ)
                [blockSelf.splashAdView showSplashAdWithAdSource:CSSSplashAdSourceAppC];
                //フラグを初期値に戻す
                appDelegate.splashAdFlg = NO;
                appDelegate.splashCnt = 0;
            }
        }
#endif
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    DLog(@"広告表示停止");
    //メニュー画面が閉じたら、広告ストップ
    [self.bannerAdView admobMediationRequestStop];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - ButtonAction

// MARK: RankingPlayButton

- (IBAction)rankplayBtnAct:(id)sender {
    
    if(touchFreePlayFlg== YES){
        
        //連続押防止フラグをたてます
        touchFreePlayFlg = NO;
        
    //ランキングモードに入ります
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    appDelegate.modeFlg = 0;
    
    //選択音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSounds = [soundsUtils selectMenuSounds];
    [selectSounds play];
    
    [self performSelector:@selector(delayAnimated)
               withObject:nil afterDelay:0.3];
    
    _rankPlayLabel.textColor = [UIColor colorWithRed:rankplayLabelR green:rankplayLabelG blue:rankplayLabelB alpha:rankplayLabelAlpha];
    }
}

- (IBAction)rankplayBtndownAct:(id)sender {
    
    _rankPlayLabel.textColor = [UIColor whiteColor];
    
}

- (IBAction)rankplayBtnoutSideAct:(id)sender {
    
    _rankPlayLabel.textColor = [UIColor colorWithRed:rankplayLabelR green:rankplayLabelG blue:rankplayLabelB alpha:rankplayLabelAlpha];
}

// MARK: ChallengePlay

- (IBAction)challengeBtnAct:(id)sender {
    
    if(touchChallengePlayFlg == YES){
        
        //連続押し防止のためタッチフラグをたてます。
        touchChallengePlayFlg = NO;
        
        //ランキングモードとフリーモードの判別
        AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
        appDelegate.modeFlg = 2;
        
        //選択音
        SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
        selectSounds = [soundsUtils selectMenuSounds];
        [selectSounds play];
        
        [self performSelector:@selector(delayAnimated)
                   withObject:nil afterDelay:0.3];
        
         _challengePlayLabel.textColor = [UIColor colorWithRed:challplayLabelR green:challrankplayLabelG blue:challrankplayLabelB alpha:challrankplayLabelAlpha];
    }

}

- (IBAction)challengeBtndownAct:(id)sender {
    _challengePlayLabel.textColor = [UIColor whiteColor];
}

- (IBAction)challengeBtnoutSideAct:(id)sender {
     _challengePlayLabel.textColor = [UIColor colorWithRed:challplayLabelR green:challrankplayLabelG blue:challrankplayLabelB alpha:challrankplayLabelAlpha];
}

- (IBAction)challengeBtndownAct35:(id)sender {
     _challengePlayLabel.textColor = [UIColor whiteColor];
}

- (IBAction)challengeBtnoutSideAct35:(id)sender {
    _challengePlayLabel.textColor = [UIColor colorWithRed:challplayLabelR green:challrankplayLabelG blue:challrankplayLabelB alpha:challrankplayLabelAlpha];
}


// MARK: Ranking

- (IBAction)rankingBtnAct:(id)sender {
    
    //選択音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSounds = [soundsUtils selectMenuSounds];
    [selectSounds play];
    
    _rankingLabel.textColor = [UIColor colorWithRed:rankingLabelR green:rankingLabelG blue:rankingLabelB alpha:rankingLabelAlpha];
    
    //Viewのインスタンス
    RankingViewController *rankingView = [self.storyboard instantiateViewControllerWithIdentifier:@"RankingViewController"];
    //遷移方法を設定
    rankingView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:rankingView animated:YES completion:nil];
    
}

- (IBAction)rankingBtndownAct:(id)sender {
    _rankingLabel.textColor = [UIColor whiteColor];

}

- (IBAction)rankingBtnoutSideAct:(id)sender {
    _rankingLabel.textColor = [UIColor colorWithRed:rankingLabelR green:rankingLabelG blue:rankingLabelB alpha:rankingLabelAlpha];
}

- (IBAction)rankingBtndownAct4:(id)sender {
    _rankingLabel.textColor = [UIColor whiteColor];
}

- (IBAction)rankingBtnoutSideAct4:(id)sender {
    _rankingLabel.textColor = [UIColor colorWithRed:rankingLabelR green:rankingLabelG blue:rankingLabelB alpha:rankingLabelAlpha];
}


// MARK: playButton

- (IBAction)playBtnAct:(id)sender {
    
    if(touchPlayFlg == YES){
        
        //連続押し防止のためタッチフラグをたてます。
        touchPlayFlg = NO;
    
        //ランキングモードとフリーモードの判別
        AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
        appDelegate.modeFlg = 1;
    
        //選択音
        SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
        selectSounds = [soundsUtils selectMenuSounds];
        [selectSounds play];
    
        [self performSelector:@selector(delayAnimated)
                   withObject:nil afterDelay:0.3];
    
        _playLabel.textColor = [UIColor colorWithRed:playLabelR green:playLabelG blue:playLabelB alpha:playLabelAlpha];
        
    }
    
}

//******************************
//playボタンを押したとき
//playラベルを白に(押した感じを出す)
//******************************
- (IBAction)playBtndownAct:(id)sender {
    
     _playLabel.textColor = [UIColor whiteColor];
    
}

//********************************
//playラベルの境界外に出てしまった場合
//********************************
- (IBAction)playBtnoutSideAct:(id)sender {
    
    _playLabel.textColor = [UIColor colorWithRed:playLabelR green:playLabelG blue:playLabelB alpha:playLabelAlpha];
}


//************************
//音がかぶるので少し遅らせます
//***********************
-(void)delayAnimated{
    
    //playViewのインスタンス
    ViewController *playViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayViewController"];
    
    //フェードアウトしながらアニメーション遷移
    [self pushViewControllerAnimted:playViewController];
    
    //タッチを許可します。
    touchPlayFlg = YES;
    touchFreePlayFlg = YES;
    touchChallengePlayFlg = YES;
}


// MARK: optionButton

- (IBAction)optionBtnAct:(id)sender {
       
    //選択音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSounds = [soundsUtils selectMenuSounds];
    [selectSounds play];
    
    //viewのインスタンス
    SettingViewController *settingView = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    //遷移方法を設定
    settingView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //画面遷移実行
    [self presentViewController:settingView animated:YES completion:nil];
    
    _optionLabel.textColor = [UIColor colorWithRed:optionLabelR green:optionLabelG blue:optionLabelB alpha:optionLabelAlpha];

}

//******************************
//optionボタンを押したとき
//optionラベルを白に(押した感じを出す)
//******************************
- (IBAction)optionBtndownAct:(id)sender {
    
    _optionLabel.textColor = [UIColor whiteColor];
}

//********************************
//optionラベルの境界外に出てしまった場合
//********************************
- (IBAction)optionBtnoutSideAct:(id)sender {
    
    _optionLabel.textColor = [UIColor colorWithRed:optionLabelR green:optionLabelG blue:optionLabelB alpha:optionLabelAlpha];
}


#pragma mark - social

//********************************
//ツイッター投稿ボタン
//********************************
- (IBAction)twitterBtnMenuAct:(id)sender {
    
    [DCSocial postToTwitter:self text:POST_TEXT imageName:POST_IMG_NAME url:POST_URL];
    
}

//********************************
//フェイスブック投稿ボタン
//********************************
- (IBAction)FacebookBtnMenuAct:(id)sender {
    
    [DCSocial postToFacebook:self text:POST_TEXT imageName:POST_IMG_NAME url:POST_URL];
    
}


#pragma mark - PushAnimated

//*****************************
//フェードアウトしながら画面遷移する
//*****************************
-(void)pushViewControllerAnimted:(UIViewController *)pushView{
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.8;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:pushView animated:NO];
    
    _playLabel.textColor = [UIColor colorWithRed:playLabelR green:playLabelG blue:playLabelB alpha:playLabelAlpha];
    _rankPlayLabel.textColor = [UIColor colorWithRed:rankplayLabelR green:rankplayLabelG blue:rankplayLabelB alpha:rankplayLabelAlpha];
     _challengePlayLabel.textColor = [UIColor colorWithRed:challplayLabelR green:challrankplayLabelG blue:challrankplayLabelB alpha:challrankplayLabelAlpha];
}

#pragma mark - AppVersion

//**************************
//アプリバージョンの管理
//**************************
-(void)appVersion{
    
    //本アプリのバージョンを管理
    //plistからversion情報を持ってくる
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    _appVersionLabel.text = version;
    
}


#pragma mark - GameCenter

//********************************
//GameCenterに自動ログイン
//********************************
- (void)authenticateLocalPlayer
{
    GKLocalPlayer* player = [GKLocalPlayer localPlayer];
    player.authenticateHandler = ^(UIViewController* ui, NSError* error )
    {
        if( nil != ui )
        {
            [self presentViewController:ui animated:YES completion:nil];
        }
        
    };
}


#pragma mark - Tutorial

//**************************
//チュートリアル遷移画面
//**************************
- (IBAction)tutorialButton:(id)sender {
    
    //選択音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSounds = [soundsUtils selectMenuSounds];
    [selectSounds play];
    
    //playViewのインスタンス
    ViewController *playViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialMainViewController"];
    
    //フェードアウトしながらアニメーション遷移
    [self pushViewControllerAnimted:playViewController];
    
}
@end
