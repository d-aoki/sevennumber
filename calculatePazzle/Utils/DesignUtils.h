//
//  -メモ-
//  主にUI系、デザインの設定を行うクラスです
//
//  DesignUtils.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/06.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DesignUtils : NSObject

//背景画像設定メソッド
-(void)backImageSettingAct:(UIView *)settingView;
//背景画像設定メソッド(カメラロール)
-(void)backImageCameraSettingAct:(UIView *)settingView image:(NSData *)data;
//グラデーション色設定
-(void)gradation:(UIView *)settingView;

@end
