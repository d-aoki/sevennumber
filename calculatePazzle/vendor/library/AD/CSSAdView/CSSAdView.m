//
//  CSSAdView.m
//  ScheduleConcierge
//
//  Created by Natsuki Funaki on 2013/07/02.
//  Copyright (c) 2013年 Takashi Yasukawa. All rights reserved.
//

#import "CSSAdView.h"

@implementation CSSAdView
{
    BOOL _admobRequested;
    
    UIView *_staticContentView;
    UIWebView *_staticContentWebView;
    UIToolbar *_staticFooter;
    
    UIActivityIndicatorView *_activityIndicatorView;
}

+ (id)adViewForAdmobMediationWithAdSize:(CGSize)size
                     rootViewController:(id)viewController
                           mediationID:(NSString*)mediationID
                               delegate:(id<GADBannerViewDelegate>)delegate {
    CSSAdView *adView = [[CSSAdView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    adView.contentMode = UIViewContentModeBottom;
    adView.rootViewController = viewController;
    adView.clipsToBounds = NO;
    
    // admob
    adView.gadBannerView = [[GADBannerView alloc] initWithAdSize:GADAdSizeFromCGSize(size) origin:CGPointMake(0, 0)];
    adView.gadBannerView.contentMode = UIViewContentModeBottom;
    adView.gadBannerView.clipsToBounds = NO;
    
    [adView addSubview:adView.gadBannerView];
    
    adView.gadBannerView.rootViewController = viewController;
    adView.gadBannerView.adUnitID = mediationID;

    if (delegate != nil) {
        adView.gadBannerView.delegate = delegate;
    } else {
        adView.gadBannerView.delegate = adView;
    }

    adView.gadRequest = [GADRequest request];
    
    //DLog(@"gadBannerView view %@", adView.gadBannerView);
    
    return adView;
}

+ (id)adViewForCSSStaticBannerWithType:(CSSStaticBannerType)type rootViewController:(UIViewController*)viewController {
    CSSAdView *adView = [[CSSAdView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    adView.rootViewController = viewController;

    UIImageView *bannerView = [[UIImageView alloc] initWithFrame:adView.bounds];
    bannerView.contentMode = UIViewContentModeCenter;
    bannerView.userInteractionEnabled = YES;
    
    adView.staticBannerType = type;
    switch (adView.staticBannerType) {
        case CSSStaticBanner30plusminus:
            [bannerView setImage:[UIImage imageNamed:@"30pmad"]];
            
            break;
            
        default:
            break;
    }
    [adView addSubview:bannerView];
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:adView action:@selector(staticBannerTapped:)];
    [adView addGestureRecognizer:tapGR];
    
    CGRect lblFrame = CGRectMake(0, 0, adView.bounds.size.width, 20);
    UILabel *adLabel = [[UILabel alloc] initWithFrame:CGRectInset(lblFrame, 6, 0)];
    adLabel.textAlignment = NSTextAlignmentRight;
    adLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0];
    adLabel.backgroundColor = [UIColor clearColor];
    adLabel.font = [UIFont italicSystemFontOfSize:8];
    adLabel.text = @"CarolSystemSendai AD";
    [adView addSubview:adLabel];
    
    return adView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        ///////////////////////////////////////////////////
        // 広告関係
        // iADのiPad版　  768 x 66
        // admobのiPad版  728 × 90
        //
        // iPhone版　320 × 50 
        ///////////////////////////////////////////////////
        self.backgroundColor = [UIColor clearColor];
        self.contentMode = UIViewContentModeCenter;
        self.hidden = NO;
        self.userInteractionEnabled = YES;
        
        DLog(@"adview %@", self);
    }
    return self;
}

#pragma mark - static
- (NSString*)serviceURLWithCSSStaticBannerType:(CSSStaticBannerType)type {
    NSString *url = nil;
    switch (type) {
        case CSSStaticBanner30plusminus:
            url = @"http://www.30plusminus.com";
            break;
            
        default:
            break;
    }
    
    return url;
}

- (void)staticBannerTapped:(UITapGestureRecognizer*)tapGestureRecoginzer {
    if (tapGestureRecoginzer.state == UIGestureRecognizerStateEnded) {
        NSString *urlString = [self serviceURLWithCSSStaticBannerType:self.staticBannerType];
        NSURL *url = [NSURL URLWithString:urlString];
        
        // webviewで表示
        UIView *contentView = [[UIView alloc] initWithFrame:self.rootViewController.view.bounds];
        contentView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
        [self.rootViewController.view addSubview:contentView];
        contentView.transform = CGAffineTransformMakeTranslation(0, contentView.bounds.size.height);
        
        // フッタのツールバー
        UIToolbar *footer = [[UIToolbar alloc] initWithFrame:CGRectOffset(CGRectMake(0, 0, contentView.bounds.size.width, 44), 0, contentView.bounds.size.height - 44)];
        footer.barStyle = UIBarStyleDefault;
        [contentView addSubview:footer];
        // フッタのツールボタン
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(webViewBack:)];
        backButton.enabled = NO;
        UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@">" style:UIBarButtonItemStylePlain target:self action:@selector(webViewNext:)];
        UIBarButtonItem *space1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        nextButton.enabled = NO;
        space1.width = 32;
        footer.items = @[backButton, space1, nextButton];


        // 閉じるボタン
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectInset(CGRectMake(contentView.bounds.size.width - 44, contentView.bounds.size.height - 44, 44, 44), 7, 7);
        closeButton.contentMode = UIViewContentModeCenter;
        [closeButton setImage:[UIImage imageNamed:@"css_banner_closebutton"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(staticBannerCloseTapped:) forControlEvents:UIControlEventTouchUpInside];
        closeButton.clipsToBounds = YES;
        closeButton.layer.cornerRadius = 30/2.0f;
        closeButton.layer.backgroundColor = [UIColor whiteColor].CGColor;
        [contentView addSubview:closeButton];
        
        if (_activityIndicatorView != nil) {
            [_activityIndicatorView stopAnimating];
            [_activityIndicatorView removeFromSuperview];
            _activityIndicatorView = nil;
        }
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicatorView.center = contentView.center;
        _activityIndicatorView.hidesWhenStopped = YES;
        [contentView addSubview:_activityIndicatorView];
        [_activityIndicatorView startAnimating];
        
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, contentView.bounds.size.width, contentView.bounds.size.height-44)];
        webView.delegate = self;
        [contentView insertSubview:webView atIndex:0];
        [webView loadRequest:[NSURLRequest requestWithURL:url]];
        
        // 表示
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            contentView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            ;
        }];
        
        _staticContentView = contentView;
        _staticContentWebView = webView;
        _staticFooter = footer;
    }
}


#pragma mark - admob
- (void)setAdmobMediationID:(NSString*)mediationID {
    self.gadBannerView.adUnitID = mediationID;
}

- (void)setAdmobBannerDelegate:(id<GADBannerViewDelegate>)delegate {
    self.gadBannerView.delegate = delegate;
}


- (void)admobEnableTestModeWithTestDevices:(NSArray*)testDevices {
    self.gadRequest.testing = YES;
    
    NSMutableArray *devices = [[NSMutableArray alloc] init];
    [devices addObject:GAD_SIMULATOR_ID];
    for (NSString *testDeviceId in testDevices) {
        [devices addObject:testDeviceId];
    }
    self.gadRequest.testDevices = devices;
}

- (void)admobMediationRequestStart {
    NSAssert(self.gadBannerView.adUnitID != nil, @"admobのメディエーションキーが設定されていません！");
    if (_admobRequested) {
        self.gadBannerView.hidden = NO;
        self.hidden = NO;
    } else {
        [self.gadBannerView loadRequest:self.gadRequest];
        _admobRequested = YES;
    }

    [self moveAdViewTheEntireWithView:self.gadBannerView];
}

- (void)admobMediationRequestStop {
    self.gadBannerView.hidden = YES;
    self.hidden = YES;
    
    self.transform = CGAffineTransformIdentity;
}

#pragma mark - delegate(default)
- (void)adViewDidReceiveAd:(GADBannerView *)view {
    DLog(@"広告表示成功 %@", view.mediatedAdView);
    [self moveAdViewTheEntireWithView:view];
    view.hidden = NO;
}

-(void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    DLog(@"広告表示失敗 %@ error:%@", view.mediatedAdView, error);
    self.transform = CGAffineTransformIdentity;
    view.hidden = YES;
}

- (void)moveAdViewTheEntireWithView:(GADBannerView*)view {
    self.transform = CGAffineTransformIdentity;
    if (self.bounds.size.height < view.mediatedAdView.bounds.size.height) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.transform = CGAffineTransformMakeTranslation(0, self.bounds.size.height - view.mediatedAdView.bounds.size.height);
        });
    }
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    DLog(@"request %@", request);
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    DLog(@"request %@", webView.request);
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    DLog(@"request %@", webView.request);

    UIBarButtonItem *backButton = [_staticFooter.items objectAtIndex:0];
    UIBarButtonItem *nextButton = [_staticFooter.items objectAtIndex:2];
    NSAssert(backButton != nil, @"footerの設定がおかしい。");
    NSAssert(nextButton != nil, @"footerの設定がおかしい。");
    if ([webView canGoForward]) {
        nextButton.enabled = YES;
    }
    if ([webView canGoBack]) {
        backButton.enabled = YES;
    }
    
    [_activityIndicatorView stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    ALog(@"request %@ ERROR! %@", webView.request, error);
    [_activityIndicatorView stopAnimating];
}

#pragma mark - static content button
- (void)staticBannerCloseTapped:(UIButton*)sender {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _staticContentView.transform = CGAffineTransformMakeTranslation(0, _staticContentView.bounds.size.height);
    } completion:^(BOOL finished) {
        [_staticContentView removeFromSuperview];
        _staticContentView = nil;
    }];
}

- (void)webViewBack:(UIBarButtonItem*)sender {
    if (_staticContentWebView.canGoBack) {
        [_staticContentWebView goBack];
    }
}
- (void)webViewNext:(UIBarButtonItem*)sender {
    if (_staticContentWebView.canGoForward) {
        [_staticContentWebView goForward];
    }
}

@end
