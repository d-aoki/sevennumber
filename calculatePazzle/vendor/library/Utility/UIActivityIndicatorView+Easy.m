//
//  UIActivityIndicatorView+Easy.m
//  GrowBookkeeping
//
//  Created by Natsuki Funaki on 2013/04/10.
//  Copyright (c) 2013年 CarolSystem Sendai. All rights reserved.
//

#define COMMON_ACTIVE_INDICATOR_VIEW_TAG INT_MAX

#import "UIActivityIndicatorView+Easy.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIActivityIndicatorView (Easy)

#pragma mark - Activity Indicator
/**
 * インジケータを指定したviewの中心に表示し、アニメーションを開始します。
 */
+ (void)showActivityIndicator:(UIView*)onView
                        style:(UIActivityIndicatorViewStyle)style
                      message:(NSString*)message {
    
    [self hideActivityIndicator:onView];
    
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                144, 100)];
    [grayView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
    grayView.center = CGPointMake(onView.bounds.size.width/2, onView.bounds.size.height/2);
    [grayView.layer setCornerRadius:10.0f];
    grayView.tag = COMMON_ACTIVE_INDICATOR_VIEW_TAG;
    [onView addSubview:grayView];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 72, 144, 28)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.text = message;
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont systemFontOfSize:10];
    lbl.minimumScaleFactor = 6;
    [grayView addSubview:lbl];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    activityIndicator.center = CGPointMake(grayView.bounds.size.width/2, grayView.bounds.size.height/2);
    [grayView addSubview:activityIndicator];
    
    [activityIndicator performSelector:@selector(startAnimating) withObject:nil afterDelay:0.01];
    
}

/**
 * インジケータを指定したviewの指定した座標に表示し、アニメーションを開始します。
 */
+ (void)showActivityIndicator:(UIView*)onView
                       center:(CGPoint)centerPoint
                        style:(UIActivityIndicatorViewStyle)style
                      message:(NSString*)message {
    
    [self hideActivityIndicator:onView];
    
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                144, 100)];
    [grayView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
    grayView.center = centerPoint;
    [grayView.layer setCornerRadius:10.0f];
    grayView.tag = COMMON_ACTIVE_INDICATOR_VIEW_TAG;
    [onView addSubview:grayView];
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 72, 144, 28)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.text = message;
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont systemFontOfSize:10];
    lbl.minimumScaleFactor = 6;
    [grayView addSubview:lbl];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    activityIndicator.center = CGPointMake(grayView.bounds.size.width/2, grayView.bounds.size.height/2);
    [grayView addSubview:activityIndicator];
    
    [activityIndicator performSelector:@selector(startAnimating) withObject:nil afterDelay:0.01];
}

/**
 * インジケータを停止し、削除します。
 */
+ (void)hideActivityIndicator:(UIView*)parentView {
    UIView *commonAcvView = [parentView viewWithTag:COMMON_ACTIVE_INDICATOR_VIEW_TAG];
    for (UIView* v in [commonAcvView.subviews reverseObjectEnumerator]) {
        if ([v isKindOfClass:[UIActivityIndicatorView class]]) {
            UIActivityIndicatorView *acv = (UIActivityIndicatorView*)v;
            [acv stopAnimating];
        }
        [v removeFromSuperview];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [commonAcvView removeFromSuperview];
    });
}

@end
