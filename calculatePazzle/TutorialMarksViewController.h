//
//  TutorialMarksViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/24.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialMarksViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *plusMsg;

@property (weak, nonatomic) IBOutlet UILabel *minusMsg;

@property (weak, nonatomic) IBOutlet UILabel *parMsg;

@property (weak, nonatomic) IBOutlet UILabel *bonusMsg;
@property (weak, nonatomic) IBOutlet UILabel *marksMsg;

//各ボタンの設定
- (IBAction)backMenuAct:(id)sender;
- (IBAction)ModeBtnAct:(id)sender;

@end
