//
//  RankingViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/13.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "RankingViewController.h"
#import "DesignUtils.h"
#import "ProcessingViewUtils.h"
#import "DCSocial.h"
#import "AppDelegate.h"
#import "SoundsUtils.h"

#import <GameKit/GameKit.h>


@interface RankingViewController ()

@end

@implementation RankingViewController

{
    NSArray *labelArray;
    
    NSString *socialMsg;
    
    //タイムアタックランキング、チャレンジランキング格納変数
    NSArray *rankingArray,*cngrankingArray;
    
    //効果音用
    AVAudioPlayer *backAudio,*selectSound;
    
    NSUserDefaults *cHighScore,*tHighScore;
    int timeAttackScore,challengeScore;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    // [banner <appC cloud>]
    [self.view addSubview:appDelegate.simpleAdView];
    
    //表示するランキング情報を返します
    [self ranking];
    
    //文字フォントの設定
    
    //このクラスでは起動時の最初の一回ののみ呼ばれます
    //デフォルト背景
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    //[designUtils gradation:self.view];
    [designUtils backImageSettingAct:self.view];
    
    
    //フレームの定義(RankingView)
    [_rankingView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_rankingView.layer setBorderWidth:2.0];
    _rankingView.layer.cornerRadius = 20;
    _rankingView.alpha = 0.85f;
    
    
    //フォント変更をするViewを配列に代入します
    labelArray = [[NSArray alloc]initWithObjects:_modeStrLabel, nil];
    //フォント変更
    ProcessingViewUtils *proUtills = [[ProcessingViewUtils alloc]init];
    [proUtills headTitleLabelshadow:labelArray];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - social

- (IBAction)twitterBtnAct:(id)sender {
    
    NSString *msg = [self socialMsg];
    
    [DCSocial postToTwitter:self text:msg imageName:POST_IMG_NAME url:POST_URL];
}

- (IBAction)facebookBtnAct:(id)sender {
   
    NSString *msg = [self socialMsg];
    
    [DCSocial postToFacebook:self text:msg imageName:POST_IMG_NAME url:POST_URL];
}

-(NSString *)socialMsg{
   
    //メッセージ格納変数
    NSString *msg;
    
    if(_segment.selectedSegmentIndex==0){
        //タイムアタックモード
        tHighScore = [NSUserDefaults standardUserDefaults];
        timeAttackScore = [tHighScore integerForKey:@"KEY_tAppScore"];
        
        msg = [NSString stringWithFormat:@"脳トレカジュアルゲーム『DoReMi Seven』のTimeAttackModeで%d点の記録を持っています。",timeAttackScore];
        
    }else if (_segment.selectedSegmentIndex==1){
        //チャレンジモード
        cHighScore = [NSUserDefaults standardUserDefaults];
        challengeScore = [cHighScore integerForKey:@"KEY_cAppScore"];
        
        msg = [NSString stringWithFormat:@"脳トレカジュアルゲーム『DoReMi Seven』のChallengeModeで%d点の記録を持っています",challengeScore];
    }
    
    DLog(@"ソーシャルメッセージです -> %@",msg);
    
    return msg;
}

#pragma mark - ButtonAction

- (IBAction)backBtn:(id)sender {
    
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    //戻る音
    backAudio = [soundsUtils backMenuSounds];
    [backAudio play];
    //一つ前の画面に戻る
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - ranking

//***********************
//ランキングの内容を返します
//***********************
-(void)ranking{
    
    //タイムアタックランキングの内容を配列に格納します
    NSUserDefaults *_rankArrayUD = [NSUserDefaults standardUserDefaults];
    rankingArray = [_rankArrayUD objectForKey:SCORE_KEY];
    
    DLog(@"%@",rankingArray);
    
    //チャレンジモードランキングの内容を配列に格納します
    NSUserDefaults *_cngrankArrayUD = [NSUserDefaults standardUserDefaults];
    cngrankingArray = [_cngrankArrayUD objectForKey:CNG_SCORE_KEY];
    
    DLog(@"%@",cngrankingArray);
}


#pragma mark - TableViewDelegate

//*******************
//Section数
//*******************
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


//********************
//Row（行数）
//********************
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [rankingArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    //配列の内容を表示します
    
    //タイムアタックランキング
    if(tableView.tag == 0){
    
        NSString *rank = [rankingArray objectAtIndex:indexPath.row];
        cell.textLabel.text = rank;
        cell.textLabel.font = [UIFont fontWithName:@"Chalkboard SE" size:18];
        
    }else{
        NSString *rank = [cngrankingArray objectAtIndex:indexPath.row];
        cell.textLabel.text = rank;
        cell.textLabel.font = [UIFont fontWithName:@"Chalkboard SE" size:18];
    }
    
    return cell;
}



#pragma mark - GameCenter

//***************************
//リーダーボードの表示
//***************************
- (IBAction)gameCBtnAct:(id)sender {

    GKGameCenterViewController *gcView = [GKGameCenterViewController new];
    
    if (gcView != nil)
    {
        gcView.gameCenterDelegate = self;
        gcView.viewState = GKGameCenterViewControllerStateLeaderboards;
        //gcView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:gcView animated:YES completion:nil];
    }
    
}

//*******************************
//リーダーボードで完了タップ時の処理
//前の画面に戻る
//*******************************
- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Segment

- (IBAction)rankCngSegmentAct:(id)sender {
    
    //選択音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSound = [soundsUtils effectSounds:@"click"];
    [selectSound play];
    
    switch (_segment.selectedSegmentIndex) {
        case 0:
            DLog(@"time Attack");
            _timeAttackView.alpha = 1.0f;
            _challengeView.alpha = 0.0f;
            _modeStrLabel.text = @"TimeAttack Mode";
            
            break;
        case 1:
            DLog(@"challenge");
            _timeAttackView.alpha = 0.0f;
            _challengeView.alpha = 1.0f;
            _modeStrLabel.text = @"Challenge Mode";
            break;
        default:
            break;
    }
    
}
@end
