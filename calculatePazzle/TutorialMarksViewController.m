//
//  TutorialMarksViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/24.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "TutorialMarksViewController.h"
#import "DesignUtils.h"
#import "ProcessingViewUtils.h"
#import "SoundsUtils.h"

@interface TutorialMarksViewController ()

@end

@implementation TutorialMarksViewController
{
    CGPoint _movePageTouch;
    
    AVAudioPlayer *backAudio,*selectSound;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //メッセージの加工
    NSArray *array = [[NSArray alloc]initWithObjects:_plusMsg,_minusMsg,_parMsg,_bonusMsg,_marksMsg, nil];
     NSArray *marksArray = [[NSArray alloc]initWithObjects:_plusMsg,_minusMsg,_parMsg, nil];
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadow:array];
    [proUtils headTitleLabelshadowBlack:marksArray];
    
    //デフォルト背景
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    //[designUtils gradation:self.view];
    [designUtils backImageSettingAct:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Touch Event

//********************************************
//画面がタッチされたら、次のページに遷移する用にします
//********************************************
-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    _movePageTouch = [touch locationInView:self.view];
    
    
    
}

//***********************************
//ここでメニューに戻るか、モードの説明画面
//に移るかの分岐
//***********************************

- (IBAction)backMenuAct:(id)sender {
    
    //選択音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSound = [soundsUtils effectSounds:@"click"];
    [selectSound play];
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (IBAction)ModeBtnAct:(id)sender {
    
    //選択音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSound = [soundsUtils effectSounds:@"click"];
    [selectSound play];
    
    //playViewのインスタンス
    UIViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialTimeAttackViewController"];
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:view animated:NO];

}


@end
