//
//  TutorialFreeViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/25.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "TutorialFreeViewController.h"
#import "ProcessingViewUtils.h"
#import "DesignUtils.h"
#import "SoundsUtils.h"

//freeplayボタンのRGB値
float const playLabelR = 1.0;
float const playLabelG = 0.753;
float const playLabelB = 0.796;
float const playLabelAlpha = 1.0;

//optionボタンのRGB値
float const optionLabelR = 1.0;
float const optionLabelG = 0.98;
float const optionLabelB = 0.803;
float const optionLabelAlpha = 1.0;

@interface TutorialFreeViewController ()

@end

@implementation TutorialFreeViewController

{
    CGPoint _movePageTouch;
    AVAudioPlayer *backAudio;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //デフォルト背景
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    [designUtils backImageSettingAct:self.view];
    
    //タイトルラベルのRGB値の設定
    _freeStr.textColor = [UIColor colorWithRed:playLabelR green:playLabelG blue:playLabelB alpha:playLabelAlpha];
    _optionStr.textColor = [UIColor colorWithRed:optionLabelR green:optionLabelG blue:optionLabelB alpha:optionLabelAlpha];
    
    //タイトルラベルのデコレート
    NSArray *titleArray = [[NSArray alloc]initWithObjects:_freeStr,_optionStr, nil];
    NSArray *optionArray = [[NSArray alloc]initWithObjects:_operatorTitle,_lifepointTitle,_maskTitle,_timelimitTitle,nil];
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadow:titleArray];
    [proUtils headTitleLabelshadowBlack:optionArray];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Touch Event

//********************************************
//画面がタッチされたら、トップ画面に戻るようにします
//********************************************
-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    //戻る音
    backAudio = [soundsUtils backMenuSounds];
    [backAudio play];
    
    UITouch *touch = [touches anyObject];
    _movePageTouch = [touch locationInView:self.view];
    
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
}


@end
