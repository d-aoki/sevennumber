//
//  SoundsUtils.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/06.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <AVFoundation/AVFoundation.h>


@interface SoundsUtils : NSObject

//BGMの再生(ランキング・フリー)
-(AVAudioPlayer *)RankingbgmSoundsSetting:(NSString *)soundName;
-(AVAudioPlayer *)bgmSoundsSetting;
//ブロックをタッチした際の効果音判別メソッド
-(AVAudioPlayer *)tapBlocksSound:(NSString *)soundName;
//「７」ブロックを作った際の効果音判別メソッド
-(AVAudioPlayer *)createSevenBlocksSound:(UIView *)sevenBlock;
//メニュー選択音
-(AVAudioPlayer *)selectMenuSounds;
//戻るボタン選択
-(AVAudioPlayer *)backMenuSounds;
//その他効果音など
-(AVAudioPlayer *)effectSounds:(NSString *)sound;

//BGMをフェードアウトしながらストップ
-(void)doVolumeFade:(AVAudioPlayer *)audio;

@end
