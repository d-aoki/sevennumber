//
//  TutorialFreeViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/25.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialFreeViewController : UIViewController

//プレイタイトルラベル
@property (weak, nonatomic) IBOutlet UILabel *freeStr;

@property (weak, nonatomic) IBOutlet UILabel *optionStr;

//オプション機能タイトル
@property (weak, nonatomic) IBOutlet UILabel *operatorTitle;
@property (weak, nonatomic) IBOutlet UILabel *lifepointTitle;
@property (weak, nonatomic) IBOutlet UILabel *timelimitTitle;
@property (weak, nonatomic) IBOutlet UILabel *maskTitle;


@end
