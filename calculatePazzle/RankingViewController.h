//
//  RankingViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/13.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>

@interface RankingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,GKGameCenterControllerDelegate>

//メニュー画面に戻るボタン
- (IBAction)backBtn:(id)sender;

//ランキングが表示されるView
@property (weak, nonatomic) IBOutlet UIView *rankingView;

//ランキングテーブル切り替えセグメント
- (IBAction)rankCngSegmentAct:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;

@property (weak, nonatomic) IBOutlet UITableView *timeAttackView;
@property (weak, nonatomic) IBOutlet UITableView *challengeView;

//ランキングモードラベル
@property (weak, nonatomic) IBOutlet UILabel *modeStrLabel;



//ソーシャル系
- (IBAction)twitterBtnAct:(id)sender;
- (IBAction)facebookBtnAct:(id)sender;

//ゲームセンター
- (IBAction)gameCBtnAct:(id)sender;

@end
