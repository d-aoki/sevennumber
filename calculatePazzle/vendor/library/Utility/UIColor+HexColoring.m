//
//  UIColor+HexColoring.m
//  GrowBookkeeping
//
//  Created by Natsuki Funaki on 2013/04/01.
//  Copyright (c) 2013年 CarolSystem Sendai. All rights reserved.
//

#import "UIColor+HexColoring.h"

@implementation UIColor (HexColoring)

//RGBAを16進数型(0xffa500ffなど)で指定します
+ (UIColor*)colorWithHex:(uint32_t)hex
{
    CGFloat red = ((hex & 0xFF000000) >> 24) / 255.0f;
    CGFloat green = ((hex & 0x00FF0000) >> 16) / 255.0f;
    CGFloat blue = ((hex & 0x0000FF00) >> 8) / 255.0f;
    CGFloat alpha = (hex & 0x0000000FF) / 255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}
+ (UIColor*)colorWithHexString:(NSString*)string
{
    uint32_t hex = 0x0;
    NSScanner* scanner = [NSScanner scannerWithString:string];
    [scanner scanHexInt:&hex];
    
    return [UIColor colorWithHex:hex];
}

@end
