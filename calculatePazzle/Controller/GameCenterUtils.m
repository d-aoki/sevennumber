//
//  GameCenterUtils.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/17.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "GameCenterUtils.h"

@implementation GameCenterUtils


#pragma mark - GameCenter

//********************************
//GameCenterに自動ログイン
//********************************
- (void)authenticateLocalPlayer
{
    
    //gameCenterのプレイヤーの取得
    GKLocalPlayer* player = [GKLocalPlayer localPlayer];
    player.authenticateHandler = ^(UIViewController* ui ,NSError* error)
    {
        if(nil != ui){
            [self presentViewController:ui animated:YES completion:nil];
        }
    };
    
}




@end
