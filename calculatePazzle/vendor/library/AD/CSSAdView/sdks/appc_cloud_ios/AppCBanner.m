//
//  AppCBanner.m
//  CSSRadio
//
//  Created by Natsuki Funaki on 2013/11/26.
//  Copyright (c) 2013年 CarolSystemSendai Co.,Ltd. All rights reserved.
//

#import "AppCBanner.h"
#import "appCCloud.h"

@implementation AppCBanner

#pragma mark - GAD
- (void)requestBannerAd:(GADAdSize)adSize
              parameter:(NSString *)serverParameter
                  label:(NSString *)serverLabel
                request:(GADCustomEventRequest *)request {
    
    // パラメータでメディアキーが渡ってくる
    DLog(@"serverParameter:%@, serverLabel:%@, adSize:%@", serverParameter, serverLabel, NSStringFromCGSize(adSize.size));
    id viewController = self.delegate.viewControllerForPresentingModalView;
    if (viewController != nil) {
        NSString *mediaKey = [NSString stringWithString:serverParameter];
        
        [appCCloud setupAppCWithMediaKey:mediaKey option:APPC_CLOUD_AD];
        
        appCSimpleView* view = [[appCSimpleView alloc] initWithViewController:viewController vertical:appCVerticalTop];
        DLog(@"appc view %@", view);
        // 表示
        [self.delegate customEventBanner:self didReceiveAd:view];
    
    } else {
        ALog(@"appCの広告を表示する先のViewControllerが無い様です。");
    }
}

@end
