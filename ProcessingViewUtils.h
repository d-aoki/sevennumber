//
//  ProcessingViewUtils.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/11.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProcessingViewUtils : NSObject

-(void)labelshadow:(NSArray *)labelArray;
-(void)titleLabelshadow:(NSArray *)labelArray;
-(void)headTitleLabelshadow:(NSArray *)labelArray;
-(void)headTitleLabelshadowBlack:(NSArray *)labelArray;

@end
