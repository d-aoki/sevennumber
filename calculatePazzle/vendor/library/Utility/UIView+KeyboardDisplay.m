//
//  UIView+KeyboardDisplay.m
//  ScheduleConcierge
//
//  Created by Natsuki Funaki on 2013/07/03.
//  Copyright (c) 2013年 Takashi Yasukawa. All rights reserved.
//

#import "UIView+KeyboardDisplay.h"
#import <objc/runtime.h>

@implementation UIView (KeyboardDisplay)

static NSString *keyboardWasShownKey = @"__keyboardWasShown";
static NSString *firstResponderViewsKey = @"__firstResponderViews";


- (void)enableKeyboardDisplay {
    [self registerKeyboardNotifications];
}

- (void)enableKeyboardDisplayAtFirstResponderViews:(NSArray *)atViews {
    [self setFirstResponderViews:[NSMutableArray arrayWithArray:atViews]];
    [self registerKeyboardNotifications];
}

- (void)disableKeyboardDisplay {
    [self setFirstResponderViews:nil];
    [self removeKeyboardNotifications];
}

- (BOOL)keyboardWasShown {
    NSNumber *b = (NSNumber*)(objc_getAssociatedObject(self, (__bridge const void *)(keyboardWasShownKey)));
    return [b boolValue];
}

- (void)setKeyboardWasShown:(BOOL)keyboardWasShown {
    objc_setAssociatedObject(self, (__bridge const void *)(keyboardWasShownKey), [NSNumber numberWithBool:keyboardWasShown], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableArray *)firstResponderViews {
    NSMutableArray *array = (NSMutableArray*)(objc_getAssociatedObject(self, (__bridge const void *)(firstResponderViewsKey)));
    return array;
}

- (void)setFirstResponderViews:(NSMutableArray *)firstResponderViews {
    objc_setAssociatedObject(self, (__bridge const void *)(firstResponderViewsKey), firstResponderViews, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)keyboardDisplayAddFirstResponderView:(UIView*)firstResponder {
    if (self.firstResponderViews == nil) {
        [self setFirstResponderViews:[[NSMutableArray alloc] init]];
    }
    if (![self.firstResponderViews containsObject:firstResponder]) {
        [self.firstResponderViews addObject:firstResponder]; 
    }
}

#pragma mark - keyboard
- (void)registerKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
}

- (void)removeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    // DLog(@"%s", __func__);
    
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval dur = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationOptions animeOption = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    
    self.transform = CGAffineTransformIdentity;
    
    CGFloat moveViewMaxY = CGRectGetMaxY(self.frame);
    CGFloat keyboardTopY = (self.superview.bounds.size.height - kbSize.height);
    BOOL needMove = YES;
    if (self.firstResponderViews != nil && self.firstResponderViews.count > 0) {
        for (UIView *view in self.firstResponderViews) {
            DLog(@"view frame %@", NSStringFromCGRect(view.frame));
            if (view.isFirstResponder) {
                CGRect superRect = [view convertRect:view.bounds toView:self.superview];

                CGFloat maxY = CGRectGetMaxY(superRect);

                DLog(@"view frame %@", NSStringFromCGRect(view.frame));
                DLog(@"superRect frame %@", NSStringFromCGRect(superRect));
                DLog(@"max Y %f , keyboardTopY %f", maxY, keyboardTopY);
                
                if (maxY < keyboardTopY) {
                    needMove = NO;
                } else {
                    moveViewMaxY = maxY;
                    needMove = YES;
                }
                break;
                
            } else {
                needMove = NO;
            }
            
        }
    }
    
    if (needMove) {
        [UIView animateWithDuration:dur delay:0.0 options:animeOption
                         animations:^{
                             self.transform = CGAffineTransformMakeTranslation(0, -(moveViewMaxY - keyboardTopY));
                         } completion:^(BOOL finished) {
                             ;
                         }];        
    }
}

- (void)keyboardWasShown:(NSNotification *)notification {
    //DLog(@"%s", __func__);
    self.keyboardWasShown = YES;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    //DLog(@"%s", __func__);
    if (self.keyboardWasShown) {
        self.keyboardWasShown = NO;
        NSDictionary *info = [notification userInfo];
        
        //CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        NSTimeInterval dur = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        UIViewAnimationOptions animeOption = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
        
        [UIView animateWithDuration:dur delay:0.0 options:animeOption
                         animations:^{
                             self.transform = CGAffineTransformIdentity;
                         } completion:^(BOOL finished) {
                         }];
    }
}

- (void)keyboardWasHidden:(NSNotification *)notification {
    
}


@end
