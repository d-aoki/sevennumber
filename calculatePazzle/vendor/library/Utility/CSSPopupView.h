//
//  CSSPopupViewDelegate.h
//
//  Created by Natsuki Funaki on 2013/06/28.
//  Copyright (c) 2013年 Natsuki Funaki. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CSSPopupViewDelegate;

typedef void (^CSSPopupViewCancelCompletion)(void);
typedef void (^CSSPopupViewDoneCompletion)(NSString *inputText);

@interface CSSPopupView : UIView

@property (assign) id <CSSPopupViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) UIView *overlayView;

@property (readwrite, copy) CSSPopupViewCancelCompletion cancelCompletion;
@property (readwrite, copy) CSSPopupViewDoneCompletion doneCompletion;

+ (void)showCSSPopupViewWithAtView:(UIView*)atView
                   withDefaultName:(NSString*)name
               withBackgroundColor:(UIColor*)color
                      withDelegate:(id<CSSPopupViewDelegate>)delegate;

+ (void)showCSSPopupViewWithAtView:(UIView*)atView
                   withTitle:(NSString*)title
                withPlaceHolder:(NSString*)placeHolder
                   withDefaultName:(NSString*)name
               withBackgroundColor:(UIColor*)color
                      withCancelCompletion:(CSSPopupViewCancelCompletion)cancelCompletion
                      withDoneCompletion:(CSSPopupViewDoneCompletion)doneCompletion;


- (IBAction)doneButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;
- (IBAction)doneNameEdit:(id)sender;

- (void)popupView;

@end

@protocol CSSPopupViewDelegate <NSObject>
@required
- (void)CSSPopupViewDidSelectedDoneButton:(CSSPopupView*)popupView;
- (void)CSSPopupViewDidSelectedCancelButton:(CSSPopupView*)popupView;

@end
