//
//  ScoreViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/03.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "PlayerNameEditViewController.h"



@protocol ScoreViewControllerDelegate;


@interface ScoreViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,GKGameCenterControllerDelegate
,PlayerNameEditViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *adview;

//スコア閲覧View
@property (weak, nonatomic) IBOutlet UIView *rankShowView;
//入賞時にスコア表示ラベル等
@property (weak, nonatomic) IBOutlet UIImageView *prizeImage;
@property (weak, nonatomic) IBOutlet UILabel *prizeMsgLabel;
- (IBAction)rankShowAct:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *rankShowBtn;
@property (weak, nonatomic) IBOutlet UIImageView *rankShowImage;

//ランキング表示テーブルビュー
@property (weak, nonatomic) IBOutlet UITableView *rankshowTableView;


//ラベル系
@property (weak, nonatomic) IBOutlet UILabel *gameoverLbl;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
//->点数
@property (weak, nonatomic) IBOutlet UILabel *levelNum;
@property (weak, nonatomic) IBOutlet UILabel *scoreNum;


//ボタン系
@property (weak, nonatomic) IBOutlet UIButton *backMenu;
- (IBAction)backMenuBtnAct:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *retry;
- (IBAction)retryBtnAct:(id)sender;

//ソーシャル系
- (IBAction)twitterBtnAct:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *twitterBtn;

- (IBAction)facebookBtnAct:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *faceBookBtn;

//ゲームセンター
@property (weak, nonatomic) IBOutlet UIButton *gameCImage;
- (IBAction)gameCImageAct:(id)sender;


// 呼び出すPickerViewControllerのポインタ　※strongを指定してポインタを掴んでおかないと解放されてしまう
@property (strong, nonatomic) PlayerNameEditViewController *playerEditView;

- (void)closePickerView:(PlayerNameEditViewController *)controller;

// 処理のデリゲート先の参照
@property (weak, nonatomic) id<ScoreViewControllerDelegate> delegate;

@end

@protocol ScoreViewControllerDelegate <NSObject>

@end
