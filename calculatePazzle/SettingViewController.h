//
//  SettingViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/02/26.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface SettingViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

//設定完了ボタン
- (IBAction)DoneBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *menuBackBtn;

//各オプション名ラベル
@property (weak, nonatomic) IBOutlet UILabel *operatorLabel;
@property (weak, nonatomic) IBOutlet UILabel *lifeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timelimitLabel;
@property (weak, nonatomic) IBOutlet UILabel *masknumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *backImageLabel;


//四則演算ボタン制限
@property (weak, nonatomic) IBOutlet UISwitch *operatorSW;
- (IBAction)operatorSWAct:(id)sender;

//制限時間
- (IBAction)timelimitSWAct:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *timelimitSW;

//ライフポイント
- (IBAction)lifepointSWAct:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *lifepointSW;

//マスク機能
- (IBAction)maskblocksSWAct:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *maskBlocksSW;

//title
@property (weak, nonatomic) IBOutlet UILabel *optionTitleLabel;

//背景設定
//切り替えての状態
- (IBAction)segBackImageAct:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *segBackImageSW;


//背景設定ボタン
- (IBAction)backgroundBtn:(id)sender;
//背景イメージ表示view
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UIButton *imageSetBtn;



@end
