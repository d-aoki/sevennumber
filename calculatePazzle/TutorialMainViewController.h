//
//  TutorialMainViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/24.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialMainViewController : UIViewController


//とりあえず今は戻るボタンをつけておきます
- (IBAction)backBtnAct:(id)sender;

//強調メッセージのデザイン加工
@property (weak, nonatomic) IBOutlet UILabel *message1;
@property (weak, nonatomic) IBOutlet UILabel *message2;
@property (weak, nonatomic) IBOutlet UILabel *message3;



@end
