//
//  NSNull+IgnoreMessages.m
//  CSSRadio
//
//  Created by Natsuki Funaki on 2013/07/31.
//  Copyright (c) 2013年 CarolSystemSendai Co.,Ltd. All rights reserved.
//

#import "NSNull+IgnoreMessages.h"

@implementation NSNull (IgnoreMessages)

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    if ([self respondsToSelector:[anInvocation selector]]) {
        [anInvocation invokeWithTarget:self];
    }
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    NSMethodSignature *sig=[[NSNull class] instanceMethodSignatureForSelector:aSelector];
    // Just return some meaningless signature
    if (sig == nil) {
        sig = [NSMethodSignature signatureWithObjCTypes:"@^v^c"];
    }
    
    return sig;
}

@end
