//
//  GameCenterUtils.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/17.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

@interface GameCenterUtils : NSObject<GKGameCenterControllerDelegate>

@end
