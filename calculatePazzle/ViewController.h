//
//  ViewController.h
//  calculatePazzle
//
//  Created by Daichi Aoki on 2014/02/17.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioToolbox.h>

#import "SettingViewController.h"
#import "ScoreViewController.h"


SystemSoundID sound;

@interface ViewController : UIViewController<ScoreViewControllerDelegate>

//テンプレートの枠
@property (weak, nonatomic) IBOutlet UIView *mainPanel;
@property (weak, nonatomic) IBOutlet UIView *scorePanel;

//演算子
//各View
@property (weak, nonatomic) IBOutlet UIView *minusView;
@property (weak, nonatomic) IBOutlet UIView *multipleView;
@property (weak, nonatomic) IBOutlet UIView *parView;
//各ボタン
@property (weak, nonatomic) IBOutlet UIButton *minusBtn;
@property (weak, nonatomic) IBOutlet UIButton *multipleBtn;
@property (weak, nonatomic) IBOutlet UIButton *parBtn;

//mask中メッセージ
@property (weak, nonatomic) IBOutlet UILabel *maskMemory;



//合計点数
@property (weak, nonatomic) IBOutlet UILabel *totalScore;
//「perfect」ラベル
@property (weak, nonatomic) IBOutlet UILabel *perfectLbl;
//「×２」ラベル
@property (weak, nonatomic) IBOutlet UILabel *twoPointLbl;
//Level値ラベル
@property (weak, nonatomic) IBOutlet UILabel *levelLbl;

//ライフポイント
@property (weak, nonatomic) IBOutlet UIImageView *life1;
@property (weak, nonatomic) IBOutlet UIImageView *life2;
@property (weak, nonatomic) IBOutlet UIImageView *life3;

//メニューへ戻るボタン
- (IBAction)backMenuBtnAct:(id)sender;

//undoボタン
@property (weak, nonatomic) IBOutlet UIButton *undoBtn;
- (IBAction)undoBtnAct:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *undoView;

//チャレンジモード用機能追加お知らせラベル
@property (weak, nonatomic) IBOutlet UILabel *maskMsg;
@property (weak, nonatomic) IBOutlet UILabel *operatorMsg;

//チャレンジお知らせイメージ
@property (weak, nonatomic) IBOutlet UIImageView *operatorImage;
@property (weak, nonatomic) IBOutlet UILabel *operatorImageMsg;
@property (weak, nonatomic) IBOutlet UIImageView *maskImage;
@property (weak, nonatomic) IBOutlet UILabel *maskImageMsg;



//四則演算ボタン
- (IBAction)minusAct:(id)sender;
- (IBAction)multipleAct:(id)sender;
- (IBAction)parAct:(id)sender;

// 呼び出すViewControllerのポインタ　※strongを指定してポインタを掴んでおかないと解放されてしまう
@property (strong, nonatomic) ScoreViewController *ScoreViewController;


@end
