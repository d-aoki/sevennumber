//
//  RankingUtils.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/14.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RankingUtils : NSObject

//ランキングの順位付け
//-(void)rankingRegist:(UILabel *)score;
-(void)rankingRegist:(UILabel *)score modeFlg:(int)modeFlg;

//順位の確認のみ行うフラグ
//-(void)checkFlgRanking:(UILabel *)ScoreLabel;
-(void)checkFlgRanking:(UILabel *)ScoreLabel mode:(int)modeFlg;

@end
