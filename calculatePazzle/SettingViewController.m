//
//  SettingViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/02/26.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "SettingViewController.h"
#import "DesignUtils.h"
#import "MainViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "SoundsUtils.h"
#import "ProcessingViewUtils.h"

@interface SettingViewController ()

@end

@implementation SettingViewController
{
    NSInteger operatorInt,timelimiterInt,lifepointInt,maskblocksInt,segmentInt;
    
    //NSUserDefault(四則演算制限、制限時間)
    NSUserDefaults *_operatorUD,*_timeLimiterUD,*_lifepointUD,*_maskblocksUD,*_segmentUD;
    NSUserDefaults *_backImageUD;
    
    AVAudioPlayer *audio,*selectSound;
    
    //カメラロール
    NSString *assetsUrl;    //assetsUrlを格納するインスタンス
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //設定の設定背景確認ImageViewのデザイン
    [_backImageView.layer setBorderColor:[UIColor blackColor].CGColor];
    [_backImageView.layer setBorderWidth:1.5];
    _backImageView.layer.cornerRadius = 2.0;
    
    //////////////////
    //広告
    // [banner <appC cloud>]
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    //広告Viewは最後に追加する
    [self.view addSubview:appDelegate.simpleAdView];
    
    _optionTitleLabel.shadowColor = [UIColor lightGrayColor];
    _optionTitleLabel.shadowOffset = CGSizeMake(4,3);
    
    
    NSArray *labelArray = [[NSArray alloc]initWithObjects:_operatorLabel,_lifeLabel,_timelimitLabel,_masknumberLabel,_backImageLabel, nil];
    
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadow:labelArray];
    
    
    
    // NSUserDefaultsからデータを読み込む(初期値は0)
    //四則演算制限
    _operatorUD = [NSUserDefaults standardUserDefaults];
    operatorInt = [_operatorUD integerForKey:@"KEY_Operator"];  // KEY_Operatorの内容をint型として取得
    //制限時間
    _timeLimiterUD = [NSUserDefaults standardUserDefaults];
    timelimiterInt = [_timeLimiterUD integerForKey:@"KEY_Timelimiter"];  // KEY_Timelimiterの内容をint型として取得
    //ライフポイント
    _lifepointUD = [NSUserDefaults standardUserDefaults];
    lifepointInt = [_lifepointUD integerForKey:@"KEY_Lifepoint"];
    //マスク
    _maskblocksUD = [NSUserDefaults standardUserDefaults];
    maskblocksInt = [_maskblocksUD integerForKey:@"KEY_Maskblocks"];
    //背景
    _segmentUD = [NSUserDefaults standardUserDefaults];
    segmentInt = [_segmentUD integerForKey:@"KEY_Segment"];
    
    //SWがオフの場合は背景の選択は出来ません
    if(segmentInt == 0){
        //_backImageView.alpha = 0.6;
        //背景がデフォルト設定の場合は背景選択ボタンは押せません
        [_imageSetBtn setEnabled:NO];
        
        //デフォルト背景を設定
        self.backImageView.image = [UIImage imageNamed:@"renga.jpg"];
        
        //設定ボタンを非表示にします
        _imageSetBtn.alpha = 0;
    }else{
        
        //設定の背景を設定します
        [self settingBackImage];
        
        _imageSetBtn.alpha = 1.0f;
    }
    
    //setOnメソッドの実行(スイッチのON/OFF)
    [_operatorSW setOn:operatorInt];
    [_timelimitSW setOn:timelimiterInt];
    [_lifepointSW setOn:lifepointInt];
    [_maskBlocksSW setOn:maskblocksInt];
    [_segBackImageSW setOn:segmentInt];
    
    
    //設定画面のイメージ画像に写真を貼付ける
    //[self settingBackImage];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    //背景
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    [designUtils backImageSettingAct:self.view];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


# pragma mark - SwicthOption

//***********************
//四則演算ボタン制限スイッチ
//***********************
- (IBAction)operatorSWAct:(id)sender {
    
    [self SWSounds];

    //operatorInt(0->OFF,1->ON)
    if(operatorInt == 0){
        operatorInt = 1;
    }else{
        operatorInt = 0;
    }
    
    //Stateをキー名"KEY_Operator"で保存とセーブ
    [_operatorUD setInteger:operatorInt forKey:@"KEY_Operator"];
    [_operatorUD synchronize];
    
}

//******************
//制限時間追加スイッチ
//******************
- (IBAction)timelimitSWAct:(id)sender {
    
    [self SWSounds];
    
    //timelimiterInt(0->OFF,1->ON)
    if(timelimiterInt == 0){
        timelimiterInt = 1;
    }else{
        timelimiterInt = 0;
    }
    
    //Stateをキー名"KEY_Operator"で保存とセーブ
    [_timeLimiterUD setInteger:timelimiterInt forKey:@"KEY_Timelimiter"];
    [_timeLimiterUD synchronize];
    
}

//**************************
//ライフポイント機能追加スイッチ
//**************************

- (IBAction)lifepointSWAct:(id)sender {
    
    [self SWSounds];
    
    //lifepointInt(0->OFF,1->ON)
    if(lifepointInt == 0){
        lifepointInt = 1;
    }else{
        lifepointInt = 0;
    }
    
    //Stateをキー名"KEY_Operator"で保存とセーブ
    [_lifepointUD setInteger:lifepointInt forKey:@"KEY_Lifepoint"];
    [_lifepointUD synchronize];
    
}


//***************************
//マスク機能追加スイッチ
//***************************
- (IBAction)maskblocksSWAct:(id)sender {
    
    [self SWSounds];
    
    //maskblocksInt(0->OFF,1->ON)
    if(maskblocksInt == 0){
        maskblocksInt = 1;
    }else{
        maskblocksInt = 0;
    }
    
    //Stateをキー名"KEY_Operator"で保存とセーブ
    [_maskblocksUD setInteger:maskblocksInt forKey:@"KEY_Maskblocks"];
    [_maskblocksUD synchronize];
    
}

- (IBAction)segBackImageAct:(id)sender {
    
    [self SWSounds];
    
    //segmentInt(0->OFF,1->ON)
    if(segmentInt == 0){
        segmentInt = 1;
        //_backImageView.alpha = 1.0;
        [_imageSetBtn setEnabled:YES];
        [self settingBackImage];
        
        //設定ボタンを表示します
        _imageSetBtn.alpha = 1.0f;
        
        
        
    }else{
        segmentInt = 0;
        //_backImageView.alpha = 0.6;
        //背景がデフォルト設定の場合は背景選択ボタンは押せません
        [_imageSetBtn setEnabled:NO];
        //デフォルト背景を設定
        self.backImageView.image = [UIImage imageNamed:@"renga.jpg"];
        
        //設定ボタンを非表示にします
        _imageSetBtn.alpha = 0;
        
    }
    
    //Stateをキー名"KEY_Segment"で保存とセーブ
    [_segmentUD setInteger:segmentInt forKey:@"KEY_Segment"];
    [_segmentUD synchronize];
}

# pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1)
    {
        //一つ前の画面に戻る
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


# pragma mark - UIButtonAction

//*************************
//前の画面に戻る(GameStart!!)
//*************************
- (IBAction)DoneBtn:(id)sender {
    
    //選択音
    SoundsUtils *soundsUtils = [[SoundsUtils alloc]init];
    selectSound = [soundsUtils effectSounds:@"click"];
    [selectSound play];
    
    NSString *message = @"設定を変更します。よろしいですか？";
    
    // 生成と同時に各種設定も完了させる例
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"確認"
                                                  message:message
                                                 delegate:self
                                        cancelButtonTitle:@"Cancel"
                                        otherButtonTitles:@"OK", nil
                         ];
    
    [alert show];
    
}


#pragma mark -background

//***************************
//カメラロールにアクセスします
//***************************
- (IBAction)backgroundBtn:(id)sender {
    
    //imagePickerViewのインスタンス
    UIImagePickerController *imagePickerView = [[UIImagePickerController alloc]init];
    imagePickerView.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerView.delegate = self;
    [self presentViewController:imagePickerView animated:YES completion:nil];
    
}

// MARK: UIImagePickerView delegate

//********************************
//写真選択後に呼ばれるデリゲートメソッド
//********************************
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    DLog(@"写真が選択されました -> %@",info);

    //カメラライブラリから選んだ写真のURLを取得。
    assetsUrl = [(NSURL *)[info objectForKey:@"UIImagePickerControllerReferenceURL"] absoluteString];
    [self showPhoto:assetsUrl];
    
    
    //カメラロールを閉じ、前の画面に戻ります(現時点では)
    [self dismissViewControllerAnimated:YES completion:nil];

}

//assetsから取得した画像を表示する
-(void)showPhoto:(NSString *)url
{
    // AssetsLibratyを作成(初期化しないと出来ません)
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    //URLからALAssetを取得
    [library assetForURL:[NSURL URLWithString:url]
             resultBlock:^(ALAsset *asset) {
                 
                 //画像があればYES、無ければNOを返す
                 if(asset){
                     //NSLog(@"データがあります");
                     //ALAssetRepresentationクラスのインスタンスの作成
                     ALAssetRepresentation *assetRepresentation = [asset defaultRepresentation];
                     
                     //ALAssetRepresentationを使用して、フルスクリーン用の画像をUIImageに変換
                     //fullScreenImageで元画像と同じ解像度の写真を取得する。
                     UIImage *fullscreenImage = [UIImage imageWithCGImage:[assetRepresentation fullScreenImage]];
                     
                     self.backImageView.image = fullscreenImage; //イメージをセット
                     
                     //背景に設定する画像URLを保存する
                     NSData *data = UIImagePNGRepresentation(fullscreenImage);
                     [_backImageUD setObject:data forKey:@"KEY_BackImage"];
                     //画像情報の保存
                     [_backImageUD synchronize];
                 }
                 
             } failureBlock: nil];
}


//****************************
//現在の背景イメージを設定しておく
//****************************
-(void)settingBackImage{
    
    //背景画像の設定(カメラロール)
    _backImageUD = [NSUserDefaults standardUserDefaults];
    NSData *data = [_backImageUD objectForKey:@"KEY_BackImage"];
    
    //DesignUtils *designUtils = [[DesignUtils alloc]init];
    //[designUtils backImageCameraSettingAct:self.view image:data];
    
    //NSData型の背景画像情報をUIImageにキャスト
    UIImage *image = [UIImage imageWithData:data];
    
    self.backImageView.image = image; //イメージをセット
    
}

//*************************
//SW切り替え音
//*************************
-(void)SWSounds{
    
    SoundsUtils *soundUtils = [[SoundsUtils alloc]init];
    audio = [soundUtils effectSounds:@"calSoundsAct"];
    [audio play];
}


#pragma mark - fade Animated

//******************
//フェードイン(画像)
//******************
-(void)fadeInImage:(UIImageView *)fadeInImage{
    
    //フェードイン
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.3];
    //目標のアルファ値を指定
    fadeInImage.alpha = 1;
    
    //アニメーション実行
    [UIView commitAnimations];
}

//*******************
//フェードアウト(画像)
//*******************
-(void)fadeoutImage:(UIImageView *)fadeoutImage{
    
    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.2];
    //目標のアルファ値を指定
    fadeoutImage.alpha = 0;
    //アニメーション実行
    [UIView commitAnimations];
}

@end
