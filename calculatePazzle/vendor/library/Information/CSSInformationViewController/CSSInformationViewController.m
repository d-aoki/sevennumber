//
//  CSSInformationViewController.m
//
//  Created by  on 12/05/30.
//  Copyright (c) 2013年

#import "CSSInformationViewController.h"

@interface CSSInformationViewController ()

@end

@implementation CSSInformationViewController

+ (void)showWithRootViewController:(UIViewController*)rootViewController showCompletion:(void (^)(void))completion dismissCompletion:(CSSInformationDismissCompletion)dismissCompletion {
    CSSInformationViewController *infoCtr = [[CSSInformationViewController alloc] init];
    infoCtr.dismissCompletion = dismissCompletion;
    
    infoCtr.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [rootViewController presentViewController:infoCtr animated:YES completion:completion];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewDidAppear:(BOOL)animated {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    [self createUI:orientation];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
    // return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    for (UIView *v in self.view.subviews) {
        [v removeFromSuperview];
    }
    
    [self createUI:toInterfaceOrientation];
}

- (void)createUI:(UIInterfaceOrientation)orientation {
    
    CGRect mainScreenBounds = [[UIScreen mainScreen] bounds];
    CGFloat baseHeightOrignY = mainScreenBounds.size.height/3;
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        mainScreenBounds = CGRectMake(mainScreenBounds.origin.x, mainScreenBounds.origin.y,
                                      mainScreenBounds.size.height, mainScreenBounds.size.width);
        baseHeightOrignY = mainScreenBounds.size.height/4;
    }
    //infoCtr.view.bounds = mainScreenBounds;
    
    self.view.backgroundColor = [UIColor colorWithRed:0xCC/255.0f green:0 blue:0 alpha:1.0];
    
    // 閉じる処理
    UITapGestureRecognizer *closeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeInformationTap:)];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:closeTap];
    
    // 各部品
    self.logoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, baseHeightOrignY)];
    self.logoView.center = CGPointMake(mainScreenBounds.size.width/2, self.logoView.center.y);
    [self.logoView setImage:[UIImage imageNamed:@"CSSLogo.png"]];
    self.logoView.contentMode = UIViewContentModeScaleAspectFit;
    self.logoView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.logoView];
    
    CGRect fullWidthRect = CGRectMake(0, 0, mainScreenBounds.size.width, 22);
    
    self.catchCopyLabel = [[UILabel alloc] initWithFrame:CGRectOffset(fullWidthRect, 0, CGRectGetMaxY(self.logoView.frame))];
    self.catchCopyLabel.textAlignment = NSTextAlignmentCenter;
    self.catchCopyLabel.textColor = [UIColor whiteColor];
    self.catchCopyLabel.font = [UIFont systemFontOfSize:17];
    self.catchCopyLabel.backgroundColor = [UIColor clearColor];
    self.catchCopyLabel.text = @"企画・提案　そして仙台から発信";
    [self.view addSubview:self.catchCopyLabel];
    
    
    self.companyNameLabel = [[UILabel alloc] initWithFrame:CGRectOffset(fullWidthRect, 0, CGRectGetMaxY(self.catchCopyLabel.frame)+22)];
    self.companyNameLabel.textAlignment = NSTextAlignmentCenter;
    self.companyNameLabel.textColor = [UIColor whiteColor];
    self.companyNameLabel.font = [UIFont systemFontOfSize:16];
    self.companyNameLabel.backgroundColor = [UIColor clearColor];
    self.companyNameLabel.text = @"キャロルシステム仙台株式会社";
    [self.view addSubview:self.companyNameLabel];
    
    self.companyWebSiteURLLabel = [[UILabel alloc] initWithFrame:CGRectOffset(fullWidthRect, 0, CGRectGetMaxY(self.companyNameLabel.frame))];
    self.companyWebSiteURLLabel.textAlignment = NSTextAlignmentCenter;
    self.companyWebSiteURLLabel.textColor = [UIColor colorWithRed:0.7 green:0.9 blue:1.0 alpha:1.0];
    self.companyWebSiteURLLabel.font = [UIFont systemFontOfSize:14];
    self.companyWebSiteURLLabel.backgroundColor = [UIColor clearColor];
    self.companyWebSiteURLLabel.text = @"http://www.carolsendai.co.jp";
    [self.view addSubview:self.companyWebSiteURLLabel];
    self.companyWebSiteURLLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *openUrlTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCompanyWebSiteURLTap:)];
    [self.companyWebSiteURLLabel addGestureRecognizer:openUrlTap];
    
    // ライセンス関係を表示するための (開発したアプリで利用しているライセンス情報)
    CGFloat licenseHeight = CGRectGetMaxY(mainScreenBounds) - CGRectGetMaxY(self.companyWebSiteURLLabel.frame) - 44;
    self.licenseTextView = [[UITextView alloc] initWithFrame:CGRectInset(CGRectOffset(CGRectMake(0, 0, fullWidthRect.size.width, licenseHeight), 0, CGRectGetMaxY(self.companyWebSiteURLLabel.frame)), 8, 8)];
    self.licenseTextView.textAlignment = NSTextAlignmentLeft;
    self.licenseTextView.textColor = [UIColor whiteColor];
    self.licenseTextView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.25];
    self.licenseTextView.editable = NO;
    // ライセンステキストを読み込む
    NSError *error = nil;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CSSUseLicense" ofType:@"txt"];
    NSString *licenseString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    if (error != nil) {
        ALog(@"ライセンス情報を記載したファイルの読み込みに失敗したようです。%@", error);
    }
    if (licenseString == nil || [licenseString isEqualToString:@""]) {
        self.licenseTextView.hidden = YES;
    }
    self.licenseTextView.text = licenseString;
    [self.view addSubview:self.licenseTextView];
    
    
    
    // 最下部コピーライト
    self.copyrightLabel = [[UILabel alloc] initWithFrame:CGRectOffset(fullWidthRect, 0, CGRectGetMaxY(mainScreenBounds) - 44)];
    self.copyrightLabel.textAlignment = NSTextAlignmentCenter;
    self.copyrightLabel.textColor = [UIColor whiteColor];
    self.copyrightLabel.font = [UIFont systemFontOfSize:11];
    self.copyrightLabel.minimumScaleFactor = 0.5;
    self.copyrightLabel.backgroundColor = [UIColor clearColor];
    self.copyrightLabel.text = @"Copyright 2013 CarolSystemSendai Co., Ltd. All rights reserved.";
    [self.copyrightLabel sizeToFit];
    [self.view addSubview:self.copyrightLabel];
}

/**
 * 戻るボタン
 */
- (void)closeInformationTap:(UITapGestureRecognizer*)tap {
    [self dismissViewControllerAnimated:YES completion:self.dismissCompletion];
}

/**
 * サイトを開くボタンエリア
 */
- (void)openCompanyWebSiteURLTap:(UITapGestureRecognizer*)tap {
    NSURL *url = [NSURL URLWithString:@"http://www.carolsendai.co.jp/"];
    [[UIApplication sharedApplication] openURL:url];
}

-(void)dealloc {
    //NSLog(@"CSS Information dealloc!");
}

@end
