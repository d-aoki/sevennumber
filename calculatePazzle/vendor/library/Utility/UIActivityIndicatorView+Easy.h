//
//  UIActivityIndicatorView+Easy.h
//  GrowBookkeeping
//
//  Created by Natsuki Funaki on 2013/04/10.
//  Copyright (c) 2013年 CarolSystem Sendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActivityIndicatorView (Easy)

+ (void)showActivityIndicator:(UIView*)onView
                        style:(UIActivityIndicatorViewStyle)style
                      message:(NSString*)message;
+ (void)showActivityIndicator:(UIView*)onView
                       center:(CGPoint)centerPoint
                        style:(UIActivityIndicatorViewStyle)style
                      message:(NSString*)message;
+ (void)hideActivityIndicator:(UIView*)parentView;
@end
