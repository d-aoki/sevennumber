//
//  CSSendaiBanner.h
//  CSReport
//
//  Created by Natsuki Funaki on 2013/05/13.
//  Copyright (c) 2013年 CarolSystem Sendai. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GADCustomEventBanner.h"

@interface CSSendaiBanner : NSObject <GADCustomEventBanner, UIWebViewDelegate>

@property (assign, nonatomic) id <GADCustomEventBannerDelegate> delegate;

@end
