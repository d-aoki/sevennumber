//
//  UIColor+HexColoring.h
//  GrowBookkeeping
//
//  Created by Natsuki Funaki on 2013/04/01.
//  Copyright (c) 2013年 CarolSystem Sendai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColoring)

//RGBAを16進数型(ffa500ffなど)で指定します
+ (UIColor*)colorWithHex:(uint32_t)hex;
+ (UIColor*)colorWithHexString:(NSString*)string;
@end
