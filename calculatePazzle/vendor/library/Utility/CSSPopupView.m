//
//  CSSPopupViewDelegate.m
//
//  Created by Natsuki Funaki on 2013/06/28.
//  Copyright (c) 2013年 Natsuki Funaki. All rights reserved.
//

#import "CSSPopupView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+KeyboardDisplay.h"

@implementation CSSPopupView
{
    BOOL _keyboardWasShown;
}

+ (void)showCSSPopupViewWithAtView:(UIView*)atView
                withDefaultName:(NSString*)name
                             withBackgroundColor:(UIColor*)color
                           withDelegate:(id<CSSPopupViewDelegate>)delegate {


    UINib* nibInputAccView = [UINib nibWithNibName:@"CSSPopupView" bundle:nil];
    CSSPopupView *popupView = [[nibInputAccView instantiateWithOwner:nil options:nil] objectAtIndex:0];
    popupView.backgroundColor = color;

    popupView.nameTextField.text = name;

    popupView.delegate = delegate;
    popupView.center = atView.center;
    popupView.clipsToBounds = YES;
    popupView.layer.cornerRadius = 8.0f;
    
    [atView addSubview:popupView];
    [popupView popupView];
    
    // バックoverlay
    popupView.overlayView = [[UIView alloc] initWithFrame:atView.bounds];
    popupView.overlayView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [atView insertSubview:popupView.overlayView belowSubview:popupView];
    [popupView.nameTextField becomeFirstResponder];

    [atView enableKeyboardDisplay];
}

+ (void)showCSSPopupViewWithAtView:(UIView*)atView
                         withTitle:(NSString*)title
                   withPlaceHolder:(NSString*)placeHolder
                   withDefaultName:(NSString*)name
               withBackgroundColor:(UIColor*)color
              withCancelCompletion:(CSSPopupViewCancelCompletion)cancelCompletion
                withDoneCompletion:(CSSPopupViewDoneCompletion)doneCompletion {
    
    UINib* nibInputAccView = [UINib nibWithNibName:@"CSSPopupView" bundle:nil];
    CSSPopupView *popupView = [[nibInputAccView instantiateWithOwner:nil options:nil] objectAtIndex:0];
    popupView.backgroundColor = color;
    
    popupView.titleLabel.text = title;
    popupView.nameTextField.placeholder = placeHolder;
    popupView.nameTextField.text = name;
    
    popupView.delegate = nil;
    popupView.cancelCompletion = cancelCompletion;
    popupView.doneCompletion = doneCompletion;
    popupView.center = atView.center;
    popupView.clipsToBounds = YES;
    popupView.layer.cornerRadius = 8.0f;
    
    [atView addSubview:popupView];
    [popupView popupView];
    
    // バックoverlay
    popupView.overlayView = [[UIView alloc] initWithFrame:atView.bounds];
    popupView.overlayView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [atView insertSubview:popupView.overlayView belowSubview:popupView];
    [popupView.nameTextField becomeFirstResponder];
    
    [popupView enableKeyboardDisplay];

}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // キーボード通知
        [self enableKeyboardDisplay];
    }
    
    return self;
}

#pragma mark - IBAction
- (IBAction)doneButtonClicked:(id)sender {
    NSString *inputText = self.nameTextField.text;
    if ([@"" isEqualToString:inputText]) {
        return;
    }

    if (self.delegate != nil) {
        [self.delegate CSSPopupViewDidSelectedDoneButton:self];
    } else {
        self.doneCompletion(self.nameTextField.text);
    }
    
    [self hideView];
}

- (IBAction)cancelButtonClicked:(id)sender {
    if (self.delegate != nil) {
        [self.delegate CSSPopupViewDidSelectedCancelButton:self];
    } else {
        self.cancelCompletion();
    }
    [self hideView];
}

- (IBAction)doneNameEdit:(id)sender {
    [self.nameTextField resignFirstResponder];
}

#pragma mark - util
- (void)hideView {

    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.transform = CGAffineTransformMakeScale(0.1, 0.1);
        self.alpha = 0.0f;
        
        self.overlayView.alpha = 0.0f;
        
    } completion:^(BOOL finished) {
        [self.overlayView removeFromSuperview];
        [self removeFromSuperview];
    }];
}

- (void)popupView
{
    __weak __block UIView *view = self;
    view.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.5f,0.5f);
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.2f,1.2f);
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.1
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              view.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.9f,0.9f);
                                          }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:0.1
                                                                    delay:0.0
                                                                  options:UIViewAnimationOptionCurveEaseOut
                                                               animations:^{
                                                                   view.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0f,1.0f);
                                                               }
                                                               completion:nil
                                               ];
                                              
                                          }
                          ];
                         
                     }   
     ];  
}

#pragma mark - dealloc
- (void)dealloc {
    DLog(@"dealloc!");
    [self disableKeyboardDisplay];
}

@end
