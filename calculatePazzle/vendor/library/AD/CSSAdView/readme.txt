/**
 * キャロルシステム仙台株式会社
 * 広告表示用共通ライブラリ
 *
 * 船木 夏樹
 */

# last update 2013/07/01

iOSで広告を表示するための共通ライブラリです。
ライブラリは、以下の2点を行うものです。
* Googleの提供するadmob及びadmob内のメディエーション機能を利用した複数広告の切替え表示。
* キャロルシステム仙台独自の自社広告の表示。

基本的に、広告に関する設定はプログラム上では行いません。（行えません）
広告サービスの利用には、広告サービスが提供する専用のWEBシステムを通して、
アプリ毎にAPIキーの設定や広告枠の設定が必要であるため、必ず各々で設定を行ってください。

Google admobの利用には、以下の資料を参考にしてください。
https://developers.google.com/mobile-ads-sdk/docs/admob/mediation#ios

#########################

# quick start
ビルドのために必要である設定は以下の通りです。

1. other linker flags に -Objc を追加
2. フレームワークに以下を追加
 * AdSupport.framework
 * StoreKit.framework
 * AudioToolbox.framework
 * MessageUI.framework
 * SystemConfiguration.framework
 * CoreGraphics.framework
#nend
 * Security.framework
#iAd
 * iAd.framework
#appC
 * CoreTelephony.framework
#GameFeat
 * CoreTelephony.framework
#mMedia
 * AVFoundation.framework
 * CFNetwork.framework
 * CoreLocation.framework
 * Foundation.framework
 * MediaPlayer.framework
 * MobileCoreServices.framework
 * PassKit.framework
 * QuartzCore.framework
 * Security.framework
 * Social.framework
 * SpeechKit.framework (Included in the Millennial SDK Download)
 