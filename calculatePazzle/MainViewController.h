//
//  MainViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/02/28.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>

//ソーシャル系フレームワーク
#import <Social/Social.h>

//GameCenter
#import <GameKit/GameKit.h>

@protocol MainViewControllerDelegate;


@interface MainViewController : UIViewController<GKGameCenterControllerDelegate>

//チュートリアル
- (IBAction)tutorialButton:(id)sender;


//題名
@property (weak, nonatomic) IBOutlet UILabel *titleSeven;
@property (weak, nonatomic) IBOutlet UILabel *titleNumber;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;


//ランキングモード(timeAttackplay)
@property (weak, nonatomic) IBOutlet UILabel *timeAtackLabel;

@property (weak, nonatomic) IBOutlet UILabel *rankPlayLabel;
@property (weak, nonatomic) IBOutlet UIButton *rankPlayBtn;
- (IBAction)rankplayBtnAct:(id)sender;
- (IBAction)rankplayBtndownAct:(id)sender;
- (IBAction)rankplayBtnoutSideAct:(id)sender;


//チャレンジモード
@property (weak, nonatomic) IBOutlet UILabel *challengeLabel;
@property (weak, nonatomic) IBOutlet UILabel *challengePlayLabel;
@property (weak, nonatomic) IBOutlet UIButton *challengeBtn;
- (IBAction)challengeBtnAct:(id)sender;

- (IBAction)challengeBtndownAct:(id)sender;
- (IBAction)challengeBtnoutSideAct:(id)sender;
- (IBAction)challengeBtndownAct35:(id)sender;
- (IBAction)challengeBtnoutSideAct35:(id)sender;

//ランキング(GameCenter,ローカル)
@property (weak, nonatomic) IBOutlet UILabel *rankingLabel;
@property (weak, nonatomic) IBOutlet UIButton *rankingBtn;
- (IBAction)rankingBtnAct:(id)sender;
- (IBAction)rankingBtndownAct:(id)sender;
- (IBAction)rankingBtnoutSideAct:(id)sender;
- (IBAction)rankingBtndownAct4:(id)sender;
- (IBAction)rankingBtnoutSideAct4:(id)sender;



//ゲームスタート(Free)ボタン
@property (weak, nonatomic) IBOutlet UILabel *freePlayLabel;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UILabel *playLabel;
- (IBAction)playBtnAct:(id)sender;
- (IBAction)playBtndownAct:(id)sender;
- (IBAction)playBtnoutSideAct:(id)sender;


//オプションボタン
@property (weak, nonatomic) IBOutlet UIButton *optionBtn;
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;
- (IBAction)optionBtnAct:(id)sender;
- (IBAction)optionBtndownAct:(id)sender;
- (IBAction)optionBtnoutSideAct:(id)sender;

//バージョン表示ラベル
@property (weak, nonatomic) IBOutlet UILabel *appVersionLabel;

//ソーシャルボタン系
- (IBAction)twitterBtnMenuAct:(id)sender;
- (IBAction)FacebookBtnMenuAct:(id)sender;

// 処理のデリゲート先の参照
//@property (weak, nonatomic) id<MainViewControllerDelegate> delegate;

@end

@protocol MainViewControllerDelegate <NSObject>

@end
