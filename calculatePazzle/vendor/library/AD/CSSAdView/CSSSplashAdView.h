//
//  CSSSplashAdView.h
//
//  全画面広告を表示するための便利クラスです。
//  表示したいタイミングで呼び出します。
//
//  AMoAdは、設定ファイルが別に用意されているため、アプリケーション側で設定することはできません。
//  AMoAdSettings.txtを開発中、リリース時に正しく設定してください。
//
//  Created by Natsuki Funaki on 2014/03/05.
//  Copyright (c) 2014年 CarolSystemSendai Co.,Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "appCCutinView.h"
#import "AMoAdSDK.h"

#import "MMAdView.h"

typedef NS_OPTIONS(NSUInteger, CSSSplashAdSource) {
    CSSSplashAdSourceNone       = 0,
    CSSSplashAdSourceAppC       = 1 << 0,   // 1
    CSSSplashAdSourceAMoAd      = 1 << 1,   // 2
    CSSSplashAdSourceMMAd       = 1 << 2,   // 4
    CSSSplashAdSourceAll        = CSSSplashAdSourceAppC | CSSSplashAdSourceAMoAd | CSSSplashAdSourceMMAd,
};


@protocol CSSSplashAdViewDelegate;
@protocol CSSSplashAdViewDelegate <NSObject>

@end


@interface CSSSplashAdView : UIView <CSSSplashAdViewDelegate>

@property (copy, nonatomic) void (^adDismissCompletion)(void);

@property (weak, nonatomic) UIViewController *viewController;
@property (assign, nonatomic) id <CSSSplashAdViewDelegate> delegate;
@property (assign, nonatomic) CSSSplashAdSource adSources;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) UIButton *dismissButton;

@property (strong, nonatomic) appCCutinView *appCCutinView;

@property (strong, nonatomic) MMRequest *mmRequest;
@property (strong, nonatomic) MMAdView *mmAdView;

@property (strong, nonatomic) UIViewController *amoAdView;

/**
 * 広告ソースを指定して、スプラッシュ広告用のビューを作成します。
 * 広告ソースは、「|」を利用して複数指定できます。 ex) CSSSplashAdSourceAppC | CSSSplashAdSourceAMoAd
 */
+ (CSSSplashAdView*)splashAdViewWithSources:(CSSSplashAdSource)adSources viewController:(UIViewController*)viewController dismissCompletion:(void (^)(void))completion;

/**
 * スプラッシュ広告を表示します。
 */
- (void)showSplashAdWithAdSource:(CSSSplashAdSource)adSource;

/**
 * スプラッシュ広告を非表示にします。
 */
- (void)hideSplahAd;

@end

