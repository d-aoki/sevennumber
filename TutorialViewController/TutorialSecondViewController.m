//
//  TutorialSecondViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/24.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "TutorialSecondViewController.h"
#import "DesignUtils.h"
#import "ProcessingViewUtils.h"

@interface TutorialSecondViewController ()

@end

@implementation TutorialSecondViewController

{
    CGPoint _movePageTouch;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //メッセージを強調する
    NSArray *mainArray = [[NSArray alloc]initWithObjects:_mainMsg,_mainMsg2, nil];
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadowBlack:mainArray];
    
    
    //デフォルト背景
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    //[designUtils gradation:self.view];
    [designUtils backImageSettingAct:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Touch Event

//********************************************
//画面がタッチされたら、次のページに遷移する用にします
//********************************************
-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    _movePageTouch = [touch locationInView:self.view];
    
    DLog(@"ページがタッチされました。ページ遷移します");
    
    //playViewのインスタンス
    UIViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialMarksViewController"];
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:view animated:NO];
    
    
}

@end
