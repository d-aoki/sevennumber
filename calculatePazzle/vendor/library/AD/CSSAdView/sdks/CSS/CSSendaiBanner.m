//
//  CSSendaiBanner.m
//  CSReport
//
//  Created by Natsuki Funaki on 2013/05/13.
//  Copyright (c) 2013年 CarolSystem Sendai. All rights reserved.
//

#import "CSSendaiBanner.h"
#import <QuartzCore/QuartzCore.h>

@implementation CSSendaiBanner
{
    UIImageView *_imageView;
    
    NSString *_openURL;
    
    UIView *_staticContentView;
    UIWebView *_staticContentWebView;
    UIToolbar *_staticFooter;
    
    UIActivityIndicatorView *_activityIndicatorView;

}

#pragma mark - GAD
- (void)requestBannerAd:(GADAdSize)adSize
              parameter:(NSString *)serverParameter
                  label:(NSString *)serverLabel
                request:(GADCustomEventRequest *)request {
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                         GAD_SIZE_320x50.width, GAD_SIZE_320x50.height)];
    
    // IDはサーバパラメータで渡す様にしている
    // 1広告の設定は、|(パイプ)をセパレータとして利用する。
    // 形式：　「表示するバナー画像ファイル名」|「リンク先URL」|「アプリ外ブラウザを開くか」(1 or 0),...
    NSArray *ids = [serverParameter componentsSeparatedByString:@","];
    if (ids.count < 1) {
        ALog(@"CSSBannerの広告表示のためのパラメータがおかしい様です。 parameter:%@", serverParameter);
        return;
    }
    
    NSUInteger randomNum = rand()%ids.count;
    NSArray *cssAds = [ids[randomNum] componentsSeparatedByString:@"|"];
    if (cssAds.count < 2) {
        ALog(@"CSSBannerの広告表示のためのパラメータがおかしい様です。 parameter:%@", serverParameter);
        return;
    }
    
    NSString *imageNameString = cssAds[0];
    NSString *serviceURLString = cssAds[1];
    NSString *openSafari = nil;
    if (cssAds.count > 2) {
        openSafari = cssAds[2];
    }
    _openURL = serviceURLString;
    
    UIImage *image = [UIImage imageNamed:imageNameString];
    if (image == nil) {
        NSURL *imageURL = [NSURL URLWithString:imageNameString];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        if (imageData != nil) {
            image = [UIImage imageWithData:imageData];            
        }
    }

    DLog(@"image name %@, service url %@, openSafari %@", imageNameString, serviceURLString, openSafari);
    NSAssert(image != nil, @"CSSオリジナルバナーの画像ファイル名、あるいや画像URLの指定に誤りがあります！");
    [_imageView setImage:image];
    _imageView.userInteractionEnabled = YES;
    
    if (openSafari == nil || [@"0" isEqualToString:openSafari]) {
        UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staticBannerTapped:)];
        [_imageView addGestureRecognizer:tapGR];
    } else {
        UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSafari:)];
        [_imageView addGestureRecognizer:tapGR];        
    }
    
    CGRect lblFrame = CGRectMake(0, 0, _imageView.bounds.size.width, 20);
    UILabel *adLabel = [[UILabel alloc] initWithFrame:CGRectInset(lblFrame, 6, 0)];
    adLabel.textAlignment = NSTextAlignmentRight;
    adLabel.textColor = [UIColor colorWithWhite:0.8 alpha:1.0];
    adLabel.backgroundColor = [UIColor clearColor];
    adLabel.font = [UIFont italicSystemFontOfSize:8];
    adLabel.text = @"CarolSystemSendai AD";
    [_imageView addSubview:adLabel];

    
    [self.delegate customEventBanner:self didReceiveAd:_imageView];
}

- (void)staticBannerTapped:(UITapGestureRecognizer*)tapGestureRecoginzer {
    if (tapGestureRecoginzer.state == UIGestureRecognizerStateEnded) {
        NSURL *url = [NSURL URLWithString:_openURL];
        
        // ステータスバーは隠す
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        
        // webviewで表示
        UIViewController *rootViewController = [self.delegate viewControllerForPresentingModalView];
        //UIView *contentView = [[UIView alloc] initWithFrame:self.rootViewController.view.bounds];
        UIView *contentView = [[UIView alloc] initWithFrame:rootViewController.view.bounds];
        contentView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
        //[self.rootViewController.view addSubview:contentView];
        [rootViewController.view addSubview:contentView];
        contentView.transform = CGAffineTransformMakeTranslation(0, contentView.bounds.size.height);
        
        // フッタのツールバー
        UIToolbar *footer = [[UIToolbar alloc] initWithFrame:CGRectOffset(CGRectMake(0, 0, contentView.bounds.size.width, 44), 0, contentView.bounds.size.height - 44)];
        footer.barStyle = UIBarStyleDefault;
        [contentView addSubview:footer];
        // フッタのツールボタン
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"<" style:UIBarButtonItemStylePlain target:self action:@selector(webViewBack:)];
        backButton.enabled = NO;
        UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@">" style:UIBarButtonItemStylePlain target:self action:@selector(webViewNext:)];
        UIBarButtonItem *space1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        nextButton.enabled = NO;
        space1.width = 32;
        footer.items = @[backButton, space1, nextButton];
        
        
        // 閉じるボタン
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectInset(CGRectMake(contentView.bounds.size.width - 44, contentView.bounds.size.height - 44, 44, 44), 7, 7);
        closeButton.contentMode = UIViewContentModeCenter;
        [closeButton setImage:[UIImage imageNamed:@"css_banner_closebutton"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(staticBannerCloseTapped:) forControlEvents:UIControlEventTouchUpInside];
        closeButton.clipsToBounds = YES;
        closeButton.layer.cornerRadius = 30/2.0f;
        closeButton.layer.backgroundColor = [UIColor whiteColor].CGColor;
        [contentView addSubview:closeButton];
        
        if (_activityIndicatorView != nil) {
            [_activityIndicatorView stopAnimating];
            [_activityIndicatorView removeFromSuperview];
            _activityIndicatorView = nil;
        }
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicatorView.center = contentView.center;
        _activityIndicatorView.hidesWhenStopped = YES;
        [contentView addSubview:_activityIndicatorView];
        [_activityIndicatorView startAnimating];
        
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, contentView.bounds.size.width, contentView.bounds.size.height-44)];
        webView.delegate = self;
        [contentView insertSubview:webView atIndex:0];
        [webView loadRequest:[NSURLRequest requestWithURL:url]];
        
        // 表示
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            contentView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            ;
        }];
        
        _staticContentView = contentView;
        _staticContentWebView = webView;
        _staticFooter = footer;
    }
}

- (void)openSafari:(UITapGestureRecognizer*)tapGestureRecoginzer {
    if (tapGestureRecoginzer.state == UIGestureRecognizerStateEnded) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_openURL]];
    }
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    DLog(@"request %@", request);
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    DLog(@"request %@", webView.request);
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    DLog(@"request %@", webView.request);
    
    UIBarButtonItem *backButton = [_staticFooter.items objectAtIndex:0];
    UIBarButtonItem *nextButton = [_staticFooter.items objectAtIndex:2];
    NSAssert(backButton != nil, @"footerの設定がおかしい。");
    NSAssert(nextButton != nil, @"footerの設定がおかしい。");
    if ([webView canGoForward]) {
        nextButton.enabled = YES;
    }
    if ([webView canGoBack]) {
        backButton.enabled = YES;
    }
    
    [_activityIndicatorView stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    ALog(@"request %@ ERROR! %@", webView.request, error);
    [_activityIndicatorView stopAnimating];
}

#pragma mark - static content button
- (void)staticBannerCloseTapped:(UIButton*)sender {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _staticContentView.transform = CGAffineTransformMakeTranslation(0, _staticContentView.bounds.size.height);
    } completion:^(BOOL finished) {
            // ステータスバーを戻す
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];

        [_staticContentView removeFromSuperview];
        _staticContentView = nil;
    }];
}

- (void)webViewBack:(UIBarButtonItem*)sender {
    if (_staticContentWebView.canGoBack) {
        [_staticContentWebView goBack];
    }
}
- (void)webViewNext:(UIBarButtonItem*)sender {
    if (_staticContentWebView.canGoForward) {
        [_staticContentWebView goForward];
    }
}


- (void)dealloc {
    DLog(@"CSSendaBanner dealloc");
    self.delegate = nil;
    _imageView = nil;
    
    _staticContentWebView.delegate = nil;
    
    [_staticFooter removeFromSuperview];
    [_staticContentWebView removeFromSuperview];
    [_staticContentView removeFromSuperview];
    
    _staticFooter = nil;
    _staticContentWebView = nil;
    _staticContentView = nil;
    
}

@end