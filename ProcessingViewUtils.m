//
//  ProcessingViewUtils.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/11.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "ProcessingViewUtils.h"

@implementation ProcessingViewUtils

//*****************************************
//画面内にある対象のラベルを加工します(黒枠弱)
//主にボタンなど
//*****************************************
-(void)labelshadow:(NSArray *)labelArray{
    
    //加工する要素数を確認
    NSInteger arrayNum = [labelArray count];
    
    //配列分ループ
    for(int i=0;i<arrayNum;i++){
        
        UILabel *labelName = labelArray[i];
        labelName.shadowColor = [UIColor blackColor];
        labelName.shadowOffset = CGSizeMake(0.5,1.0);
    }
    
}

//*******************************************
//画面内にある対象のラベルを加工します(ライトグレー強)
//主にタイトルなど主張したい文字用
//*******************************************
-(void)titleLabelshadow:(NSArray *)labelArray{
    
    //加工する要素数を確認
    NSInteger arrayNum = [labelArray count];
    
    //配列分ループ
    for(int i=0;i<arrayNum;i++){
        
        UILabel *labelName = labelArray[i];
        labelName.shadowColor = [UIColor lightGrayColor];
        labelName.shadowOffset = CGSizeMake(4,3);
    }
    
}


//*******************************************
//画面内にある対象のラベルを加工します(ライトグレー中)
//項目ラベルなどの文字用
//*******************************************
-(void)headTitleLabelshadow:(NSArray *)labelArray{
    
    //加工する要素数を確認
    NSInteger arrayNum = [labelArray count];
    
    //配列分ループ
    for(int i=0;i<arrayNum;i++){
        
        UILabel *labelName = labelArray[i];
        labelName.shadowColor = [UIColor darkGrayColor];
        labelName.shadowOffset = CGSizeMake(2,2);
    }

}

//*******************************************
//画面内にある対象のラベルを加工します(ライトグレー中)
//項目ラベルなどの文字用
//*******************************************
-(void)headTitleLabelshadowBlack:(NSArray *)labelArray{

    //加工する要素数を確認
    NSInteger arrayNum = [labelArray count];
    
    //配列分ループ
    for(int i=0;i<arrayNum;i++){
        
        UILabel *labelName = labelArray[i];
        labelName.shadowColor = [UIColor blackColor];
        labelName.shadowOffset = CGSizeMake(1,2);
    }
    
}

@end
