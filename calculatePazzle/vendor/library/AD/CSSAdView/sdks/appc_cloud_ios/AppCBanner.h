//
//  AppCBanner.h
//  CSSRadio
//
//  Created by Natsuki Funaki on 2013/11/26.
//  Copyright (c) 2013年 CarolSystemSendai Co.,Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GADCustomEventBanner.h"

@interface AppCBanner : NSObject <GADCustomEventBanner>
@property (assign, nonatomic) id <GADCustomEventBannerDelegate> delegate;


@end
