//
//  TutorialTimeAttackViewController.h
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/24.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialTimeAttackViewController : UIViewController

//各モードタイトル
@property (weak, nonatomic) IBOutlet UILabel *timeAttackStr;
@property (weak, nonatomic) IBOutlet UILabel *challengeStr;

@end
