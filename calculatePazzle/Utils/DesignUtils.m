//
//  DesignUtils.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/06.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "DesignUtils.h"

@implementation DesignUtils


//********************************
//背景画像の設定
//********************************
-(void)backImageSettingAct:(UIView *)settingView{
    
    //画面サイズの取得
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:rect];
    
    //夜のイメージ
    imageView.image = [UIImage imageNamed:@"renga.jpg"];
    //日中イメージ
    //imageView.image = [UIImage imageNamed:@"nature2.jpg"];
    
    [settingView addSubview:imageView];
    [settingView sendSubviewToBack:imageView];
    
}


//********************************
//背景画像の設定(カメラロール)
//********************************
-(void)backImageCameraSettingAct:(UIView *)settingView image:(NSData *)data{
    
    
    if(data){
        //画面サイズの取得
        CGRect rect = [[UIScreen mainScreen] bounds];
    
        //広告分小さくします
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(rect.origin.x,rect.origin.y,rect.size.width, rect.size.height-60)];
    
        //NSData型の背景画像情報をUIImageにキャスト
        UIImage *image = [UIImage imageWithData:data];
    
        //カメラロールで選択した画像の張付け
        imageView.image = image;
        
    
        [settingView addSubview:imageView];
        [settingView sendSubviewToBack:imageView];
    }else{
        
        //画面サイズの取得
        CGRect rect = [[UIScreen mainScreen] bounds];
       
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:rect];
        
        imageView.image = [UIImage imageNamed:@"renga.jpg"];
        
        [settingView addSubview:imageView];
        [settingView sendSubviewToBack:imageView];
    }

}

//************************************
//背景(グラデーション)
//************************************
-(void)gradation:(UIView *)settingView{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = settingView.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor orangeColor] CGColor], (id)[[UIColor whiteColor] CGColor], nil];
    [settingView.layer insertSublayer:gradient atIndex:0];
    
}

@end
