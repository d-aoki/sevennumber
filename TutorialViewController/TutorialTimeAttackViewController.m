//
//  TutorialTimeAttackViewController.m
//  SevensNumber
//
//  Created by Daichi Aoki on 2014/03/24.
//  Copyright (c) 2014年 Daichi Aoki. All rights reserved.
//

#import "TutorialTimeAttackViewController.h"
#import "DesignUtils.h"
#import "ProcessingViewUtils.h"

@interface TutorialTimeAttackViewController ()

@end


//TimeAttackLabelのRGB値
float const rankplayLabelR = 0.59;
float const rankplayLabelG = 0.90;
float const rankplayLabelB = 0.48;
float const rankplayLabelAlpha = 1.0;

//ChallengeLabelのRGB値
float const challplayLabelR = 1.0;
float const challrankplayLabelG = 0.388;
float const challrankplayLabelB = 0.278;
float const challrankplayLabelAlpha = 1.0;

@implementation TutorialTimeAttackViewController

{
    CGPoint _movePageTouch;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //デフォルト背景
    DesignUtils *designUtils = [[DesignUtils alloc]init];
    //[designUtils gradation:self.view];
    [designUtils backImageSettingAct:self.view];
    
    
    //テキストのRGB値
    _timeAttackStr.textColor = [UIColor colorWithRed:rankplayLabelR green:rankplayLabelG blue:rankplayLabelB alpha:rankplayLabelAlpha];
    _challengeStr.textColor = [UIColor colorWithRed:challplayLabelR green:challrankplayLabelG blue:challrankplayLabelB alpha:challrankplayLabelAlpha];
    
    NSArray *titleArray = [[NSArray alloc]initWithObjects:_timeAttackStr,_challengeStr, nil];
    ProcessingViewUtils *proUtils = [[ProcessingViewUtils alloc]init];
    [proUtils headTitleLabelshadow:titleArray];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Touch Event

//********************************************
//画面がタッチされたら、次のページに遷移する用にします
//********************************************
-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    _movePageTouch = [touch locationInView:self.view];
    
    DLog(@"ページがタッチされました。ページ遷移します");
    
    //playViewのインスタンス
    UIViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialFreeViewController"];
    
    // アニメーションを準備します。
    CATransition* transition = [CATransition animation];
    
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    
    // UINavigationController にアニメーションを設定して、プッシュを行います。
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:view animated:NO];
    
    
}

@end
