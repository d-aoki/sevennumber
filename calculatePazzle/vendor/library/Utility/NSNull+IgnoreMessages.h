//
//  NSNull+IgnoreMessages.h
//
//  NSNullへのメッセージが、nilと同様の振る舞いをするようにします。
//
//  Created by Natsuki Funaki on 2013/07/31.
//  Copyright (c) 2013年 CarolSystemSendai Co.,Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNull (IgnoreMessages)
- (void)forwardInvocation:(NSInvocation *)anInvocation;
- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector;
@end
